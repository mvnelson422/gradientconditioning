import numpy as np
import torch
from warnings import warn

def lr_line_search(model, trainloader, max_lr, lossf, tol=1e-5, test_run=False, 
                   tolerable_loss_variance=1e-5, min_batches=2, max_batches=5,
                   min_samples=100):
    """
    initially, plots the line with best guess
    
    inputs:
        model - this model must have current gradient information (.backward has been called but not optimizer.step)
        trainloader - the dataloader used during training
        max_lr - the maximum allowable lr
        loss_f - the loss function for the model in question
        tol - the desired spacing between test values
        test_run - a variable which forces testing of every possible lr and returns the linspace and loss results
        
        inputs to estimate_loss:
            tolerable_loss_variance - helps determine how many batches to test before returning
            min_batches - the required minimum number of batches to test
            max_batches - the maximum tolerable number of batches to test before returning an average
            
        inputs to line_search:
            min_samples - minimum required samples before selecting minimum
    """
    
#     # perform backwards step
#     for x, y in trainloader:
#         x = x.to('cuda')
#         y = y.to('cuda')

#         y_hat = model.forward(x)
#         loss = loss_f(y_hat, y)
#         loss.backward()
#         break
    
    lr_space = np.linspace(0, max_lr, int(max_lr/tol))
    losses = np.zeros(len(lr_space))
    optimal_lr = 0
    
    for i, lr in enumerate(lr_space):
        # TODO: replace estimate loss function with a simple loss calculation. Use the same large batch for every forward 
        # projection loss calculation. This should smooth the results, significantly reduce calculation times, and make 
        # it possible to use newton's method in the line search function
        losses[i] = estimate_loss(model, trainloader, lossf, lr, tolerable_loss_variance, min_batches, max_batches)
        optimal_lr, stop_early = line_search(losses, test_run, min_samples)
        
        if stop_early:
            break
    
    if test_run:
        return lr_space, losses, optimal_lr
    else:
        return optimal_lr
        
        
def estimate_loss(model, trainloader, lossf, project_lr, tolerable_loss_variance, min_batches, max_batches):
    """
    estimate the loss using trainloader
    """
    with torch.no_grad():
        lossArray = []
        for x, y in trainloader:
            x = x.to('cuda')
            y = y.to('cuda')

            y_hat = model.forward(x, project_lr=project_lr)
            lossArray += [lossf(y_hat, y).item()]
            
            if len(lossArray) >= max_batches:
                return np.mean(lossArray)
            elif len(lossArray) >= min_batches and np.var(lossArray) < tolerable_loss_variance:
                return np.mean(lossArray)
    
    
def line_search(losses_test_run, min_samples, peak_tolerances):
    """
    This will terminate early if a suitible minimum is found based on peak_tolerances
    Generally, this function tries to identify overal trends in loss data, and returns the absolute minimum found which is contained
    in the earliest decreasing trend.
    
    """
    
    # Use a single large batch and find using newton's method (check if noise disappears using single batch)
    # increase termination condition if fails
    
    warn("line_search is not fully implemented yet.")
    if len(losses_test_run < min_samples):
        return losses_test_run[-1], False
    return losses_test_run[-1], False
    

def find_tolerable_loss(model, train_loader, lossf, target_batches, n_tests):
    target_var = 0
    for i in range(n_tests):
        losses = np.zeros(target_batches)
        for j, (x, y) in enumerate(train_loader):
            x = x.to('cuda')
            y = y.to('cuda')

            y_hat = model.forward(x)
            losses[j] = lossf(y_hat, y).item()
            
            if j >= target_batches - 1:
                break
        target_var += np.var(losses)/n_tests
        
    return target_var
                
            
            
        