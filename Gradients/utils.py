import torch
import torch.nn as nn
from torch.autograd import Variable
import numpy as np
from matplotlib import pyplot as plt
from tqdm import tqdm
from IPython.display import Audio, display
from LayersAndNetworks import LinearHettingerInit, LooksLinearLayer

layerTypesOfInterest = [nn.Conv2d, nn.Linear, nn.Conv1d, LinearHettingerInit, LooksLinearLayer]

def play_alert():
    display(Audio(url='sms-alert-1-daniel_simon.wav', autoplay=True))

#___________________________________________________________________________________________________________________________________________
def find_optimal_lr(model, y_hat, testloader, optimizer_class, lossf, lr_bounds, npoints, findS=False):
    """
    Train the provided model several times for one step (using the same initialization repeatedly)
    to identify the learning rate which results in the lowest loss.
    """

    def optimize_func(lr):
        # reset the optimizer using a cloned model
        temp_model = deepcopy(model).to('cuda')
        optimizer = optimizer_class(temp_model.parameters(), lr=lr)

        # recalculate loss using the same y_hat, calculate backwards pass, and step 
        # TODO: find a way to do this more efficiently (i.e. without recalculating backwards pass)
        temp_model.zero_grad()
        loss = lossf(y_hat, y)
        loss.backward()
        optimizer.step()

        # evaluate the loss using the whole testing set
        with torch.no_grad():
            lossArray = []
            for x, y in testloader:
                x = x.to('cuda')
                y = y.to('cuda')

                y_hat = temp_model(x)
                lossArray += [lossf(y_hat, y).item()]

        return np.mean(lossArray)
    
    # test all learning ratese in specified geomspace
    testpoints = np.geomspace(lr_bounds[0], lr_bounds[1], npoints)

    # test each point in geomspace
    optimal_lr = testpoints[0]
    low = optimize_func(optimal_lr)

    for lr in testpoints[1:]:
        loss = optimize_func(lr)
        if loss < low:
            low = loss
            optimal_lr = lr 

    return optimal_lr, low
    
#___________________________________________________________________________________________________________________________________________

def relative_effective_rank(model, data_loader, lossf, batch_size=256, normalizing_constants=None,
                           return_normalizing_constant=False, n_ave=1, wrt_input=False, which_layers=-1, verbose=True):
    
    norm_not_provided = (normalizing_constants is None)
        
    if data_loader.batch_size != 1:
        raise ValueError("Batch size must be 1.")
    
    relevant_layers = [module for module in model.modules() if type(module) in layerTypesOfInterest]
    
    if which_layers == 'all':
        which_layers = np.arange(len(relevant_layers))
    elif which_layers is None:
        which_layers = []
        
    n_layers = len(relevant_layers) + int(wrt_input)
    
    if not norm_not_provided and len(normalizing_constants) != n_layers:
        raise ValueError("len of normalizing constants does not match number of desired layers") 
    
    if norm_not_provided:
        normalizing_constants = np.zeros(n_layers)
    effective_ranks = np.zeros(n_layers)
    
    for _ in range(n_ave):
        print(f'    starting test {_+1} out of {n_ave}')
        As = dict()
        model = model.cuda()

        total = 0
        for x, y_true in data_loader:
            x, y_true = x.cuda(), y_true.cuda()
            model.zero_grad()

            x = Variable(x, requires_grad=True)
            output = lossf(model(x), y_true)
            output.backward(retain_graph=True)
                
            if wrt_input:
                minibatch_size, c, m, n  = x.shape
                if 0 not in As.keys():
                    As[0] = np.zeros((c*m*n, batch_size))

                for i in range(minibatch_size):
                    As[0][:, total] = x.grad[i, :].detach().cpu().numpy().flatten() 

            for i, j in enumerate(which_layers):
                layer = relevant_layers[which_layers[j]]
                i += int(wrt_input)
                grad_temp = layer.weight.grad.detach().cpu().numpy().flatten() 
                
                if i not in As.keys():
                    As[i] = np.zeros((len(grad_temp), batch_size))
                As[i][:, total] = grad_temp
                    
            total += 1
            if total >= batch_size:
                break

        #effective_rank = np.trace(A.T@A)/(np.linalg.norm(A, ord=2)**2) # TODO: check this is the correct norm
        for i in range(n_layers):
            effective_ranks[i] += (np.linalg.norm(As[i], ord='fro')**2)/(np.linalg.norm(As[i], ord=2)**2)/n_ave

        # could put following in a separate loop, but we'll assume n_ave is high enough

        if norm_not_provided:
            for i in range(n_layers):
                A = As.pop(i) # deletes A so it's ready for next cycle. Also, keeps memory from doubling like it would otherwise
                m, n = np.shape(A)
                std = np.std(A)
                del A
                
                # calculate normalizing constant
                A = np.random.normal(scale=std, size=(m, n))
                #normalizing_constant = np.trace(A.T@A)/(np.linalg.norm(A, 2)**2)
                normalizing_constants[i] += (np.linalg.norm(A, ord='fro')**2)/(np.linalg.norm(A, ord=2)**2)/n_ave

    if return_normalizing_constant:
        return effective_ranks/normalizing_constants, normalizing_constants
    else:
        return effective_ranks/normalizing_constants
     
#___________________________________________________________________________________________________________________________________________

def gradient_statistics(model, data_loader, lossf):
    model = model.cuda()
    relevant_layers = [module for module in model.modules() if type(module) in layerTypesOfInterest]
    
    nlayers = len(relevant_layers)
    
    magnitudes = np.zeros(len(relevant_layers))
    means = np.zeros(len(relevant_layers))
    stds = np.zeros(len(relevant_layers))
    
    for x, y_true in data_loader:
        x, y_true = x.cuda(), y_true.cuda()
        model.zero_grad()
        
        x = Variable(x, requires_grad=True)
        output = lossf(model(x), y_true)
        output.backward(retain_graph=True)
        
        for j, layer in enumerate(relevant_layers):
            grad_temp = layer.weight.grad.detach().cpu().numpy().flatten()
            magnitudes[j] = np.linalg.norm(grad_temp, 2)
            means[j] = np.mean(grad_temp)
            stds[j] = np.std(grad_temp)
        break
                                                   
    return magnitudes, means, stds

        
#___________________________________________________________________________________________________________________________________________


def gradient_statistics_old(model, dataset, lossf, minibatch_size,
                        calculate_relative_effective_rank=True,
                        normalizing_constant=None, 
                        n_batches=1, n_samples=1, 
                        return_normalizing_constant=False):
    model = model.cuda()
    data_loader = torch.utils.data.DataLoader(dataset,
                                             batch_size=1,
                                             shuffle=True)
    relevant_layers = [module for module in model.modules() if type(module) in layerTypesOfInterest]
    effective_rank = 0
    gradient_magnitudes = np.zeros(len(relevant_layers))
    
    for _ in range(n_batches):
        # construct matrix of gradients
        i = 0
        grads = []
        for x, y_true in data_loader:
            x, y_true = x.cuda(), y_true.cuda()
            i += 1

            y_hat = model(x)
            #print(y_hat, y_true)
            loss = lossf(y_hat, y_true)
            loss.backward()

            flattened_grads = []               # Here we flatten all of the gradients for each layer and construct one large matrix. Since all we care about is structure between columns, we don't care how they were oriented originally
            for j, layer in enumerate(relevant_layers):
                grad_temp = layer.weight.grad.detach().cpu().numpy().flatten()
                gradient_magnitudes[j] += np.linalg.norm(grad_temp, 2)/(minibatch_size*n_batches)
                if calculate_relative_effective_rank:
                    flattened_grads = np.append(flattened_grads, grad_temp)

            grads += [flattened_grads]

            if i == minibatch_size:
                break
        
        # calculate effective rank
        if calculate_relative_effective_rank:
            A = np.array(grads)
            effective_rank += np.trace(A.T@A)/(np.linalg.norm(A, 2)**2)/n_batches
        
    if not calculate_relative_effective_rank:
        return gradient_magnitudes
    
    if normalizing_constant is None:
        m, n = np.shape(A)
        print(m, n)
        del A, grads

        # calculate normalizing constant
        normalizing_constant = 0
        for j in range(n_samples):
            A = np.random.normal(size=(m, n))
            normalizing_constant += np.trace(A.T@A)/(np.linalg.norm(A, 2)**2)/n_samples

    if return_normalizing_constant:
        return gradient_magnitudes, effective_rank/normalizing_constant, normalizing_constant
    else:
        return gradient_magnitudes, effective_rank/normalizing_constant
    
    
#___________________________________________________________________________________________________________________________________________ 
def gradient_metrics(network, train_loader, optimizer, loss_func, G_vals=None, layers='all', N=100, normalizing_constants=[],
                     GC_layers_bool=True, GC_stacked_bool=False, RER_layers_bool=True, RER_stacked_bool=False, 
                     GD_layers_bool=True, GD_stacked_bool=False,
                     n_ave_nc=2, verbose=True):
    """
    Calculate gradient confusion by cosine similarity of N randomly sampled minibatches.
    Each layer is calculated separately and as one large matrix.
    
    Also calculate the relative effective rank for the same layers using the same samples for convenience.
    
    input_:
        network: the pytorch model being trained
        train_loader: the train loader being used in training
        optimizer: the optimizer being used in training
        loss_func: the loss function being used in training
        layers: the layers of the network to be used in measuring the gradient metrics. Only those that can learn
            are chosen by default if input_ is 'all'
        N: the number of pairs of minibatches to use in testing the gradient confusion cosine similarity,
            for convenience N*2 is used as the number of columns for measuring the relative effective rank
        normalizing_constants: the normalizing constants to use for the relative effective rank per layer,
            if None, will re-calculate. If RER_stacked desired then last normalizing constant should be for that.
        verbose: whether or not to print updates
        n_ave_nc: the number of samples to draw for calculating the normalizing constant for RER
        ??_layers_bool: determines whether that metric is calculated for specific layers
        ??_stacked_bool: determines whether that metric is calculated using the full stacked vectors
        
        G_vals: if G_vals not provided, then run through a training loop with network frozen to sample them
            
    returns:
        GC_layers: the cosine similarity for each sampled pair (specific per layer)
        GC_stacked: the cosine similarity for each sampled pair (all layers stacked)
        RER_layers: the relative efffective rank of the matrix A, where each column is 
            the flattened gradient of the specified layer for one of the 2*N recorded minibatches
        RER_stacked: the relative effective rank calculated as specified above, except that for each
            minibatch the flattened gradients for all layers are stacked.
    """
    # TODO: determine which of these is more efficient
    EffRank = lambda A: ((np.linalg.norm(A, ord='fro')**2)/(np.linalg.norm(A, ord=2)**2))
    
    rd = dict() # return dictionary
    return_nc = False
    first_pass = True
    
    # determine which pairs
    samples = np.random.choice(len(train_loader), size=(N, 2), replace=False) # replace=False for RER not GC
    
    # this line may need updates if new types of gradient_tracking layers are included later
    if layers == 'all':
        layers = [module for module in network.modules() if type(module) in layerTypesOfInterest]

    if len(normalizing_constants) > 0 and (len(normalizing_constants) != len(layers) + RER_stacked_bool):
        raise ValueError("len of normalizing constants does not match number of desired layers + sta")
    if len(normalizing_constants) == 0:
        return_nc = True
        normalizing_constants = np.zeros(len(layers) + RER_stacked_bool)

    # initialize storage for gradients
    gradient_samples_provided = True
    if G_vals is None:
        gradient_samples_provided = False
        G_vals = dict()
        for k in range(len(layers)): 
            G_vals[k] = dict()

    if GC_layers_bool:
        rd['GC_layers'] = np.zeros((len(layers), N))
    if GC_stacked_bool:
        rd['GC_stacked'] = np.zeros(N)
    if RER_layers_bool:
        rd['RER_layers'] = np.zeros((len(layers)))
    if RER_stacked_bool:
        rd['RER_stacked'] = 0
    if GD_layers_bool:
        rd['GD_layers'] = np.zeros(len(layers))
    if GD_stacked_bool:
        rd['GD_stacked'] = 0
        
    
    if not gradient_samples_provided:
        if verbose:
            print(f'    storing {N} pairs of gradients....')
            loop = tqdm(total = 2*N)

        network = network.cuda()
        for i, (x_train, y_train) in enumerate(train_loader):
            if i in samples.flatten(): # else skip this iteration
                x_train, y_train = x_train.cuda(async=True), y_train.cuda(async=True)

                # zero gradients, then forward and backward pass
                optimizer.zero_grad()
                y_train_hat = network(x_train)
                loss_train = loss_func(y_train_hat, y_train)
                loss_train.backward()

                # store values for gradient confusion  
                for k, layer in enumerate(layers):
                    G_vals[k][i] = layer.weight.grad.detach().cpu().numpy().flatten()
                loop.update(1)

        loop.close()
    
    # calculate layer specific metrics
    stacked_height=0
    Aj = dict()
    if verbose:
        print('    prepping and calculating requested layer-specific metrics....')
        loop = tqdm(total=len(layers))
    
    for j in range(len(layers)):
        
        # construct Aj matrices for calculating RER
        m = len(G_vals[j][samples[0][0]])
        stacked_height += m
        Aj[j] = np.zeros((m, 2*N))
        
        for k, (p, q) in enumerate(samples):
            if GC_layers_bool:
                rd['GC_layers'][j, k] = np.inner(G_vals[j][p], G_vals[j][q])
            Aj[j][:, 2*k] = G_vals[j][p]
            Aj[j][:, 2*k+1] = G_vals[j][q]
        
        # calculate RER layer specific
        if RER_layers_bool: 
            # calculate normalizing constants if necessary
            if normalizing_constants[j] == 0:
                m, n = np.shape(Aj[j])
                for k in range(n_ave_nc):
                    A_temp = np.random.normal(size=(m, n))
                    normalizing_constants[j] += EffRank(A_temp)/n_ave_nc

            rd['RER_layers'][j] = EffRank(Aj[j])
            rd['RER_layers'][j] /= normalizing_constants[j]
            
        if GD_layers_bool:
            rd['GD_layers'][j] = np.sum(Aj[j]**2)/np.sum(np.sum(Aj[j], axis=1)**2)
            
        loop.update(1)
    loop.close()
    
    # Calculate stacked variables if desired
    if GC_stacked_bool or RER_stacked_bool:
        A = np.vstack((Aj[j] for j in range(len(layers))))
        
        # calculate GC_total
        if GC_stacked_bool:
            if verbose:
                print('    calculating stacked gradient confusion....')
                loop = tqdm(total=N)
            for k in range(N):
                rd['GC_stacked'][k] = np.inner(A[:, 2*k], A[:, 2*k+1])
                loop.update()
            loop.close()
        
        # calculate RER_total and normalizing constant if necessary
        if RER_stacked_bool:
            if verbose:
                print('    calculating stacked RER....')
                
            RER_stacked = EffRank(A)
            
            if normalizing_constants[-1] == 0:
                m, n = np.shape(A)
                for k in range(n_ave_nc):
                    A_temp = np.random.normal(size=(m, n))
                    normalizing_constants[-1]  += EffRank(A_temp)/n_ave_nc

            rd['RER_stacked'] /= normalizing_constants[-1]
            
        if GD_stacked_bool:
            rd['GD_stacked'] = np.sum(A**2)/np.sum(np.sum(A, axis=1)**2)
    
    # zero gradients just in case training loop doesn't take this precaution
    optimizer.zero_grad()
    
    rd2 = dict()
    rd2['metrics'] = rd
    if return_nc:
        rd2['normalizing_constants'] = normalizing_constants
            
    return rd2


#___________________________________________________________________________________________________________________________________________

def train(md, train_loader, test_loader, epochs, fileloc,
          graph_skip=50, tqdm_bool=True, plot_every=10,
          GC_layers_bool=True, GC_stacked_bool=False, 
          RER_layers_bool=True, RER_stacked_bool=False, 
          GD_layers_bool=True, GD_stacked_bool=False,
          GD_samples=20, GD_skip=5,
          grad_stats_every=10, grad_stats_at_init=True):
    """
    input_ parameters:
        md: a dictionary containing the following keys
            model: The network used to train.

            losses_train: the array where averaged training losses are stored. 
              If this is to be kept over multiple calls to train_heartbeat than should be passed in.
            losses_test: the array where testing losses are stored. " "
            accuracy: overall accuracy of network. " "
            per_class_accuracy: accuacy of each individual class. " "

        epochs: the number of epochs to train over

        graph_skip: the number of batches to complete before storing the losses
        tqdm_bool: whether or not to display a progress bar
        plot_every: the number of epochs to complete in between plotting losses and accuracy measurements
          if None will never plot

            num_classes: used in plotting devices. Could find a way not to need this as parameter.

       return values:

    """ 

    if "model" not in md.keys():
        raise ValueError("Key model must be included in input_ dictionary")
    if "optimizer" not in md.keys():
        raise ValueError("Key optimizer must be included in input_ dictionary")
    if "loss_func" not in md.keys():
        raise ValueError("Key loss_func must be included in input_ dictionary")
    if "num_classes" not in md.keys():
        raise ValueError("Key model must be included in input_ dictionary")
    if "losses_train" not in md.keys():
        md["losses_train"] = []
    if "losses_test" not in md.keys():
        md["losses_test"] = []
    if "accuracy" not in md.keys():
        md["accuracy"] = []
    if "per_class_accuracy" not in md.keys():
        md["per_class_accuracy"] = []
    if "gradient_metrics" not in md.keys():
        md["gradient_metrics"] = dict()
        md["gradient_metrics"]["normalizing_constants"] = []
    if "epochs_completed" not in md.keys():
        md["epochs_completed"] = 0
    if "graph_skip" not in md.keys():
        md["graph_skip"] = graph_skip
    if "lr_scheduler" not in md.keys():
        md["lr_scheduler"] = None
    if "GD" not in md.keys():
        # we don't calculate these specific for each layer since that generates an excess of data
        md["GD"] = dict()
        
#         md["GD"]["running_sum"] = None
#         md["GD"]["running_sum_of_norms"] = None
#         md["GD"]["vals"] = []
#         md["GD"]["N"] = running_N
        
        # for periodic test (instead of running test)
        md["GD"]["N_skip"] = GD_skip
        md["GD"]["N_samples"] = GD_samples
        md["GD"]["current_samples"] = 0
        
        md["GD"]["sum"] = None
        md["GD"]["sum_of_norms"] = 0
        
        md["GD"]["vals"] = []
        
        
    else:
        if md["graph_skip"] != graph_skip:
            print('using previous graph_skip for consistency')
            graph_skip = md["graph_skip"]
    
    relevant_layers = [module for module in md['model'].modules() if type(module) in layerTypesOfInterest]
    
    # move model to cuda (technically optimizer should be constructed after this, but it doesn't matter with SGD or ADAM)
    md["model"] = md["model"].cuda()
    
    # calculate gradient metrics at initialization
    if (GC_layers_bool + GC_stacked_bool + RER_layers_bool + RER_stacked_bool > 0) and grad_stats_at_init and md['epochs_completed']==0:
        if f'epoch 0' not in md['gradient_metrics'].keys():
            print('calculating gradient metrics:')
            metrics_dict = dict()
            metrics_dict = gradient_metrics(md['model'], train_loader, md['optimizer'], md['loss_func'], layers='all', N=100, 
                                            normalizing_constants=md['gradient_metrics']['normalizing_constants'],
                                            GC_layers_bool=GC_layers_bool, GC_stacked_bool=GC_stacked_bool, 
                                            RER_layers_bool=RER_layers_bool, RER_stacked_bool=RER_stacked_bool, 
                                            n_ave_nc=2, verbose=tqdm_bool)
            md['gradient_metrics']['epoch 0'] = metrics_dict['metrics']
            if md['gradient_metrics']['normalizing_constants'] is None:
                md['gradient_metrics']['normalizing_constants'] = metrics_dict['normalizing_constants']
            del metrics_dict
            
            torch.save(md, fileloc)
            
        if RER_layers_bool:
            plt.plot(md['gradient_metrics'][f'epoch 0']['RER_layers'])
            plt.title('RER per layer: at initialization')
            plt.show()
    

    # begin training loop
    for epoch in range(md['epochs_completed'] + 1, epochs + md['epochs_completed'] + 1):
        
        if tqdm_bool:
            loop = tqdm(total = len(train_loader), position=0)
            loop.set_description(f'epoch: {epoch}')

        # initialize temporary training losses array. This will be averaged and reset each i%graph_skip==0 iteration
        temp_losses_train = []
        temp_accuracy = []
        temp_per_class_accuracy = []

        for i, (x_train, y_train) in enumerate(train_loader):
            x_train, y_train = x_train.cuda(async=True), y_train.cuda(async=True)

            # zero gradients
            md['optimizer'].zero_grad()

            # calculate and store training loss
            y_train_hat = md['model'](x_train)
            loss_train = md['loss_func'](y_train_hat, y_train)

            # call .backward and step
            loss_train.backward()
            
#             # calculate Gradient Diversity Metric
#             if running_GD_bool:
#                 flattened_grads = []               
#                 for j, layer in enumerate(relevant_layers):
#                     flattened_grads = np.append(flattened_grads, layer.weight.grad.detach().cpu().numpy().flatten())

#                 if md["GD"]["running_sum"] is None:
#                     md["GD"]["running_sum"] = flattened_grads * running_N
#                 else:
#                     md["GD"]["running_sum"] = ((running_N-1)/running_N)*md["GD"]["running_sum"] + flattened_grads
                    
#                 if md["GD"]["running_sum_of_norms"] is None:
#                     md["GD"]["running_sum_of_norms"] = (np.linalg.norm(flattened_grads, 2)**2) * running_N
#                 else:
#                     md["GD"]["running_sum_of_norms"] = (((running_N-1) / running_N) * md["GD"]["running_sum_of_norms"] 
#                                                         + np.linalg.norm(flattened_grads, 2)**2)
#                 del flattened_grads
                
#                 md["GD"]["vals"] += [md["GD"]["running_sum_of_norms"]/np.linalg.norm(md["GD"]["running_sum"], 2)**2]
            
            if GD_samples:
                if ((epoch-1)*len(train_loader) + i )%GD_skip == 0:
                    md["GD"]["current_samples"] += 1
                    
                    flattened_grads = []               
                    for j, layer in enumerate(relevant_layers):
                        flattened_grads = np.append(flattened_grads, layer.weight.grad.detach().cpu().numpy().flatten())
                    
                    if md["GD"]["sum"] is None:
                        md["GD"]["sum"] = flattened_grads
                    else:
                        md["GD"]["sum"] += flattened_grads

                    md["GD"]["sum_of_norms"] += np.linalg.norm(flattened_grads, 2)**2
                        
                    if md["GD"]["current_samples"] >= GD_samples:
                        md["GD"]["current_samples"] = 0
                        md["GD"]["vals"] += [md["GD"]["sum_of_norms"] / 
                                                              np.linalg.norm(md["GD"]["sum"], 2)**2]
                        md["GD"]["sum"] = None
                        md["GD"]["sum_of_norms"] = 0
            
            md['optimizer'].step()

            # store loss, accuracy, and per class accuracies calling .cpu on each
            temp_losses_train.append(loss_train.detach().cpu().item())

            # accuracy
            class_probabilities = torch.exp(y_train_hat)
            chosen_classes = torch.multinomial(class_probabilities,1).squeeze(1)
            temp_accuracy.append(torch.mean((chosen_classes==y_train).float()).detach().cpu())

            # per-class accuracies
            acct = np.zeros(md['num_classes'])
            for j in range(md['num_classes']):
                mask = (y_train==j)
                acct[j] = torch.mean((chosen_classes[mask]==y_train[mask]).cpu().float())
            temp_per_class_accuracy.append(acct)


            #calculate testing loss, and store training and testing losses for later plotting every (graph_skip)th iteration
            if i > 0 and i%graph_skip==0:
                # store averaged accuracies and training loss and update progress bar
                # also reset temporary arrays
                acc = np.mean(temp_accuracy)
                md['accuracy'].append(acc)
                temp_accuracy = []

                lt = np.mean(temp_losses_train)
                md['losses_train'].append(lt)
                temp_losses_train = []

                acct = np.nanmean(temp_per_class_accuracy, axis=0)
                md['per_class_accuracy'].append(acct)
                temp_per_class_accuracy = []

                if tqdm_bool:
                    if GD_samples and len(md['GD']['vals']) >= 1:
                        loop.set_description('epoch: {}, averaged loss: {:.3f}, accuracy: {:.2f}%, mean GD: {:.2f}'#, gpu usage: {:.2f}%'
                                       .format(epoch, lt, acc*100, md["GD"]["vals"][-1]))#, 100*gpu.memoryUtil))
                    else:
                        loop.set_description('epoch: {}, averaged loss: {:.3f}, accuracy: {:.2f}%'#, gpu usage: {:.2f}%'
                                       .format(epoch, lt, acc*100))#, 100*gpu.memoryUtil))

                with torch.no_grad():
                    #get x and y test and transfer them to gpu
                    x_test, y_test = next(iter(test_loader))
                    x_test, y_test = x_test.cuda(async=True), y_test.cuda(async=True) #async tiny speed improvement

                    # calculate and store testing losses
                    y_test_hat = md['model'](x_test)
                    loss_test = md['loss_func'](y_test_hat,y_test).detach().cpu()
                    md['losses_test'].append(loss_test)

            #update progress bar
            if tqdm_bool:
                loop.update(1)
                
        # update lr
        if md['lr_scheduler']:
            md['lr_scheduler'].step()

        # close progress bar for this epoch
        if tqdm_bool:
            loop.close()
        md['epochs_completed'] = epoch
        
        # calculate gradient metrics before epoch starts with frozen optimizer (so we get it at initialization)
        if (GC_layers_bool + GC_stacked_bool + RER_layers_bool + RER_stacked_bool > 0) and (epoch)%grad_stats_every==0:
            if f'epoch {epoch}' not in md['gradient_metrics'].keys():

                print('calculating gradient metrics:')
                metrics_dict = dict()
                metrics_dict = gradient_metrics(md['model'], train_loader, md['optimizer'], md['loss_func'], layers=relevant_layers, N=100, 
                                                normalizing_constants=md['gradient_metrics']['normalizing_constants'],
                                                GC_layers_bool=GC_layers_bool, GC_stacked_bool=GC_stacked_bool, 
                                                RER_layers_bool=RER_layers_bool, RER_stacked_bool=RER_stacked_bool,
                                                GD_layers_bool=GD_layers_bool, GD_stacked_bool=GD_stacked_bool,
                                                n_ave_nc=2, verbose=tqdm_bool)
                md['gradient_metrics'][f'epoch {epoch}'] = metrics_dict['metrics']
                if md['gradient_metrics']['normalizing_constants'] is None:
                    md['gradient_metrics']['normalizing_constants'] = metrics_dict['normalizing_constants']
                del metrics_dict
                
                torch.save(md, fileloc)
                
            if RER_layers_bool:
                plt.plot(md['gradient_metrics'][f'epoch {epoch}']['RER_layers'])
                plt.title(f'RER per layer: epoch {epoch}')
                plt.show()
        
        if plot_every is not None:
            if epoch%plot_every==0:    
                x_spacing = np.arange(len(md['losses_train']))
                plt.plot(graph_skip*x_spacing, md['losses_train'], label='Training loss')
                plt.plot(graph_skip*x_spacing, md['losses_test'], label='Testing loss')
                plt.xlabel('iterations')
                plt.ylabel('loss')
                plt.title("Loss over time")
                plt.legend()
                plt.show()

                # plot accuracies
                plt.plot(graph_skip*x_spacing, md['accuracy'], label='Total')
                for i in range(md['num_classes']):
                    plt.plot(graph_skip*x_spacing, np.array(md['per_class_accuracy'])[:,i], label='Class '+str(i))

                plt.xlabel('iterations')
                plt.ylabel('accuracy')
                plt.title("Accuracy over time")
                plt.legend()
                plt.show()
                
                
    return md