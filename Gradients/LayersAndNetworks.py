import torch
from torch import nn
import torch.nn.functional as F
import numpy as np

from warnings import warn

class gradient_memory_vector_tracking_network(nn.Module):
    """
    This class is intended to be used as a parent for any network which needs to be able to track gradient memory vectors.
    """
    def __init__(self):
        """
        Very little is done here
        """
        super(gradient_memory_vector_tracking_network, self).__init__()
        self.mask_sizes_percent = None
        self.mask_sizes_count = None
        self.relevant_layers_dict = None
        self.include_bias = None
        
        self.n_vectors = None
        self.n_stored_in_sequence = None
        self.memory_vectors = None
        
    def initialize_masks(self, mask_sizes_percent, include_bias=True):
        """
        Using a provided float(s) between 0 and 1, initialize a mask(s) to be used when saving gradient vectors. Random
        projections tend to maintain a large amount of information about distances and angles in high dimensional spaces,
        hence we use a random mask to preserve memory and reduce computational time for any tasks which use these vectors.
        Multiple floats may be provided to compare effects of different size projections. This unfortunately results in 
        some awkward and slightly inefficient code.
        
        inputs:
            mask_sizes_percent: array of float values between 0 and 1 determines what percent of gradient vectors are stored
                as a projection
            include_bias: set false to ignore bias gradients when constructing masks and later when saving memory vectors
        """
        self.include_bias = include_bias
        
        for p in mask_sizes_percent:
            if p > 1 or p < 0:
                raise ValueError('each value in mask_sizes_percent must be between 0 and 1')
        
        self.mask_sizes_percent = mask_sizes_percent
        self.mask_sizes_count = np.zeros(len(mask_sizes_percent)).astype(int)
        self.relevant_layers_dict = dict()
        
        i = 0
        for module in self.modules():
            if hasattr(module, 'weight') and module.weight.requires_grad and type(module) is not nn.BatchNorm2d:
                self.relevant_layers_dict[i] = dict()
                self.relevant_layers_dict[i]['layer'] = module
                self.relevant_layers_dict[i]['start_indices'] = np.array(self.mask_sizes_count)
                
                if self.include_bias:
                    total_len = np.product(module.weight.shape) + np.product(module.bias.shape)
                else:
                    total_len = np.product(module.weight.shape)
                
                self.relevant_layers_dict[i]['masks'] = [np.random.choice(total_len, replace=False, size=int(min(np.ceil(p*total_len), total_len))) for p in mask_sizes_percent]
                
                self.mask_sizes_count += [len(mask) for mask in self.relevant_layers_dict[i]['masks']]
                self.relevant_layers_dict[i]['end_indices'] = np.array(self.mask_sizes_count)
                i += 1
        self.mask_sizes_count = self.mask_sizes_count.astype(int)
        
    def initialize_memory_vectors(self, n_vectors):
        """
        Initializes empty arrays of the correct size here for storing the chosen number of memory vectors.
        """
        if self.relevant_layers_dict is None:
            raise ValueError('Please call initialize_masks before calling initialize_memory_vectors.') 
        
        self.n_vectors = n_vectors
        self.n_stored_in_sequence = 0
        self.memory_vectors = dict()
        
        for i, mask_size in enumerate(self.mask_sizes_count):
            self.memory_vectors[i] = np.zeros((n_vectors, mask_size))
        
    def store_current_gradient(self):
        """
        Obviously, masks and stores the current set of gradients according to previously set parameters.
        """
        if self.memory_vectors is None:
            raise ValueError('Please call initialize_memory_vectors before calling this function.')
        
        current_index = (self.n_stored_in_sequence) % self.n_vectors
        self.n_stored_in_sequence += 1
        
        for key in self.relevant_layers_dict:
            layer = self.relevant_layers_dict[key]['layer']
            
            if self.include_bias:
                grads = np.concatenate((layer.weight.grad.cpu().flatten(), layer.bias.grad.cpu().flatten()))
            else:
                grads = layer.weight.grad.cpu().flatten()
                
            for j in range(len(self.mask_sizes_percent)):
                self.memory_vectors[j][current_index, self.relevant_layers_dict[key]['start_indices'][j]:self.relevant_layers_dict[key]['end_indices'][j]] = grads[self.relevant_layers_dict[key]['masks'][j]]
        
    def return_memory_vectors(self, layer_indices=None):
        """
        Simply returns the stored memory vectors.
        """
        if self.memory_vectors is None:
            raise ValueError('please call initialize_memory_vectors before trying to access them.')
        if (self.n_stored_in_sequence < self.n_vectors):
            warn(f'only {self.n_stored_in_sequence} memory vectors have been stored, but {self.n_vectors} have been requested.')
        
        if layer_indices is None:
            return self.memory_vectors
        else:
            raise NotImplementedError("Obtaining memory vectors for specific layers has not yet been implemented.")
        
    def clear_memory_vectors(self):
        """
        Clears every variable not set in initialize masks.
        """
        self.memory_vectors = None
        self.n_vectors = 0
        self.n_stored_in_sequence = None
                

# TODO: 
#   Implement initialization parameter 

#_______________________________________________________________________________________________________________________________________ 
class Conv2d(nn.Conv2d):
    """
    custom modification of nn.Conv2d that allows the layer to temporarily be called with alternate weights (shifted forward by lr)
    """
    def __init__(self, *args, **kwargs):
        super(Conv2d, self).__init__(*args, **kwargs)
        self.projection_enabled = True
        self.memory_gradient_required = True
    
    def forward(self, input: torch.Tensor, project_lr=None) -> torch.Tensor:
        if project_lr is None:
            return self._conv_forward(input, self.weight, self.bias)
        else:
            # TODO: raise warning if self.weight.grad is None
            return self._conv_forward(input, self.weight - lr*self.weight.grad, self.bias - lr*self.bias.grad)
        
    # This method exists in the parent class, but we've modified it slightly to include a projected bias as well.
    def _conv_forward(self, input, weight, bias):
        if self.padding_mode != 'zeros':
            return F.conv2d(F.pad(input, self._reversed_padding_repeated_twice, mode=self.padding_mode),
                            weight, bias, self.stride,
                            _pair(0), self.dilation, self.groups)
        return F.conv2d(input, weight, bias, self.stride,
                        self.padding, self.dilation, self.groups)
            
    
class Linear(nn.Linear):  
    """
    custom modification of nn.Linear that allows the layer to temporarily be called with alternate weights (shifted forward by lr)
    """
    def __init__(self, *args, **kwargs):
        super(Linear, self).__init__(*args, **kwargs)
        self.projection_enabled = True
        self.memory_gradient_required = True
    
    def forward(self, input: torch.Tensor, project_lr=None) -> torch.Tensor:
        if project_lr is None:
            return F.linear(input, self.weight, self.bias)
        else:
            # TODO: raise warning if self.weight.grad is None
            return F.linear(input, self.weight - lr*self.weight.grad, self.bias - lr*self.bias.grad)
        
class Sequential(nn.Sequential):
    """
    custom modification of nn.Sequential that allows contained layers to be called with a shifted forward learning rate 
    if they are projection enabled
    """
    def __init__(self, *args, **kwargs):
        super(Sequential, self).__init__(*args, **kwargs)
        self.projection_enabled = True
        
    def forward(self, input, project_lr=None):
        for module in self:
            if hasattr(module, 'projection_enabled'):
                input = module(input, project_lr = project_lr)
            else:
                input = module(input)
        return input
    
#___________________________________________________________________________________________________________________________________________
class CReLU(nn.Module):
    def __init__(self):
        super(CReLU, self).__init__()
        self.relu = nn.ReLU()

    def forward(self, input, dim=1):
        return torch.cat([self.relu(input), self.relu(-input)], dim=dim)

   #___________________________________________________________________________________________________________________________________________
class LinearHettingerInit(Linear):
    def __init__(self, in_features, out_features, withCReLU=False, bias=False, orthogonal=True):
        """
        There was some carelessness in treating the bias in Chris's paper. So the initialization scheme may need
        to be tweaked slightly. Specifically, the standard deviation used might need to be adjusted.
        """
        super(LinearHettingerInit, self).__init__(in_features, out_features, bias)
        if bias and withCReLU:
            print("Warning, bias=True breaks linear properties")
        #self.__dict__.update(locals())
        
        if orthogonal:
            nn.init.orthogonal_(self.weight)
        
        if withCReLU:
            in_features = in_features//2
            P = torch.randn(out_features, in_features)/(in_features*out_features)**(0.25)
            N = P
            self.weight.data = torch.cat([P, -N], dim=1)
            
    
    
#___________________________________________________________________________________________________________________________________________
class LooksLinearLayer(Linear):
    def __init__(self, in_features, out_features, bias=False):
        """
        Implemented as in shattered gradients paper with orthogonal weights
        """
        super(LooksLinearLayer, self).__init__(in_features, out_features, False)
        if bias:
            print("Warning, bias=True breaks linear properties; bias set to False.")
        if in_features%2 == 1:
            raise ValueError('parameter in_features must be even for use with CReLU activation function.')
        
        W = self.weight.data[:, :in_features//2] # done in this order for efficiency, previous weight initialized for convenience...
        nn.init.orthogonal_(W)
        self.weight.data = torch.cat([W, -W], dim=1)
            
    
#___________________________________________________________________________________________________________________________________________
# class LinearHettingerInitOld(nn.Module):
#     def __init__(self, in_features, out_features, withCReLU=False, bias=True):
#         """
#         There was some carelessness in treating the bias in Chris's paper. So the initialization scheme may need
#         to be tweaked slightly. Specifically, the standard deviation used might need to be adjusted.
#         """
#         if withCReLU:
#             in_features = in_features//2

#         super(LinearHettingerInitOld, self).__init__()
#         self.__dict__.update(locals())

#         P = torch.randn(out_features, in_features)/(in_features*out_features)**(0.25)
#         b = torch.randn(out_features)/(in_features*out_features)**(0.25)

#         if withCReLU:
#             #temp = nn.Linear(in_features, out_features)
#             #P = temp.weight.data
#             N = P

#             self.W = nn.Linear(2*in_features, out_features, bias=bias)
#             self.W.weight.data = torch.cat([P, -N], dim=1)

#         else:
#             self.W = nn.Linear(in_features, out_features, bias=bias)
#             #self.W.weight.data = P
            
#         #if bias:
#         #    self.W.bias.data = b
    
#     def forward(self, input_):
#         return self.W(input_)
    
#___________________________________________________________________________________________________________________________________________
class LinearNet(gradient_memory_vector_tracking_network):
    def __init__(self, in_layers, hidden_layer_specifications, num_classes, 
                 outf=None,
                 activationClass=CReLU,
                 layerClass=LinearHettingerInit, 
                 batchnorm=True):
        super(LinearNet, self).__init__()
        self.inDims = in_layers
        self.numClasses = num_classes

        layers = [layerClass(in_layers, hidden_layer_specifications[0])]
        
        withCReLU = (activationClass is CReLU)
        input_multiplier = 1
        if withCReLU:
            input_multiplier = 2

        for i in range(len(hidden_layer_specifications)-1):
            channelsin = input_multiplier*hidden_layer_specifications[i]
            channelsout = hidden_layer_specifications[i+1]
       
            if layerClass is LinearHettingerInit:
                layers += [activationClass(), layerClass(channelsin, channelsout, withCReLU=withCReLU)]
            else:
                layers += [activationClass(), layerClass(channelsin, channelsout)]
            if batchnorm:
                layers += [nn.BatchNorm1d(channelsout)]
                
        channelsin = input_multiplier*hidden_layer_specifications[-1]
        if layerClass is LinearHettingerInit:
            layers += [activationClass(), layerClass(channelsin, num_classes, withCReLU=withCReLU)]
        else:
            layers += [activationClass(), layerClass(channelsin, num_classes)]

#         channelsin = channels
#         if activationClass is CReLU:
#             channelsin = 2*channels

#         if layer_class is LinearHettingerInit:
#             withCReLU = (activationClass is CReLU)
#             for i in range(depth - 1):
#                 layers += [activationClass(), layer_class(channelsin, channels, withCReLU=withCReLU)]
#             layers += [activationClass(), layer_class(channelsin, numClasses, withCReLU=withCReLU)]

#         else:
#             for i in range(depth - 1):
#                 layers += [activationClass(), layer_class(channelsin, channels)]
#             layers += [activationClass(), layer_class(channelsin, numClasses)]


        self.layers = nn.ModuleList(layers)
        self.outf = outf

    def forward(self, x, project_lr=None):
        x = x.view(x.shape[0], -1)
        
        for layer in self.layers:
            if hasattr(layer, 'projection_enabled'):
                x = layer(x, project_lr = project_lr)
            else:
                x = layer(x)

        if self.outf is not None:
            x = self.outf(x)

        return x
        
#_______________________________________________________________________________________________________________________________________
class ResBlock(nn.Module):
    """
    A simple ResNet block with optional batch normalization and 2d convolutions. 
    """
    def __init__(self, in_planes, planes, stride=1, batchNorm=True, dropout_prob=.5, initialization=None, beta=.3):
        """"""
        super(ResBlock, self).__init__()
        self.beta = beta
        
        if batchNorm:
            self.main_path =  Sequential(
                                nn.BatchNorm2d(in_planes),
                                Conv2d(in_planes, planes, kernel_size=3, padding=1, stride=stride),
                                nn.BatchNorm2d(planes),
                                nn.ReLU(),
                                Conv2d(planes, planes, kernel_size=3, padding=1, stride=1))
        else:
            self.main_path =  Sequential(
                                  Conv2d(in_planes, planes, kernel_size=3, padding=1, stride=stride),
                                  nn.ReLU(),
                                  Conv2d(planes, planes, kernel_size=3, padding=1, stride=1))
        
        # if dropout_prob is not None, add a dropout layer at the end of the main path
        if dropout_prob:
            self.main_path = Sequential(self.main_path, nn.Dropout(p=dropout_prob))
            
        self.shortcut = Sequential()
        if stride != 1 or in_planes != planes:
            if batchNorm:
                self.shortcut = Sequential(
                        nn.BatchNorm2d(in_planes),
                        Conv2d(in_planes, planes, kernel_size=1, stride=stride))
            else:
                self.shortcut = Sequential(Conv2d(in_planes, planes, kernel_size=1, stride=stride))
        
        self.finalReLU = nn.ReLU()

    def forward(self, x, project_lr=None):
        out = self.beta*self.main_path(x, project_lr=project_lr)
        out += self.shortcut(x, project_lr=project_lr)
        return self.finalReLU(out)

#_______________________________________________________________________________________________________________________________________
class ConvBlock(nn.Module):
    """
    A simple Convolutional block with optional batch normalization and 2d convolutions. 
    """
    def __init__(self, in_planes, planes, stride=1, batchNorm=True, dropout_prob=.5, initialization=None):
        """"""
        super(ConvBlock, self).__init__()
        
        if batchNorm:
            self.main_path =    [nn.BatchNorm2d(in_planes),
                                nn.ReLU(), # TODO: Test This
                                Conv2d(in_planes, planes, kernel_size=3, padding=1, stride=stride),
                                nn.BatchNorm2d(planes),
                                nn.ReLU(),
                                Conv2d(planes, planes, kernel_size=3, padding=1, stride=1)]
        else:
            self.main_path =    [nn.ReLU(), # TODO: Test This
                                Conv2d(in_planes, planes, kernel_size=3, padding=1, stride=stride),
                                nn.ReLU(),
                                Conv2d(planes, planes, kernel_size=3, padding=1, stride=1)]
        
        # if dropout_prob is not None, add a dropout layer at the end of the main path
        if dropout_prob:
            self.main_path += [nn.Dropout(p=dropout_prob)]
        self.main_path = nn.ModuleList(self.main_path)
            
        self.finalReLU = nn.ReLU()

    def forward(self, x, project_lr=None):
        for layer in self.main_path:
            if hasattr(layer, 'projection_enabled'):
                x = layer(x, project_lr=project_lr)
            else:
                x = layer(x)
        return self.finalReLU(x)
    
#_______________________________________________________________________________________________________________________________________ 
class BlockNet(gradient_memory_vector_tracking_network):
    def __init__(self, in_layers, hidden_layer_specifications, num_classes, x_typical,
                 objective_func=None, activation_class=nn.ReLU,
                 dropout_prob=0,
                 batch_norm=True, initialization=None,
                 block_type=ResBlock):
#     def __init__(self, in_layers, num_classes, 
#                      num_blocks, channels, 
#                      block_type=ConvBlock, activation_class=nn.ReLU, 
#                      objective_func=None, dropout_prob=0, batch_norm=False, initialization=None):
        """
        input_:
            hidden_layer_specifications: a vector of integers specifying the layer for each resnet block.
                When a stride other than 1 is desired, that entry should be a tuple where the second input_
                is the desired stride.
        """
        super(BlockNet, self).__init__()
        self.hidden_layer_specifications = hidden_layer_specifications
        self.num_classes = num_classes

        if activation_class is not nn.ReLU:
            print('Only nn.ReLU is compatible with this BlockNet class. Ingoring specification.')
            
        
        # initial layer
        current_planes = self.hidden_layer_specifications[0]
        stride = 1
        if type(current_planes) != int:
            current_planes, stride = current_planes
        
        self.inp = Sequential(Conv2d(in_layers, current_planes, kernel_size=3, padding=1, stride=stride))

        
        # the bulk of the network is composed of these blocks
        blocks = []
        for num_planes in self.hidden_layer_specifications[1:]:
            stride = 1
            if type(num_planes) != int:
                num_planes, stride = num_planes
                
            blocks += [block_type(current_planes, num_planes, stride=stride, 
                 batchNorm=batch_norm, dropout_prob=dropout_prob, initialization=initialization).cuda()]
            current_planes = num_planes

        self.blocks = nn.ModuleList(blocks)
            
        # finally a linear layer and activation function
        #self.avgpool = nn.AdaptiveAvgPool2d(1)
        self.linear = None #nn.Linear(current_planes, num_classes) # defined instead during first forward pass (requires constant input_ size)
        
        if objective_func is not None:
            self.objective_func = objective_func
        else:
            self.objective_func = Sequential()
            
            
        self.cuda()
        
        self.temp_inp = None
        self.temp_blocks = None
        self.temp_linear = None
        
        # finish the initialization (this ensures that the last layer is always added to the optimizer)
        y = self.forward(x_typical)

    def forward(self, x, project_lr=None):
        # initial reshaping convolutional layer
        out = self.inp(x, project_lr=project_lr)
        # print(out.shape)
        
        # blocks 
        for block in self.blocks:
            out = block(out, project_lr = project_lr)
            # print(out.shape)

        # linear and softmax
        #print(out.shape)
        out = out.view(out.shape[0], -1)
        if self.linear is None:
            finalDim = out.shape[1]
            #print(finalDim)
            self.linear = Linear(finalDim, self.num_classes).cuda()
        
        out = self.linear(out, project_lr=project_lr)
        
        return self.objective_func(out)
    
#     def create_temp(self, lr):
#         # this copy is expensive, so we only perform it if the duplicate layers are not intialized
#         if self.temp_inp is None:
#             self.temp_inp = deepcopy(self.inp) # we use deepcopy instead of copy so we don't risk changing the original
#             self.temp_blocks = deepcopy(self.blocks) # TODO: if deepcopy is too expensive consider implementing a custom copy method
#             self.temp_linear = deepcopy(self.linear)
        
#         # set weight values according to provided lr
#         self.temp_inp[0].weight = nn.Parameter(self.inp[0].weight - lr*self.inp[0].weight.grad)
        
#         for i, temp_block in enumerate(self.temp_blocks):
#             for j, temp_layer in enumerate(temp_block.main_path):
#                 if type(temp_layer) is Conv2d:
#                     temp_layer.weight = nn.Parameter(self.blocks[i].main_path[j].weight - lr*self.blocks[i].main_path[j].weight.grad)
                
#             if hasattr(temp_block, 'shortcut'):
#                 for j, temp_layer in enumerate(temp_block.shortcut):
#                     if type(temp_layer) is Conv2d:
#                         temp_layer.weight = nn.Parameter(self.blocks[i].shortcut[j].weight - lr*self.blocks[i].shortcut[j].weight.grad)
                    
#         self.temp_linear.weight = nn.Parameter(self.linear.weight - lr*self.linear.weight.grad)
        
#     def temp_forward(self, x):
#         if self.temp_inp is None:
#             raise RuntimeError("Please call create_temp before calling temp_forward")
        
#         # initial reshaping convolutional layer
#         out = self.temp_inp(x)
#         # print(out.shape)
        
#         # blocks 
#         for block in self.temp_blocks:
#             out = block(out)

#         # linear and softmax
#         out = out.view(out.shape[0], -1)
#         out = self.temp_linear(out)
        
#         return self.objective_func(out)
        
                                  
#     def delete_temp(self):
#         self.temp_inp = None
#         self.temp_blocks = None
#         self.temp_linear = None

#_______________________________________________________________________________________________________________________________________ 
class DenseNet(nn.Module):
    """
    see https://towardsdatascience.com/densenet-2810936aeebb
    A simple DenseNet implementation.
    Includes dropout and transition layers.
    """
    def __init__(self, in_channels=3, start_channels=12, output_classes=5, 
               n_per_block=4, nblocks=3, growthRate=4, bottleneck=False, 
               reduce_constant=1/2, kernel_size_avg_pool=2, padding_avg_pool=0, 
               pooling=False):
        
        super(DenseNet, self).__init__()

        self.output_classes = output_classes
    
        self.inp = Conv2d(in_channels, start_channels, kernel_size=3, padding=1)
        nChannels = start_channels
    
        blocks = []
        for i in range(nblocks):
            layers = []

            for i in range(n_per_block):
                if bottleneck:
                    layers += [BottleneckLayer(nChannels, growthRate)]
                else:
                    layers += [DenseLayer(nChannels, growthRate)]
          
                nChannels += growthRate
      
            # don't use a transition layer after last block
            if (not i==blocks):
                reduceChannels = int(np.floor(nChannels*reduce_constant))
                layers += [TransitionLayer(nChannels, reduceChannels, kernel_size_avg_pool, padding_avg_pool, pooling)]
                nChannels = reduceChannels

            blocks += [Sequential(*layers)]
        
        self.main = Sequential(*blocks)
        
        self.flinear_prep = Sequential(nn.BatchNorm2d(nChannels),
                                       nn.ReLU())
        self.flinear = None
        
        self.log_softm = nn.LogSoftmax(dim=1)
    
    def forward(self, x, project_lr=None):
        out1 = self.inp(x, project_lr=project_lr)
        #print('\nInput Layer:',out1.shape)
        out1 = self.main(out1, project_lr=project_lr)
        #print('Main layer:',out1.shape)
        out1 = self.flinear_prep(out1)
        
        
        # linear initialized here to avoid using nn.AdaptiveAvgPool (which usually removes a ton of information. Especially bad if you have
        # fewer channels immediately prior to final nn.Linear layer than output classes) 
        # this is acceptable if inputs become long and narrow prior to fully connected layer
        if self.flinear is None:
            linear_indim = len(torch.flatten(out1))
            self.flinear = Linear(linear_indim, self.output_classes)
            
        out1 = self.flinear(out1.flatten(), project_lr=project_lr)
        #print('Final layer:',out1.shape)
        #print('Softmax:',self.log_softmax(out1).shape)
        return self.log_softm(out1)
    
#_______________________________________________________________________________________________________________________________________

class DenseLayer(nn.Module):
    """ A simple single layer of the densenet architecture. Concatenates output to input_ in forward function.
    """
    def __init__(self, nchannels, growth_rate):
        super(DenseLayer, self).__init__()
        self.layer = Sequential(nn.BatchNorm2d(nchannels),
                                nn.ReLU(),
                                Conv2d(nchannels, nchannels+growth_rate, kernel_size=3, padding=1))
    
    def forward(self, x, project_lr=None):
        """Calls the Sequential module layer and concatenates it to the end of the input_"""
        #print('Dense layer:',x.shape,'-->',self.layer(x).shape)
        return  torch.cat((x, self.layer(x, project_lr=project_lr)), 1)

#_______________________________________________________________________________________________________________________________________

class TransitionLayer(nn.Module):
    """ This transition layer includes average pooling and a bottleneck (i.e. convolution with kernel size of 1), 
    see paper: https://towardsdatascience.com/densenet-2810936aeebb"""
    def __init__(self, inchannels, outchannels, kernel_size_avg_pool, padding_avg_pool, pooling):
        super(TransitionLayer, self).__init__()
        if pooling:
            self.layer = Sequential(nn.BatchNorm2d(inchannels),
                                    nn.ReLU(),
                                    Conv2d(inchannels, outchannels, kernel_size=1, padding=0),
                                    nn.AvgPool2d(kernel_size_avg_pool, padding=padding_avg_pool))
        else:
            self.layer = Sequential(nn.BatchNorm2d(inchannels),
                                    nn.ReLU(),
                                    Conv2d(inchannels, outchannels, kernel_size=1, padding=0))
            
    def forward(self, x, project_lr=None):
        #print('Transition layer:',x.shape,'-->',self.layer(x).shape)
        return self.layer(x, project_lr=project_lr)

#_______________________________________________________________________________________________________________________________________

class BottleneckLayer(nn.Module):
    """
    see paper: https://towardsdatascience.com/densenet-2810936aeebb
    """
    def __init__(self, inchannels, growth_rate):
        super(BottleneckLayer, self).__init__()
        intermediate_channels = 4*growth_rate

        self.layer = Sequential(nn.BatchNorm2d(inchannels),
                                nn.ReLU(),
                                Conv2d(inchannels, intermediate_channels, kernel_size=1, padding=0),
                                nn.BatchNorm2d(intermediate_channels),
                                nn.ReLU(),
                                Conv2d(intermediate_channels, growth_rate, kernel_size=3, padding=1))
    
    def forward(self, x, project_lr=None):
        print('Bottleneck layer:', x.shape, '-->', self.layer(x).shape)
        return torch.cat((x, self.layer(x, project_lr=project_lr)), 1)