from torch import nn, Tensor
from warnings import warn


# ___________________________________________________________________________________________________________________________________________
class SubstitutionNetworkError(Exception):
    """An exception type used exclusively in the SubstitutionEnabledNetwork class."""


# ___________________________________________________________________________________________________________________________________________
class SubstitutionEnabledNetwork(nn.Module):
    """
    This class in intended to act as a parent for any pytorch network class.

    This class enables evaluation of the forward function using shifted or substitute learnable parameters.

    To accomplish that functionality in an efficient way it also enables the creation of a single model-state checkpoint
    that can be restored at a later time. In the typical use case the checkpoint is created and deleted within the
    substituted-weight forward call to avoid irreversible changes in network state.
    """
    def __init__(self):
        nn.Module.__init__(self)
        self._weight_checkpoint = None
        self._bias_checkpoint = None

    def shifted_forward(self, input_: Tensor,
                        weight_delta: dict = None, tracking_on_shifted_weight: bool = False,
                        bias_delta: dict = None, tracking_on_shifted_bias: bool = False,
                        input_delta: dict = None, tracking_on_shifted_input: bool = False):
        """
        This function is not required to complete the intended core functionality of this class, but it included for
        convenience because several common use cases are with reference to a fixed point. For example: creating loss
        landscape visualizations around an identified minimum, comparing gradients or loss for slightly perturbed input
        values in adversarial robustness studies, or measuring gradient persistence across steps in weight space.
        """
        shifted_values = {'weight': None, 'bias': None, 'input_': None}

        if weight_delta is not None:
            shifted_weight = {module_name: nn.Parameter(weight.detach() + weight_delta[module_name],
                                                        requires_grad=tracking_on_shifted_weight)
                              for module_name, weight in self.weights.items()}
            shifted_values['weight'] = shifted_weight
        else:
            shifted_weight = self.weights

        if bias_delta is not None:
            shifted_bias = {module_name: nn.Parameter(bias.detach() + bias_delta[module_name],
                                                      requires_grad=tracking_on_shifted_bias)
                            for module_name, bias in self.biases.items()}
            shifted_values['bias'] = shifted_bias
        else:
            shifted_bias = self.biases

        if input_delta is not None:
            shifted_input = nn.Parameter(input_.detach() + input_delta, requires_grad=tracking_on_shifted_input)
            shifted_values['input_'] = shifted_input
        else:
            shifted_input = input_

        return self.substitution_forward(shifted_input,
                                         substitute_weights=shifted_weight,
                                         substitute_biases=shifted_bias), shifted_values

    def substitution_forward(self, input_: Tensor,
                             substitute_weights: dict = None,
                             substitute_biases: dict = None):
        """
        Creates a temporary checkpoint of weight and/or bias as needed, changes these to the provided values,
        evaluates the forward function, and then sets everything back to the state it was in beforehand.

        If multiple substitutions are made with a single checkpoint it would be slightly more efficient to only create
        and clear the checkpoints one time, but using this function will guarantee that no mistakes are made
        that result in an irreversible change.
        """
        # create checkpoint
        self._create_checkpoint()

        # assign new weights
        self._overwrite_weights(substitute_weights, substitute_biases)

        # call forward with modified parameters
        out = self(input_)

        # restore original parameters
        self._restore_checkpoint()

        # delete checkpoint
        self._clear_checkpoint()

        return out

    def __getattr__(self, name):
        """
        This handles the special attributes weights and biases separately. These return a list of the weight or bias
        from each submodule.
        """
        if name in ['weights', 'biases']:
            if 'weight' in name:
                name = 'weight'
            if 'bias' in name:
                name = 'bias'

            params = dict()
            for module_name, module in self.named_modules():
                # if this layer doesn't have the specified weight or bias then skip it
                if hasattr(module, name) and module.__getattr__(name) is not None:
                    params[module_name] = module.__getattr__(name)

            return params

        else:
            return super(SubstitutionEnabledNetwork, self).__getattr__(name)

    def __setattr__(self, name, value):
        """
        This allows the weights and biases of submodules to be set using a dictionary of tensors.
        """
        if name not in ['weights', 'biases']:
            super(SubstitutionEnabledNetwork, self).__setattr__(name, value)
        else:
            if 'weight' in name:
                tensor_type = 'weight'
            else:
                tensor_type = 'bias'

            for module_name, module in self.named_modules():
                # if this layer doesn't have the specified weight or bias or if it is None then skip it
                if hasattr(module, tensor_type) and module.__getattr__(tensor_type) is not None:
                    if module_name not in value.keys():
                        raise SubstitutionNetworkError(f'{tensor_type} missing for module {module_name}.')
                    module.__setattr__(tensor_type, value[module_name])

    def _create_checkpoint(self):
        """Create a checkpoint of the current weight and bias."""
        if self._weight_checkpoint or self._bias_checkpoint:
            warn("A previously saved checkpoint is being overwritten. That network state will not be recoverable.")

        self._weight_checkpoint = self.weights
        self._bias_checkpoint = self.biases

    def _overwrite_weights(self, new_weight: dict, new_bias: dict):
        """
        Overwrite the current network weights and bias with the provided weight and bias. No warning is provided if
        new_bias is None as that is common for many layer types.
        """
        if self._weight_checkpoint is None:
            warn("There was no saved weight checkpoint so this weight reassignment may be irreversible. This class " +
                 "is intended for temporary weight reassignment only.")

        self.weights = new_weight
        self.biases = new_bias

    def _clear_checkpoint(self):
        """Erase the saved weight and bias checkpoint."""
        self._weight_checkpoint = None
        self._bias_checkpoint = None

    def _restore_checkpoint(self):
        """Restore the network weights and bias to the last saved checkpoint."""
        if self._weight_checkpoint is None:
            raise SubstitutionNetworkError("No checkpoint exists to be restored.")

        self.weights = self._weight_checkpoint
        self.biases = self._bias_checkpoint


# ______________________________________________________________________________________________________________________
class Sequential(nn.Sequential):
    """
    custom modification of nn.Sequential that doesn't overwrite the input in forward call to enable requires_grad=True
    on the input
    """
    def __init__(self, *args):
        nn.Sequential.__init__(self, *args)

    def forward(self, input_):
        out = None
        for module in self:
            if out is not None:
                out = module(out)
            else:
                out = module(input_)

        return out
