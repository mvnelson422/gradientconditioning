from __future__ import annotations

from torch import nn, Tensor
from numpy.typing import ArrayLike

from .Tracking_and_Substitutions import Sequential
from .Gradient_Masking_and_Memory import GradientMaskAndMemoryEnabledNetwork


# ______________________________________________________________________________________________________________________
class ResBlock(nn.Module):
    """
    A simple ResNet block with optional batch normalization and 2d convolutions. 
    """

    def __init__(self, in_planes, planes, stride=1, group_norm=True, dropout_prob: float = 0, initialization=None,
                 beta: float = .3, groups_per_channel_normalization: int = 2):
        """"""
        super(ResBlock, self).__init__()
        self.beta = beta

        if initialization is not None:
            raise ValueError("Parameter 'initialization' is not yet implemented for class ResBlock.")

        # define main path
        if group_norm:
            self.main_path = [
                nn.GroupNorm(in_planes // groups_per_channel_normalization, in_planes),
                nn.Conv2d(in_planes, planes, kernel_size=3, padding=1, stride=stride),
                nn.GroupNorm(planes // groups_per_channel_normalization, planes),
                nn.ReLU(),
                nn.Conv2d(planes, planes, kernel_size=3, padding=1, stride=1)]
        else:
            self.main_path = [
                nn.Conv2d(in_planes, planes, kernel_size=3, padding=1, stride=stride),
                nn.ReLU(),
                nn.Conv2d(planes, planes, kernel_size=3, padding=1, stride=1)]

        # if dropout_prob is not None, add a dropout layer at the end of the main path
        if dropout_prob:
            self.main_path += [nn.Dropout(p=dropout_prob)]

        self.main_path = nn.Sequential(*self.main_path)

        # define shortcut path
        if stride != 1 or in_planes != planes:
            shortcut_elements = []
            if group_norm:
                shortcut_elements += [nn.GroupNorm(in_planes // groups_per_channel_normalization, in_planes)]
            shortcut_elements += [nn.Conv2d(in_planes, planes, kernel_size=1, stride=stride)]

            self.shortcut = Sequential(*shortcut_elements)
        else:
            self.shortcut = None

        self.finalReLU = nn.ReLU()

    def forward(self, x):
        out = self.beta * self.main_path(x)

        if self.shortcut:
            out += self.shortcut(x)
        else:
            out += x

        return self.finalReLU(out)


# ______________________________________________________________________________________________________________________
class ConvBlock(nn.Module):
    """
    A simple Convolutional block with optional batch normalization and 2d convolutions. 
    """

    def __init__(self, in_planes, planes, stride=1, group_norm=True, dropout_prob: float = 0.0, initialization=None,
                 groups_per_channel_normalization=2):
        """"""
        super(ConvBlock, self).__init__()

        if initialization is not None:
            raise ValueError("Parameter 'initialization' is not yet implemented for class ConvBlock.")

        if group_norm:
            self.main_path = [nn.GroupNorm(in_planes // groups_per_channel_normalization, in_planes),
                              nn.ReLU(),
                              nn.Conv2d(in_planes, planes, kernel_size=3, padding=1, stride=stride),
                              nn.GroupNorm(planes // groups_per_channel_normalization, planes),
                              nn.ReLU(),
                              nn.Conv2d(planes, planes, kernel_size=3, padding=1, stride=1)]
        else:
            self.main_path = [nn.ReLU(),
                              nn.Conv2d(in_planes, planes, kernel_size=3, padding=1, stride=stride),
                              nn.ReLU(),
                              nn.Conv2d(planes, planes, kernel_size=3, padding=1, stride=1)]

        # if dropout_prob is not None, add a dropout layer at the end of the main path
        if dropout_prob:
            self.main_path += [nn.Dropout(p=dropout_prob)]

        self.main_path = Sequential(*self.main_path)

        self.finalReLU = nn.ReLU()

    def forward(self, x):
        out = self.main_path(x)
        return self.finalReLU(out)


# ______________________________________________________________________________________________________________________
class BlockNet(GradientMaskAndMemoryEnabledNetwork):
    def __init__(self, input_planes: int, n_blocks: int, num_classes: int, x_typical: Tensor,
                 block_channels: ArrayLike[int] | int, block_strides: ArrayLike[int] | int = None,
                 objective_func=None, dropout_prob: float = 0.0,
                 group_norm: bool = True, initialization=None,
                 block_type=ResBlock, begin_with_batchnorm: bool = True, cuda: bool = True,
                 groups_per_channel_normalization: int = 2,
                 memory_enabled: bool = False):
        """
        input_:
            hidden_layer_channels: a vector of integers specifying the number of channels for the first layer and
                each block of two layers after that.
        """
        GradientMaskAndMemoryEnabledNetwork.__init__(self, memory_enabled=memory_enabled)

        # Should I create settings that allow kernel size and padding to change? Answer, no.
        # Dimension of the input can already be manipulated using stride, and it would create complications

        if type(block_channels) != int:
            if len(block_channels) != n_blocks:
                raise ValueError('If # channels is not uniform it must be specified for every block.')
        else:
            block_channels = [block_channels] * n_blocks

        if block_strides is None:
            block_strides = 1

        if type(block_strides) is int:
            block_strides = [block_strides] * n_blocks
        elif len(block_strides) != n_blocks:
            raise ValueError('If stride is not uniform it must be specified for every block.')

        self.num_classes = num_classes
        self.n_layers = 2 * n_blocks + 2

        # initial layer
        current_planes = input_planes

        # A single conv layer is required here because each block begins with the activation function
        # No stride other than 1 is allowed here
        if begin_with_batchnorm:
            self.inp = [nn.GroupNorm(input_planes // groups_per_channel_normalization, input_planes),
                        nn.Conv2d(input_planes, current_planes, kernel_size=3, padding=1, stride=1)]
        else:
            self.inp = [nn.Conv2d(input_planes, current_planes, kernel_size=3,
                                  padding=1, stride=1)]

        self.inp = nn.Sequential(*self.inp)

        # the bulk of the network is composed of these blocks
        blocks = []
        for k in range(n_blocks):
            blocks += [block_type(current_planes, block_channels[k], stride=block_strides[k],
                                  group_norm=group_norm, dropout_prob=dropout_prob, initialization=initialization)]
            current_planes = block_channels[k]

        self.blocks = Sequential(*blocks)

        # finally a linear layer and activation function
        self.linear = None  # defined during first forward pass (requires constant input_ size)

        if objective_func is not None:
            self.objective_func = objective_func
        else:
            self.objective_func = Sequential()

        self.input_size = x_typical.shape

        # finish the initialization (this ensures that the last layer is always added to the optimizer)
        if cuda:
            self.cuda()
        _ = self.forward(x_typical, initialize_linear=True, initialize_cuda=cuda)

    def forward(self, x: Tensor, initialize_linear=False, initialize_cuda=True):
        if x.shape[1:] != self.input_size[1:]:
            raise ValueError('Input size is not the same as x_typical network was initialized with, and final linear ' +
                             'layer will break.')

        # initial reshaping convolutional layer
        out = self.inp(x)
        # print(out.shape)

        # blocks
        out = self.blocks(out)

        # linear and softmax
        # print(out.shape)
        out = out.view(out.shape[0], -1)

        if initialize_linear:
            final_dim = out.shape[1]
            # print(final_dim)
            self.linear = nn.Linear(final_dim, self.num_classes)

            # if self.individual_grads_enabled:
            #     self.linear = GradSampleModule(self.linear)

            if initialize_cuda:
                self.linear.cuda()

        out = self.linear(out)

        return self.objective_func(out)
