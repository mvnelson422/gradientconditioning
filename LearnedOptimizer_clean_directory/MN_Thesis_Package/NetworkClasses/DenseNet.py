import torch
from torch import nn
import numpy as np

from Tracking_and_Substitutions import SubstitutionEnabledNetwork, Sequential
from Gradient_Masking_and_Memory import GradientMaskAndMemoryEnabledNetwork


# _______________________________________________________________________________________________________________________________________
class DenseNet(SubstitutionEnabledNetwork, GradientMaskAndMemoryEnabledNetwork):
    """
    see https://towardsdatascience.com/densenet-2810936aeebb
    A simple DenseNet implementation.
    Includes dropout and transition layers.
    """

    def __init__(self, in_channels=3, start_channels=12, output_classes=5,
                 n_per_block=4, n_blocks=3, growth_rate=4, bottleneck=False,
                 reduce_constant=1 / 2, kernel_size_avg_pool=2, padding_avg_pool=0,
                 pooling=False):
        SubstitutionEnabledNetwork.__init__(self)
        GradientMaskAndMemoryEnabledNetwork.__init__(self)

        self.output_classes = output_classes

        self.inp = nn.Conv2d(in_channels, start_channels, kernel_size=3, padding=1)
        n_channels = start_channels

        blocks = []
        for i in range(n_blocks):
            layers = []

            for j in range(n_per_block):
                if bottleneck:
                    layers += [BottleneckLayer(n_channels, growth_rate)]
                else:
                    layers += [DenseLayer(n_channels, growth_rate)]

                n_channels += growth_rate

            # don't use a transition layer after last block
            if not i == blocks:
                reduce_channels = int(np.floor(n_channels * reduce_constant))
                layers += [TransitionLayer(n_channels, reduce_channels,
                                           kernel_size_avg_pool, padding_avg_pool,
                                           pooling)]
                n_channels = reduce_channels

            blocks += [Sequential(*layers)]

        self.main = Sequential(*blocks)

        self.f_linear_prep = Sequential(nn.BatchNorm2d(n_channels),
                                        nn.ReLU())
        self.f_linear = None

        self.log_softmax = nn.LogSoftmax(dim=1)

    def forward(self, x, project_lr=None):
        out1 = self.inp(x, project_lr=project_lr)
        # print('\nInput Layer:',out1.shape)
        out1 = self.main(out1, project_lr=project_lr)
        # print('Main layer:',out1.shape)
        out1 = self.f_linear_prep(out1)

        # linear initialized here to avoid using nn.AdaptiveAvgPool
        # (which usually removes a ton of information. Especially bad if you have
        # fewer channels immediately prior to final nn.Linear layer than output classes) 
        # this is acceptable if inputs become long and narrow prior to fully connected layer
        if self.f_linear is None:
            linear_in_dim = len(torch.flatten(out1))
            self.f_linear = nn.Linear(linear_in_dim, self.output_classes)

        out1 = self.f_linear(out1.flatten(), project_lr=project_lr)
        # print('Final layer:',out1.shape)
        # print('Softmax:',self.log_softmax(out1).shape)
        return self.log_softmax(out1)


# _______________________________________________________________________________________________________________________________________

class DenseLayer(nn.Module):
    """ A simple single layer of the densenet architecture. Concatenates output to input_ in forward function.
    """

    def __init__(self, n_channels, growth_rate):
        super(DenseLayer, self).__init__()
        self.layer = Sequential(nn.BatchNorm2d(n_channels),
                                nn.ReLU(),
                                nn.Conv2d(n_channels, n_channels + growth_rate, kernel_size=3, padding=1))

    def forward(self, x, project_lr=None):
        """Calls the Sequential module layer and concatenates it to the end of the input_"""
        # print('Dense layer:',x.shape,'-->',self.layer(x).shape)
        return torch.cat((x, self.layer(x, project_lr=project_lr)), 1)


# _______________________________________________________________________________________________________________________________________

class TransitionLayer(nn.Module):
    """ This transition layer includes average pooling and a bottleneck (i.e. convolution with kernel size of 1), 
    see paper: https://towardsdatascience.com/densenet-2810936aeebb"""

    def __init__(self, in_channels, out_channels, kernel_size_avg_pool, padding_avg_pool, pooling):
        super(TransitionLayer, self).__init__()
        if pooling:
            self.layer = Sequential(nn.BatchNorm2d(in_channels),
                                    nn.ReLU(),
                                    nn.Conv2d(in_channels, out_channels, kernel_size=1, padding=0),
                                    nn.AvgPool2d(kernel_size_avg_pool, padding=padding_avg_pool))
        else:
            self.layer = Sequential(nn.BatchNorm2d(in_channels),
                                    nn.ReLU(),
                                    nn.Conv2d(in_channels, out_channels, kernel_size=1, padding=0))

    def forward(self, x, project_lr=None):
        # print('Transition layer:',x.shape,'-->',self.layer(x).shape)
        return self.layer(x, project_lr=project_lr)


# _______________________________________________________________________________________________________________________________________

class BottleneckLayer(nn.Module):
    """
    see paper: https://towardsdatascience.com/densenet-2810936aeebb
    """

    def __init__(self, in_channels, growth_rate):
        super(BottleneckLayer, self).__init__()
        intermediate_channels = 4 * growth_rate

        self.layer = Sequential(nn.BatchNorm2d(in_channels),
                                nn.ReLU(),
                                nn.Conv2d(in_channels, intermediate_channels, kernel_size=1, padding=0),
                                nn.BatchNorm2d(intermediate_channels),
                                nn.ReLU(),
                                nn.Conv2d(intermediate_channels, growth_rate, kernel_size=3, padding=1))

    def forward(self, x, project_lr=None):
        print('Bottleneck layer:', x.shape, '-->', self.layer(x).shape)
        return torch.cat((x, self.layer(x, project_lr=project_lr)), 1)
