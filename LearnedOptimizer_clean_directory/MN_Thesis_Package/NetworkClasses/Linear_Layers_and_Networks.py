from __future__ import annotations

from torch import nn, Tensor, cat, einsum
from torch.nn import functional as F
from opacus.grad_sample import register_grad_sampler
from typing import Dict

from .Tracking_and_Substitutions import Sequential  # , SubstitutionEnabledNetwork
from .Gradient_Masking_and_Memory import GradientMaskAndMemoryEnabledNetwork


# ______________________________________________________________________________________________________________________
# def empty_wrapper(input_function):
#     def wrapper(*args, **kwargs):
#         return input_function(*args, **kwargs)
#     return wrapper


# ______________________________________________________________________________________________________________________
class CReLU(nn.Module):
    """
    Concatenated ReLU activation function
    """

    def __init__(self):
        super(CReLU, self).__init__()
        self.relu = nn.ReLU()

    def forward(self, input_: Tensor, dim: int = 1):
        return cat([self.relu(input_), self.relu(-input_)], dim=dim)


# ______________________________________________________________________________________________________________________
class CReLULinearLayer(nn.Linear):
    def __init__(self, in_features: int, out_features: int,
                 bias: bool = False, pre_act_f: bool = True, norm_type: str | None = 'batchnorm',
                 device=None, dtype=None, symmetrical: bool = False):
        """
        Implemented as in shattered gradients paper with orthogonal weights
        """
        if bias and symmetrical:
            raise ValueError('Cannot use a bias with symmetric initialization.')

        nn.Linear.__init__(self, in_features, out_features // 2, bias=bias, device=device, dtype=dtype)

        # done in this order for efficiency, previous weight initialized for convenience...
        if symmetrical:
            w = self.weight.data[:, :in_features // 2]
            nn.init.orthogonal_(w)
            self.weight.data = cat([w, -w], dim=1)

        self.act_f = None
        if pre_act_f:
            self.act_f = CReLU()

        self.norm = None
        if norm_type in ['batchnorm']:
            self.norm = nn.BatchNorm1d(self.out_features)
        elif norm_type in ['layernorm']:
            self.norm = nn.LayerNorm(self.out_features)

    def forward(self, input_: Tensor) -> Tensor:
        if self.act_f:
            out = F.linear(self.act_f(input_), self.weight, self.bias)
        else:
            out = F.linear(input_, self.weight, self.bias)

        if self.norm is not None:
            return self.norm(out)
        else:
            return out


# ______________________________________________________________________________________________________________________
class LooksLinearLayer(CReLULinearLayer):
    def __init__(self, in_features: int, out_features: int,
                 bias: bool = False, pre_act_f: bool = True,
                 norm_type: str | None = 'batchnorm', device=None, dtype=None):
        CReLULinearLayer.__init__(self, in_features, out_features, bias=bias, pre_act_f=pre_act_f,
                                  norm_type=norm_type, device=device, dtype=dtype, symmetrical=True)


# ______________________________________________________________________________________________________________________
class OrthoLinear(nn.Linear):
    def __init__(self, in_features: int, out_features: int, bias: bool = False, pre_act_f: bool = True,
                 norm_type: str | None = 'batchnorm', device=None, dtype=None, use_crelu=False):
        # print(in_features, out_features, bias, device, dtype)
        nn.Linear.__init__(self, in_features, out_features, bias=bias, device=device, dtype=dtype)
        nn.init.orthogonal_(self.weight.data)

        self.act_f = None
        if pre_act_f and use_crelu:
            self.act_f = CReLU()
        else:
            self.act_f = nn.ReLU()

        self.norm_after = None
        if norm_type in ['batchnorm']:
            self.norm_after = nn.BatchNorm1d(self.out_features)
        elif norm_type in ['layernorm']:
            self.norm_after = nn.LayerNorm(self.out_features)

    def forward(self, input_: Tensor) -> Tensor:
        if self.act_f:
            out = nn.Linear.forward(self, self.act_f(input_))
        else:
            out = nn.Linear.forward(self, input_)
        if self.norm_after is not None:
            return self.norm_after(out)
        else:
            return out


# ______________________________________________________________________________________________________________________
@register_grad_sampler((nn.Linear, CReLULinearLayer, LooksLinearLayer, OrthoLinear))
def compute_linear_grad_sample(
        layer: nn.Linear | CReLULinearLayer | LooksLinearLayer | OrthoLinear,
        activations: Tensor, backprops: Tensor) -> Dict[nn.Parameter, Tensor]:
    """
    Computes per sample gradients for ``nn.Linear`` layer
    Args:
        layer: Layer
        activations: Activations
        backprops: Backpropagations

    Taken from the opacus module and overwritten here to make sure the grad sampler inherits to the subclasses defined
    in this file
    """
    gs = einsum("n...i,n...j->nij", backprops, activations)
    ret = {layer.weight: gs}
    if layer.bias is not None:
        ret[layer.bias] = einsum("n...k->nk", backprops)

    return ret

# ______________________________________________________________________________________________________________________
class LinearNet(GradientMaskAndMemoryEnabledNetwork):  # noqa
    def __init__(self, input_size_flattened, hidden_layer_specifications, num_classes,
                 out_f=None, layer_class=LooksLinearLayer,
                 norm_type: str | None = 'batchnorm', begin_with_batchnorm=True, cuda=True,
                 memory_enabled=False, x_typical=None):
        GradientMaskAndMemoryEnabledNetwork.__init__(self, memory_enabled=memory_enabled)

        self.inDims = input_size_flattened
        self.numClasses = num_classes

        use_crelu_in_final = layer_class in [LooksLinearLayer, CReLULinearLayer]

        if begin_with_batchnorm:
            if norm_type in ['layernorm']:
                layers = [nn.LayerNorm(input_size_flattened),
                          layer_class(input_size_flattened, hidden_layer_specifications[0],
                                      pre_act_f=False, norm_type=norm_type)]
            else:
                layers = [nn.BatchNorm1d(input_size_flattened),
                          layer_class(input_size_flattened, hidden_layer_specifications[0],
                                      pre_act_f=False, norm_type=norm_type)]
        else:
            layers = [layer_class(input_size_flattened, hidden_layer_specifications[0],
                                  pre_act_f=False, norm_type=norm_type)]

        for i in range(len(hidden_layer_specifications) - 1):
            channels_in = hidden_layer_specifications[i]
            channels_out = hidden_layer_specifications[i + 1]
            layers += [layer_class(channels_in, channels_out, pre_act_f=True, norm_type=norm_type)]

        channels_in = hidden_layer_specifications[-1]
        layers += [OrthoLinear(channels_in, num_classes, pre_act_f=True, use_crelu=use_crelu_in_final, norm_type=None)]

        self.layers = Sequential(*layers)
        self.out_f = out_f

        # finish the initialization
        # this ensures that grads and things are populated correctly before anything else is called
        if cuda:
            self.cuda()
        _ = self.forward(x_typical)

    def forward(self, x: Tensor):
        out = x.view(x.shape[0], -1)

        out = self.layers(out)

        if self.out_f is not None:
            out = self.out_f(out)

        return out
