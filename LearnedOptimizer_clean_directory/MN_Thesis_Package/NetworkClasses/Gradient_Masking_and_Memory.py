from torch import nn, flatten, cat, Tensor, zeros, from_numpy, clone, rand_like, masked_select, rand, gather
from torch import bool as t_bool
import numpy as np
from warnings import warn
from opacus import GradSampleModule
from typing import TypeVar

from .Tracking_and_Substitutions import SubstitutionEnabledNetwork
from collections import OrderedDict

T = TypeVar('T', bound='Module')


# ______________________________________________________________________________________________________________________
class MaskError(Exception):
    """An exception used in the various mask classes defined in Gradient_Masking_and_Memory.py."""

# ______________________________________________________________________________________________________________________
# class FasterParameterMask:
#
#     def __init__(self, percent_included: float, model: nn.Module, name: str,
#                  parameter_types_to_include: tuple[str] = ('weight', 'bias'),
#                  requires_grad_only: bool = False, layer_types_to_exclude: tuple = tuple(),
#                  device='cuda:0', method: int = 1):
#         # only needed until initialized
#         self.layer_types_to_exclude = layer_types_to_exclude
#         self.requires_grad_only = requires_grad_only
#         self.percent_included = percent_included
#
#         # needed always
#         self.name = name
#         self.model = model
#         self.device = device
#         self.length = None
#         self.method = method
#
#         # constructed in initialize method
#         self.param_masks = OrderedDict()
#         for param_type in parameter_types_to_include:
#             self.param_masks[param_type] = dict()
#             self.param_masks[param_type]['module_names'] = []
#             self.param_masks[param_type]['length'] = []
#             self.param_masks[param_type]['masks'] = []
#
#     def __str__(self):
#         return self.name
#
#     def initialize_submasks(self):
#
#         for module_name, module in self.model.named_modules():
#             for param_name in self.param_masks.keys():
#                 if type(module) not in self.layer_types_to_exclude and hasattr(module, param_name):
#                     if not self.requires_grad_only or module.__getattr__(param_name).requires_grad:
#                         (mask, length) = self.initialize_mask_for_submodule(module, self.percent_included, param_name)
#                         self.param_masks[param_name]['module_names'] += [module_name]
#                         self.param_masks[param_name]['length'] += [length]
#                         self.param_masks[param_name]['masks'] += [mask]
#
#         self.length = 0
#         for param_name in self.param_masks.keys():
#             length = np.sum(self.param_masks[param_name]['length'])
#             self.param_masks[param_name]['end_indices'] = np.cumsum(self.param_masks[param_name]['length'])
#             self.param_masks[param_name]['length'] = length
#             self.param_masks[param_name]['end_indices'] += self.length
#             self.length += length
#
#     def __len__(self):
#         return self.length
#
#     def apply_flat_mask(self, grad_instead=False, individual_grads=False, stack=True, width: int = 1,
#                         **parameter_values):
#         attribute_name = None
#         if individual_grads:
#             attribute_name = 'grad_sample'
#         elif grad_instead:
#             attribute_name = 'grad'
#
#         result = zeros((width, self.length))
#
#         last_end_index = 0
#         for param, param_settings in self.param_masks.items():
#             if param not in parameter_values.keys():
#                 raise MaskError('input missing named input {param}')
#             for i, mask in enumerate(param_settings['masks']):
#                 module_name = param_settings['module_names'][i]
#                 parameter = parameter_values[param][module_name]
#                 end_index = param_settings['end_indices'][i]
#                 result[:, last_end_index:end_index] = self.apply_mask_to_parameter(parameter, mask,
#                                                                                    attribute_name, width)
#                 last_end_index = end_index
#
#         return result
#
#     def __call__(self, grad_instead=False, individual_grads=False, stack=True, width: int = 1):
#         modules = {module_name: module for module_name, module in self.model.named_modules()}
#
#         attribute_name = None
#         if individual_grads:
#             attribute_name = 'grad_sample'
#         elif grad_instead:
#             attribute_name = 'grad'
#
#         if width > 1:
#             result = zeros((width, self.length), device='cuda:0')
#         else:
#             result = zeros(self.length, device='cuda:0')
#
#         last_end_index = 0
#         for param, param_settings in self.param_masks.items():
#             for i, mask in enumerate(param_settings['masks']):
#                 module = modules[param_settings['module_names'][i]]
#                 end_index = param_settings['end_indices'][i]
#                 if width > 1:
#                     result[:, last_end_index:end_index] = self.apply_mask_to_submodule(module, param, mask,
#                                                                                        attribute_name, width)
#                 else:
#                     result[last_end_index:end_index] = self.apply_mask_to_submodule(module, param, mask,
#                                                                                     attribute_name, width)
#                 last_end_index = end_index
#
#         return result
#
#     # @classmethod
#     def apply_mask_to_submodule(self, submodule, param_name: str, mask: Tensor, attribute_name: str = None,
#                                 width: int = 1):
#         """To maximize efficiency mask should be a torch tensor of torch bools on the same device as the model."""
#         return self.apply_mask_to_parameter(submodule.__getattr__(param_name), mask, attribute_name, width)
#
#     # @staticmethod
#     def apply_mask_to_parameter(self, parameter: Tensor, mask: Tensor, attribute_name: str = None, width: int = 1):
#         """To maximize efficiency mask should be a torch tensor of torch bools on the same device as the model."""
#         if attribute_name:
#             to_process = parameter.__getattribute__(attribute_name)
#         else:
#             to_process = parameter
#
#         if self.method == 1:
#             if width != 1:
#                 return cat([masked_select(el, mask).unsqueeze(0) for el in to_process[:width]])
#                 # return gather(to_process, 1, mask)
#             else:
#                 return masked_select(to_process, mask)
#         else:
#             if width != 1:
#                 to_process = cat([flatten(el).unsqueeze(0) for el in to_process])
#                 return to_process[:width, mask]
#             else:
#                 return to_process[mask]
#
#     # @staticmethod
#     def initialize_mask_for_submodule(self, submodule, percent_included, param_name: str):
#         if percent_included == 1:
#             return np.index_exp[:], np.product(submodule.__getattr__(param_name).shape)
#         elif self.method == 1:
#             mask = rand_like(submodule.__getattr__(param_name), device=self.device).le(percent_included).to(t_bool)
#             return mask, mask.sum().item()
#         else:
#             to_mask = submodule.__getattr__(param_name)
#             mask = rand(len(to_mask), device=self.device).le(percent_included).nonzero(as_tuple=False).reshape(-1)
#             return mask, len(mask)


# ______________________________________________________________________________________________________________________
class ParametersMask:
    """
    This mask class remembers a model and will return a mask for that model with the specified initialization
    features when called.
    """
    def __init__(self, percent_included: float, model: nn.Module, name: str,
                 parameter_types_to_include: tuple[str] = ('weight', 'bias'),
                 requires_grad_only: bool = False, layer_types_to_exclude: tuple = tuple(),
                 device='cuda:0'):
        if percent_included <= 0 or percent_included > 1:
            raise MaskError('Percent included must be greater than 0 and less than or equal to 1.')

        self.percent_included = percent_included
        self.model = model
        self.parameter_types_to_include = parameter_types_to_include
        self.requires_grad_only = requires_grad_only
        self.layer_types_to_exclude = layer_types_to_exclude
        self.sub_masks = None
        self.lengths = None
        self.length = None
        self.grad_compatible = True
        self.name = name
        self.device = device
        # self.flat_masks = None

    def __str__(self):
        return self.name

    def initialize_submasks(self):
        self.sub_masks = OrderedDict()
        self.length = 0
        self.lengths = dict()

        for param_name in self.parameter_types_to_include:
            self.lengths[param_name] = 0

        for module_name, module in self.model.named_modules():

            if type(module) not in self.layer_types_to_exclude:
                self.sub_masks[module_name] = OrderedDict()

                for param_name in self.parameter_types_to_include:
                    if hasattr(module, param_name) and module.__getattr__(param_name) is not None:
                        if not hasattr(module.__getattr__(param_name), 'grad'):
                            if self.grad_compatible:
                                self.grad_compatible = False
                            print(f'A masked parameter does not have a grad attribute: ' +
                                  f'{module_name}.{param_name}: {module.__getattr__(param_name).shape}')

                        elif self.requires_grad_only:
                            # this first statement should never be true
                            if not hasattr(module.__getattr__(param_name), 'requires_grad'):
                                raise MaskError('requires_grad is true, but model submodule ' +
                                                f"{module_name} doesn't have a requires_grad parameter")
                            elif not module.__getattr__(param_name).requires_grad:
                                continue

                        self.sub_masks[module_name][param_name] = TensorMask(self.percent_included,
                                                                             module.__getattr__(param_name),
                                                                             device=self.device)
                        self.lengths[param_name] += len(self.sub_masks[module_name][param_name])
                        self.length += len(self.sub_masks[module_name][param_name])
                        # self.flat_masks[param_name] = np.append(self.flat_masks[param_name],
                        #                                         self.sub_masks[module_name][param_name].mask)

                # remove key if no layers require masking
                if len(self.sub_masks[module_name].keys()) == 0:
                    del self.sub_masks[module_name]

        to_remove = []
        for param_name in self.parameter_types_to_include:
            if self.lengths[param_name] == 0:
                to_remove += [param_name]
                del self.lengths[param_name]
        self.parameter_types_to_include = list(set(self.parameter_types_to_include) - set(to_remove))

    def __len__(self):
        if self.length is None:
            self.initialize_submasks()

        return self.length

    def apply_flat_mask(self, grad_instead=False, individual_grads=False, stack=True, n_opacus_samples: int = 1,
                        **parameter_values):
        """
        For every value in self.parameter_types_to_include input a dictionary with that parameter (or its grads)
        indexed by the module name.
        """
        if self.sub_masks is None:
            self.initialize_submasks()

        for param in self.parameter_types_to_include:
            if param not in parameter_values.keys():
                raise MaskError(f'Parameter {param} is missing from input.')

        masked_tensors = {param_name: [] for param_name in self.parameter_types_to_include}

        for module_name, module in self.model.named_modules():
            if module_name in self.sub_masks.keys():
                for param in self.sub_masks[module_name].keys():
                    if grad_instead:
                        if individual_grads:
                            masked_tensors[param] += [self.sub_masks[module_name]
                                                      [param].apply_mask(parameter_values[param][module_name]
                                                                         .grad_sample,
                                                                         individual_grads=True,
                                                                         n_opacus_samples=n_opacus_samples)]
                        else:
                            masked_tensors[param] += [self.sub_masks[module_name]
                                                      [param].apply_mask(parameter_values[param][module_name].grad,
                                                                         individual_grads=False)]

                    else:
                        masked_tensors[param] += [self.sub_masks[module_name]
                                                  [param].apply_mask(parameter_values[param][module_name],
                                                                     individual_grads=individual_grads,
                                                                     n_opacus_samples=n_opacus_samples)]

        return_dict = dict()
        for param_name in self.parameter_types_to_include:
            if individual_grads:
                return_dict[param_name] = cat(masked_tensors[param_name], dim=1)
            else:
                return_dict[param_name] = cat(masked_tensors[param_name])

        if stack:
            if individual_grads:
                return cat([return_dict[param_name] for param_name in self.parameter_types_to_include], dim=1)
            else:
                return cat([return_dict[param_name] for param_name in self.parameter_types_to_include])
        else:
            return return_dict

    def __call__(self, grad_instead=False, individual_grads=False, stack=True, n_opacus_samples: int = None):
        if self.sub_masks is None:
            self.initialize_submasks()

        if grad_instead and not self.grad_compatible:
            raise MaskError('This mask includes some parameters that do not have a grad attribute.')

        masked_tensors = {param_name: [] for param_name in self.parameter_types_to_include}
        # masked_tensors = {param_name: zeros(self.lengths[param_name])
        #                   for param_name in self.parameter_types_to_include}
        # filled = {param_name: 0 for param_name in self.parameter_types_to_include}

        for module_name, module in self.model.named_modules():
            if module_name in self.sub_masks.keys():
                for param in self.sub_masks[module_name].keys():
                    # el = self.sub_masks[module_name][param](module.__getattr__(param),
                    #                                         grad_instead=grad_instead,
                    #                                         individual_grads=individual_grads)
                    # masked_tensors[param][filled[param]:filled[param]+len(el)] = el
                    # filled[param] += len(el)
                    masked_tensors[param] += [self.sub_masks[module_name][param](module.__getattr__(param),
                                                                                 grad_instead=grad_instead,
                                                                                 individual_grads=individual_grads,
                                                                                 n_opacus_samples=n_opacus_samples)]

        return_dict = dict()
        for param_name in self.parameter_types_to_include:
            if individual_grads:
                return_dict[param_name] = cat(masked_tensors[param_name], dim=1)
            else:
                return_dict[param_name] = cat(masked_tensors[param_name])

        if stack:
            if individual_grads:
                return cat([return_dict[param_name] for param_name in self.parameter_types_to_include], dim=1)
            else:
                return cat([return_dict[param_name] for param_name in self.parameter_types_to_include])
        else:
            return return_dict


# ______________________________________________________________________________________________________________________
class TensorMask:
    def __init__(self, percent_included: float, sample_tensor: Tensor = None, device='cuda:0'):
        if percent_included < 0 or percent_included > 1:
            raise MaskError('Percent included must be between 0 and 1 inclusive.')

        self.percent_included = percent_included
        self.device = device
        self.input_length = None
        self.length = None
        self.mask = None

        if sample_tensor is not None:
            self._initialize_mask(sample_tensor)

    def _initialize_mask(self, sample_tensor):
        self.input_length = np.product(sample_tensor.shape)
        self.length = int(np.ceil(self.percent_included * self.input_length))

        if self.percent_included == 1:
            self.mask = np.index_exp[:]
        elif self.percent_included == 0:
            raise NotImplemented('Mask size of 0 has been disabled because the previous implementation was not ' +
                                 'compatible with individual_grads=True on the forward call.')
        else:
            self.mask = np.zeros(self.input_length, dtype=bool)

            self.mask[:self.length] = True
            np.random.shuffle(self.mask)

            self.mask = from_numpy(self.mask).to(t_bool).cuda()

    def __len__(self):
        if self.length is None:
            raise MaskError('This mask has not yet been fully initialized. Please call _initialize_mask or ' +
                            '__call__ with a sample vector to calculate length.')
        return self.length

    def apply_mask(self, input_: Tensor, individual_grads: bool, n_opacus_samples: int = None):
        if individual_grads:
            # if n_opacus_samples is not None:
            #     to_process = cat([flatten(el).unsqueeze(0) for el in input_[:n_opacus_samples]])
            # else:
            to_process = cat([flatten(el).unsqueeze(0) for el in input_])
            if len(to_process[0]) != self.input_length:
                raise MaskError('Provided input to mask does not have the same flattened per-batch length.')

            if self.percent_included == 1:
                return to_process
            else:
                return to_process[:, self.mask]  # masked_select(to_process, self.mask)
        else:
            to_process = flatten(input_)
            if len(to_process) != self.input_length:
                raise MaskError('Provided input to mask does not have the same flattened length.')
            return to_process[self.mask]

    def __call__(self, input_: Tensor, grad_instead=False, individual_grads=False, n_opacus_samples: int = None):
        if grad_instead:
            if individual_grads:
                if not hasattr(input_, 'grad_sample'):
                    raise MaskError('Provided tensor does not have attribute grad_sample. Please enable opacus.')
                else:
                    to_process = input_.grad_sample  # noqa
            else:
                to_process = input_.grad
            if to_process is None:
                raise MaskError('Gradient is empty and therefore cannot be masked.')
        else:
            if individual_grads:
                raise MaskError('individual grad is only allowed if grad_instead is also true.')
            to_process = input_

        # this allows the mask to be populated at time of first execution
        if self.mask is None:
            if individual_grads:
                raise MaskError('Please initialize mask using an averaged batch gradient.')
            self._initialize_mask(input_)

        return self.apply_mask(to_process, individual_grads, n_opacus_samples)


# ______________________________________________________________________________________________________________________
class MemoryUnitError(Exception):
    """An exception used by the TensorMemoryUnit class in Gradient_Masking_and_Memory.py."""


# ______________________________________________________________________________________________________________________
class TensorMemoryUnit:
    def __init__(self, cuda_just_in_time=False):
        self.n_vectors = None
        self.n_stored_in_sequence = 0
        self.memory_vectors = None
        self.memory_vector_order = None
        self.cuda_just_in_time = cuda_just_in_time

        self.return_mask = None
        self.dict_mode = False
        self.stored_dict = None

    def reserve_memory(self, n_vectors, length, in_cuda=True):
        if self.memory_vectors:
            raise MemoryUnitError('Memory has already been reserved.')

        self.n_vectors = n_vectors
        self.n_stored_in_sequence = 0
        self.memory_vectors = zeros((length, n_vectors))

        if in_cuda and not self.cuda_just_in_time:
            self.memory_vectors = self.memory_vectors.cuda()
        elif in_cuda and self.cuda_just_in_time:
            warn('cuda just in time is enabled, ignoring input in_cuda=True.')

        self.memory_vector_order = np.zeros(n_vectors) + np.NaN

    def store_dict(self, input_: dict, return_mask):
        self.clear()

        self.dict_mode = True
        self.return_mask = return_mask
        self.stored_dict = input_

    def get_stored_dict(self):
        if self.stored_dict is None:
            raise MemoryUnitError('No weights and biases dictionary has been stored.')
        return self.stored_dict

    def store_tensor(self, input_: Tensor):
        if self.dict_mode:
            raise MemoryUnitError('This memory unit is in dictionary mode and cannot store tensors.')

        if self.memory_vectors is None:
            raise MemoryUnitError('Please call reserve_memory before trying to store any tensors.')

        if len(input_.shape) != 1:
            raise MemoryUnitError('Only flattened vectors can be stored.')

        if len(input_) != self.memory_vectors.shape[0]:
            raise MemoryUnitError('Length of input_ does not match reserved memory specifications.')

        current_index = self.n_stored_in_sequence % self.n_vectors
        self.memory_vector_order += 1
        self.memory_vector_order[current_index] = 0
        self.n_stored_in_sequence += 1

        self.memory_vectors[:, current_index] = input_

    def __getitem__(self, key) -> Tensor:
        """Allows the vector memory object to be indexed in one dimension to access an arbitrary selection of
        recently stored memory vectors. Does not allow each individual vector to be sliced."""
        if self.dict_mode:
            if key != 0:
                raise MemoryUnitError('This memory unit is in dict mode and can only recall 1 vector.')
            return self.return_mask.apply_flat_mask(weight=self.stored_dict['weights'], bias=self.stored_dict['biases'])
        else:
            if self.memory_vectors is None:
                raise MemoryUnitError('Please call initialize_memory_vectors before trying to access them.')

            # this gets correct order of the stored vectors, with the 0th entry being the most recently obtained.
            to_get = np.argsort(self.memory_vector_order)[key].astype(int)

            # this checks to see if any of the requested vectors are nan
            if np.sum(np.isnan(self.memory_vector_order[to_get])) > 0:
                raise MemoryUnitError('Some memory vectors were requested that have not been stored yet.')

            if self.cuda_just_in_time and not self.memory_vectors.is_cuda:
                return self.memory_vectors[:, to_get].cuda()
            else:
                return self.memory_vectors[:, to_get]

    def fetch_last_n_memory_vectors(self, n_memory_vectors: int = None):
        """n_vectors = None will return all that have been stored so far."""
        if self.dict_mode and n_memory_vectors > 1:
            raise MemoryUnitError('This memory unit is in dict mode and can only return one vector.')

        if n_memory_vectors is None:
            n_memory_vectors = min(self.n_vectors, self.n_stored_in_sequence)

        return self.memory_vectors[:n_memory_vectors]

    def clear(self):
        """clears memory vectors"""
        self.n_vectors = None
        self.n_stored_in_sequence = 0
        self.memory_vectors = None
        self.memory_vector_order = None

        self.dict_mode = False
        self.return_mask = None
        self.stored_dict = None


# ______________________________________________________________________________________________________________________
class MaskAndMemoryNetworkError(Exception):
    """An exception type used exclusively in the GradientMaskAndMemoryEnabledNetwork class."""


# ______________________________________________________________________________________________________________________
class GradientMaskAndMemoryEnabledNetwork(SubstitutionEnabledNetwork):
    """
    This class is intended to be used as a parent for any network.

    It enables:
        1. the creation and storage of semi-random masks over the learnable parameters weight and bias.
        2. fetching the masked weight or weight.grad for cheaper calculation of weight or grad based metrics
        3. storing a small number of recent weights or gradients for metrics that require samples taken at multiple
            points in weight space.
    """

    def __init__(self, memory_enabled: bool = False):
        """
        Very little is done here
        """
        SubstitutionEnabledNetwork.__init__(self)
        self.masks = []

        self.memory_enabled = memory_enabled
        self.memory_units = dict()
        self.memory_mode = None

        self.opacus_wrapped = None

    def enable_opacus(self):
        """
        Enable individual gradient tracking with opacus.

        This assumes that every included layer type has a valid compute_grad_sample function that is registered
        with opacus. See the full list here: https://github.com/pytorch/opacus/tree/main/opacus/grad_sample

        Hooks are added at initialization of the GradSampleModule. Gradient samples are accessed from a tensor
        that has been associated with the network using Tensor.grad_sample.

        The following function calls will be passed to the GradSampleModuleWrapper
         * add_hooks
         * remove_hooks
         * disable_hooks
         * enable_hooks
         * zero_grad (overwrites default behavior to erase grad_sample for all parameters in addition to grad)
        """

        if self.opacus_wrapped is not None:
            warn('Opacus has already been enabled for this model.')

        # stick the wrapper in a dictionary so that it doesn't cause an infinite loop if .cuda is called on model
        self.opacus_wrapped = {'opacus': GradSampleModule(self)}

    # def cuda(self: T, device: Optional[Union[int, device]] = None) -> T:
    #     """
    #     Overwrites base cuda so that it ignores self.opacus_wrapped
    #     """
    #     return self._apply(lambda t: t.cuda(device))

    def add_hooks(self, loss_reduction: str = "mean", batch_first: bool = True) -> None:
        if self.opacus_wrapped:
            self.opacus_wrapped['opacus'].add_hooks(loss_reduction=loss_reduction, batch_first=batch_first)

    def remove_hooks(self) -> None:
        if self.opacus_wrapped:
            self.opacus_wrapped['opacus'].remove_hooks()

    def disable_hooks(self) -> None:
        if self.opacus_wrapped:
            self.opacus_wrapped['opacus'].disable_hooks()

    def enable_hooks(self) -> None:
        if self.opacus_wrapped:
            self.opacus_wrapped['opacus'].enable_hooks()

    def zero_grad(self, set_to_none: bool = False):
        if not self.opacus_wrapped:
            super().zero_grad(set_to_none=set_to_none)
        else:
            self.opacus_wrapped['opacus'].zero_grad(set_to_none=set_to_none)

    def initialize_mask(self, percent_included: float, name: str,
                        parameter_types_to_include: tuple[str] = ('weight', 'bias'),
                        requires_grad_only: bool = False, layer_types_to_exclude: tuple = tuple()):
        """Create a ParametersMask with the given specifications, save it in self.masks, and return it."""
        if name in [mask.name for mask in self.masks]:
            raise MaskAndMemoryNetworkError(f'The specified mask name {name} has already been used.')

        mask = ParametersMask(percent_included=percent_included, model=self, name=name,
                              parameter_types_to_include=parameter_types_to_include,
                              requires_grad_only=requires_grad_only, layer_types_to_exclude=layer_types_to_exclude)
        mask.initialize_submasks()

        self.masks += [mask]
        if self.memory_enabled:
            self.memory_units[name] = TensorMemoryUnit()

        return mask

    def get_masks(self):
        return self.masks

    def get_masked_weights(self) -> (str, Tensor):
        """Iteratively returns all masked weights."""
        for mask in self.masks:
            yield mask.name, mask()

    def get_masked_grads(self, full_mask_in_dict: bool = False):
        """Iteratively returns all masked grads."""
        for mask in self.masks:
            if full_mask_in_dict and mask.percent_included == 1:
                unmasked_weights_and_bias_grads = dict()
                unmasked_weights_and_bias_grads['weights'] = {module_name: clone(weight.grad)
                                                              for module_name, weight in self.weights.items()}
                unmasked_weights_and_bias_grads['biases'] = {module_name: clone(bias.grad)
                                                             for module_name, bias in self.biases.items()
                                                             if bias is not None}
                yield mask, unmasked_weights_and_bias_grads
            else:
                yield mask.name, mask(grad_instead=True)

    def enable_memory_mode(self) -> None:
        if self.memory_enabled:
            return

        self.memory_units = dict()
        for mask in self.masks:
            self.memory_units[mask.name] = TensorMemoryUnit()
        self.memory_enabled = True

    def disable_memory_mode(self):
        self.memory_enabled = False
        self.memory_mode = None
        self.clear_reserved_memory()
        self.memory_units = dict()

    def reset_memory_units(self, n_vectors_per_mask: int, in_cuda=True):
        self.disable_memory_mode()
        self.enable_memory_mode()
        self.reserve_vector_memory(n_vectors_per_mask, in_cuda=in_cuda)

    def get_memory_units(self):
        return self.memory_units

    def reserve_vector_memory(self, n_vectors_per_mask: int, in_cuda=True):
        if len(self.masks) != len(self.memory_units.keys()):
            raise MaskAndMemoryNetworkError('Length mismatch between stored masks and vector memory units')

        for i, memory_unit in enumerate(self.memory_units.values()):
            memory_unit.reserve_memory(n_vectors_per_mask, len(self.masks[i]), in_cuda=in_cuda)

    def clear_reserved_memory(self):
        self.memory_mode = None
        for memory_unit in self.memory_units.keys():
            self.memory_units[memory_unit].clear()

    def store_masked_weights(self):
        """Stores the current weight for every mask in self.masks."""
        if len(self.masks) == 0:
            raise MaskAndMemoryNetworkError('No masks or memory units have been initialized.')

        if self.memory_mode is None:
            self.memory_mode = 'weight'
        elif self.memory_mode != 'weight':
            raise MaskAndMemoryNetworkError('Memory units currently have gradients stored. ' +
                                            'Please clear them before storing weights.')

        for mask_name, weight in self.get_masked_weights():
            self.memory_units[mask_name].store_tensor(weight)

    def store_masked_grads(self, mask_names: list[str] = None, dict_if_full_mask: bool = False):
        """Stores the current gradient for every mask in self.masks."""
        if len(self.masks) == 0:
            raise MaskAndMemoryNetworkError('No masks or memory units have been initialized.')

        if self.memory_mode is None:
            self.memory_mode = 'grad'
        elif self.memory_mode != 'grad':
            raise MaskAndMemoryNetworkError('Memory units currently have weights stored. ' +
                                            'Please clear them before storing weights.')

        for mask_name, grad in self.get_masked_grads(full_mask_in_dict=dict_if_full_mask):
            if type(grad) == dict:
                # in this case mask_name will actually be the mask itself
                self.memory_units[mask_name.name].store_dict(grad, return_mask=mask_name)
            elif mask_names is None or mask_name in mask_names:
                self.memory_units[mask_name].store_tensor(grad)

    def fetch_stored_weights(self, n_memory_vectors: int = None):
        """Iteratively returns all memory weights by mask."""
        if not self.memory_enabled:
            raise MaskAndMemoryNetworkError("memory functionality is not enabled")
        if self.memory_mode != 'weight':
            raise MaskAndMemoryNetworkError('Memory mode is set to grad, and no weights have been stored.')

        for memory_unit in self.memory_units.values():
            yield memory_unit.fetch_last_n_memory_vectors(self, n_memory_vectors)

    def fetch_stored_grads(self, n_memory_vectors: int = None):
        """Iteratively returns all memory grads by mask."""
        if not self.memory_enabled:
            raise MaskAndMemoryNetworkError("memory functionality is not enabled")
        if self.memory_mode != 'grad':
            raise MaskAndMemoryNetworkError('Memory mode is set to weight, and no weights have been stored.')

        for memory_unit in self.memory_units.values():
            yield memory_unit.fetch_last_n_memory_vectors(self, n_memory_vectors)

    def paired_mask_and_memory_units(self) -> (ParametersMask, TensorMemoryUnit):
        if not self.memory_enabled or len(self.masks) != len(self.memory_units.keys()):
            raise MaskAndMemoryNetworkError('No memory units created or mismatch between saved masks and memory units.')

        for mask in self.masks:
            yield mask, self.memory_units[mask.name]
