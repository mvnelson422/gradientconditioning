from __future__ import annotations

from torch import std, normal, flatten, zeros, allclose, empty_like
from torch import abs as t_abs
from torch import max as t_max
from torch.linalg import norm
import numpy as np
from .timers import TimerManager, TimerRunning


# ______________________________________________________________________________________________________________________
def realize_grads(opacus_samples=False, **parameters):
    """
    The entire purpose of this function is to call .grad pointlessly on the provided parameters to prevent
    the timing getting thrown off wildly for the first mask that has to reference them. It seems these are
    constructed at time of first reference. We do this by calling the cheapest possible function on them
    that requires values to actually be computed, max, and then using it in a way that forces the expression
    to evaluate. If not all grads or weights are actually going to be used then this can add significant overhead.
    """
    # return
    for module_name, param in parameters.items():
        if opacus_samples and hasattr(param, 'grad_samples'):
            if param.grad_samples.max() < 0:
                pass
        elif hasattr(param, 'grad') and not opacus_samples:
            if param.grad.max() < 0:
                pass
        elif param is not None:
            # if the parameter doesn't have grad or grad_samples as an attribute (whichever one is specified)
            # assume that it is the grad or grad_sample
            if param.max() < 0:
                pass


# ______________________________________________________________________________________________________________________
class Itemizer(object):
    """
    Takes a function that operates on torch variables and outputs a scalar Tensor and forces it
    to return a detached non-tensor scalar on the cpu.

    Also casts the result from each call as np.float64 to avoid division by zero errors
    (norms and inner products are used to normalize a lot)
    """
    def __init__(self, function):
        self.function = function

    def __call__(self, *args, **kwargs):
        return np.float64(self.function(*args, **kwargs).item())


# ______________________________________________________________________________________________________________________
def all_grad_metrics(model, loss_f, batch_x, batch_y, train_loader,
                     inner_product_w, norm_x, norm_w,
                     rank_normalizing_constants: dict, n_samples_norm_constant: int,
                     step_scale_factor_x: float, step_scale_factor_w: float,
                     n_pairs_per_sample_confusion_x: int,
                     n_batches_per_sample_diversity_x: int,
                     n_samples_weight_shift_direction: int = None,
                     n_samples_input_shift_direction: int = None,
                     n_samples_input_shift_average: int = None,
                     conditioning_x: bool = True, input_grad_rank_x: bool = True,
                     weight_grad_rank_x: bool = False, coherence_x: bool = True,
                     confusion_x_samples: int = 0, diversity_x_samples: int = 0,
                     conditioning_w: bool = True, confusion_w: bool = True,
                     diversity_w: bool = True, coherence_w: bool = True,
                     targeted_sampling_w=True, targeted_sampling_x=True,
                     debug=False, shift_direction_test: bool = False,
                     batch_grad_targeted_w: bool = False, device='cuda:0'):
    """
    On input the model must have individual gradients and opacus individual gradient samples populated. This can be
    accomplished by calling model.add_hooks (or model.enable_hooks if hooks have already been added) and executing a
    backwards pass.

    This function requires that current gradients are stored for the weights AND for the input batch_x. Furthermore,
    the opacus created attribute grad_sample must be populated (these are the un-averaged weight grads).

    This function modifies the model state in two ways:
        1. It clears all gradients
        2. It disables opacus individual gradient hooks
        3. It clears all memory units
    Neither of these modifications are technically necessary, but they significantly improve efficiency.

    inputs:
        model: the model
        loss_f: the loss function
        batch_x and batch_y: the batch values and batch labels that the currently stored gradient was calculated with
        step_scale_factor_x: the step size for calculating shifted input grad metrics
        step_scale_factor_w: the step size for calculating shifted weight grad metrics
        inner_product_w: the inner product over the weight space
        norm_x: the norm over the input space
        norm_w: the norm over the weight space
        n_pairs_per_sample_confusion_x: the number of pairs of batches per sample when calculating confusion_x
        n_batches_per_sample_diversity_x: the number of batches per sample when calculating diversity_x
        rank_normalizing_constants: a dictionary in which calculated normalizing constants for the Relative Effective
            Rank metric can be stored and referenced for efficiency
        n_samples_weight_shift_direction: the number of samples to subsample from a batch for the weight shift samples
        n_samples_input_shift_direction: the number of samples to subsample from a batch for the input shift samples
        n_samples_norm_constant: the number of samples to be averaged when calculating the Relative Effective Rank
            normalizing constants
        confusion_x_samples: the number of samples to calculate for confusion_x
        diversity_x_samples: the number of samples to calculate for diversity_x
        device: defaults to 'cuda:0'. If set to None will disable all .item() and .cpu() calls
        all other variables: boolean switches that determine whether that metric will be calculated or not
    """
    n_individual_grads_to_store = max(n_samples_weight_shift_direction, n_samples_input_shift_average)

    opacus_storage_required = (conditioning_x or ((not batch_grad_targeted_w and targeted_sampling_w)
                               and (conditioning_w or confusion_w or diversity_w or coherence_w)))
    requires_opacus = (opacus_storage_required or conditioning_x or weight_grad_rank_x or coherence_x)
    requires_grad_on_input = (input_grad_rank_x or targeted_sampling_x)

    if device is not None:
        norm_x = Itemizer(norm_x)
        norm_w = Itemizer(norm_w)
        inner_product_w = Itemizer(inner_product_w)

    if not (conditioning_x or input_grad_rank_x or weight_grad_rank_x or coherence_x
            or conditioning_w or confusion_w or diversity_w or coherence_w or
            confusion_x_samples > 0 or diversity_x_samples > 0):
        raise ValueError('At least one metric must be enabled.')

    b = batch_x.shape[0]
    if (n_samples_weight_shift_direction > b
            or n_samples_input_shift_direction > b
            or n_samples_input_shift_average > b):
        raise ValueError('Using n_samples_weight_shift_direction variables higher than the batch size is not ' +
                         'currently supported.')

    if requires_opacus and (not model.opacus_wrapped or not model.opacus_wrapped['opacus'].hooks_enabled):
        raise ValueError('The provided model has not had individual gradients stored for the last batch. ' +
                         'Please use model.enable_opacus(), make sure hooks are enabled, and then call ' +
                         'loss.backward prior to calling this function.')

    if requires_grad_on_input and not (hasattr(batch_x, 'grad') and batch_x.grad is not None):
        raise ValueError('If input_grad_rank_x or conditioning_x are true then batch_x must have a non-None ' +
                         'grad attribute.')

    # check to make sure that all masks have unique names we can index with
    mask_names = []
    for mask in model.get_masks():
        if mask in mask_names:
            raise ValueError('At least two masks stored by the provided model have the same name.')
        mask_names += [mask.name]
    del mask_names

    timers = TimerManager()

    # calculate metrics that don't require a projected forward call-----------------------------------------------------
    x_rank_hw, x_coherence, x_grad_rank = None, None, None

    if weight_grad_rank_x:
        x_rank_hw = dict()

    if coherence_x:
        x_coherence = dict()

    if weight_grad_rank_x or coherence_x:
        timers.add_timers(['weight_grad_rank_x', 'coherence_x'])

        # this call should prevent the first mask called from having a way longer execution time than the others
        with TimerRunning(timers, ['weight_grad_rank_x', 'coherence_x'], None):
            realize_grads(**model.weights)
            realize_grads(**model.biases)
            if requires_opacus:
                realize_grads(**model.weights, opacus_samples=True)
                realize_grads(**model.biases, opacus_samples=True)

        for k, mask in enumerate(model.get_masks()):
            with TimerRunning(timers, ['weight_grad_rank_x', 'coherence_x'], mask.name):
                masked_grad = mask(grad_instead=True)
                grad_indv_sample = mask(grad_instead=True, individual_grads=True)
                shape = np.shape(grad_indv_sample)

                if debug:
                    if not allclose(masked_grad, grad_indv_sample.mean(dim=0),
                                    equal_nan=True, atol=5*1e-5, rtol=1e-4):

                        for module_name, module in model.named_modules():
                            if module_name in mask.sub_masks.keys():
                                for param in mask.sub_masks[module_name].keys():
                                    unmasked_grad = module.__getattr__(param).grad
                                    unmasked_indv_grad = module.__getattr__(param).grad_sample

                                    masked_grad = mask.sub_masks[module_name][param](module.__getattr__(param),
                                                                                     grad_instead=True,
                                                                                     individual_grads=False)
                                    masked_indv_grad = mask.sub_masks[module_name][param](module.__getattr__(param),
                                                                                          grad_instead=True,
                                                                                          individual_grads=True)

                                    if not allclose(unmasked_grad, unmasked_indv_grad.mean(dim=0)):
                                        print(f"Opacus grad_samples for {module_name}.{param} do not average " +
                                              "correctly. Max error: " +
                                              f"{t_max(t_abs(unmasked_grad - unmasked_indv_grad.mean(dim=0)))}")
                                    elif not allclose(masked_grad, masked_indv_grad.mean(dim=0)):
                                        print(f"Masked opacus grad_samples for {module_name}.{param} do not average " +
                                              f"correctly, but the unmasked samples do. " +
                                              f"Max error: {t_max(t_abs(masked_grad - masked_indv_grad.mean(dim=0)))}")
                        raise ValueError("Opacus grad samples don't average accurately to the batch gradient for " +
                                         f'mask {mask.name}. See printed per-layer statements. Ensure opacus hooks ' +
                                         'are actually disabled or call model.zero_grad() after every ' +
                                         'optimizer.step() call to ensure opacus grads do not build up.')

            if weight_grad_rank_x:
                with TimerRunning(timers, ['weight_grad_rank_x'], mask.name):
                    if shape not in rank_normalizing_constants:
                        x_rank_hw[mask.name], rank_normalizing_constants[shape] = \
                            relative_effective_rank(grad_indv_sample, n_samples=n_samples_norm_constant)
                    else:
                        x_rank_hw[mask.name] = \
                            relative_effective_rank(grad_indv_sample,
                                                    normalizing_constant=rank_normalizing_constants[shape],
                                                    device=device)

            if coherence_x:
                with TimerRunning(timers, ['coherence_x'], mask.name):
                    x_coherence[mask.name] = coherence_over_x(grad_indv_sample, masked_grad,
                                                              inner_product_w, norm_w)
        timers.close_timers(['weight_grad_rank_x', 'coherence_x'])

    if input_grad_rank_x:
        timers.add_timers(['input_grad_rank_x'])
        shape = np.shape(batch_x)
        with TimerRunning(timers, ['input_grad_rank_x'], None):
            if shape not in rank_normalizing_constants:
                x_grad_rank, rank_normalizing_constants[shape] = \
                    relative_effective_rank(flatten(batch_x.grad, start_dim=1), normalizing_constant=None,
                                            n_samples=n_samples_norm_constant)
            else:
                x_grad_rank = relative_effective_rank(flatten(batch_x.grad, start_dim=1),
                                                      normalizing_constant=rank_normalizing_constants[shape])
        timers.close_timers(['input_grad_rank_x'])

    # store masked gradients here and clear unmasked grads in preparation for shifted forward call metrics--------------
    x_conditioning, w_conditioning, w_confusion, w_diversity, w_coherence = None, None, None, None, None
    if conditioning_x or conditioning_w or confusion_w or diversity_w or coherence_w:
        timers.add_timers(['conditioning_x', 'conditioning_w', 'confusion_w', 'diversity_w', 'coherence_w'])

        model.reset_memory_units(1, in_cuda=True)
        for mask in model.get_masks():
            with TimerRunning(timers, ['conditioning_x', 'conditioning_w',
                                       'confusion_w', 'diversity_w', 'coherence_w'], mask.name):
                if batch_grad_targeted_w or shift_direction_test:
                    model.store_masked_grads([mask.name], dict_if_full_mask=True)
                else:
                    model.store_masked_grads([mask.name])

        # calculate norms
        masked_grad_norms = dict()
        masked_param_norms = dict()
        for PM, TMU in model.paired_mask_and_memory_units():
            with TimerRunning(timers, ['conditioning_x', 'conditioning_w', 'confusion_w',
                                       'diversity_w', 'coherence_w'], PM.name):
                masked_grad_norms[PM.name] = norm_w(TMU[0])  # index the last stored grad
                masked_param_norms[PM.name] = norm_w(PM())  # call mask to get masked weights

        # store individual unmasked gradient estimates so that they will persist over optimizer.zero_grad
        # for module_name, weight in model.weights.items():
        #     print(module_name, 'has grad:', hasattr(weight, 'grad'), 'grad is None:', weight.grad is None,
        #           'has grad samples:', hasattr(weight, 'grad_sample'), 'grad_sample is none:',
        #           weight.grad_sample is None)

        with TimerRunning(timers, ['conditioning_x', 'conditioning_w', 'confusion_w',
                                   'diversity_w', 'coherence_w'], None):
            if opacus_storage_required:
                individual_grads = {module_name: weight.grad_sample[:n_individual_grads_to_store]
                                    for module_name, weight in model.weights.items()}
                individual_bias_grads = {module_name: bias.grad_sample[:n_individual_grads_to_store]
                                         for module_name, bias in model.biases.items() if bias is not None}
            else:
                individual_grads = None
                individual_bias_grads = None

        # this is called in each of the following functions (at the beginning and end), but we call it here just in case
        # none of those are called
        model.zero_grad()

        # calculate metrics that require a shifted forward call in x----------------------------------------------------
        x_conditioning = None
        if conditioning_x:
            if shift_direction_test:
                x_conditioning = []
                # targeted
                x_conditioning += [shifted_x_metrics(individual_grads, individual_bias_grads, batch_x,
                                                     batch_y, step_scale_factor_x, loss_f, norm_x, norm_w,
                                                     model, n_samples_input_shift_direction,
                                                     n_samples_input_shift_average, timers,
                                                     targeted_sampling=True, shift_convergence_test=True)]
                # half interpolation targeted
                x_conditioning += [shifted_x_metrics(individual_grads, individual_bias_grads, batch_x,
                                                     batch_y, step_scale_factor_x, loss_f, norm_x, norm_w,
                                                     model, n_samples_input_shift_direction,
                                                     n_samples_input_shift_average, timers,
                                                     targeted_sampling=False, shift_convergence_test=True,
                                                     half_interpolation=True)]
                # full interpolation targeted
                x_conditioning += [shifted_x_metrics(individual_grads, individual_bias_grads, batch_x,
                                                     batch_y, step_scale_factor_x, loss_f, norm_x, norm_w,
                                                     model, n_samples_input_shift_direction,
                                                     n_samples_input_shift_average, timers,
                                                     targeted_sampling=False, shift_convergence_test=True,
                                                     full_interpolation=True)]

                # un-targeted
                # x_conditioning += [np.zeros_like(x_conditioning[0])]
                x_conditioning += [shifted_x_metrics(individual_grads, individual_bias_grads, batch_x,
                                                     batch_y, step_scale_factor_x, loss_f, norm_x, norm_w,
                                                     model, n_samples_input_shift_direction,
                                                     n_samples_input_shift_average, timers,
                                                     targeted_sampling=False, shift_convergence_test=True)]
            else:
                x_conditioning = shifted_x_metrics(individual_grads, individual_bias_grads, batch_x, batch_y,
                                                   step_scale_factor_x, loss_f, norm_x, norm_w, model,
                                                   n_samples_input_shift_direction, n_samples_input_shift_average,
                                                   timers, targeted_sampling=targeted_sampling_x)
        timers.close_timers(['conditioning_x'])

        # these are disabled here for efficiency and because the shifted forward call breaks when they are enabled
        # TODO: find out why disable_hooks doesn't fully prevent opacus state change on forward and backward calls
        model.disable_hooks()

        # calculate metrics that require a shifted forward call in w----------------------------------------------------
        if conditioning_w or confusion_w or diversity_w or coherence_w:
            if shift_direction_test:
                w_conditioning = [None, None, None]
                w_confusion = [None, None, None]
                w_diversity = [None, None, None]
                w_coherence = [None, None, None]
                # targeted
                w_conditioning[0], w_confusion[0], \
                    w_diversity[0], w_coherence[0] = shifted_w_metrics(individual_grads, individual_bias_grads,
                                                                       masked_grad_norms, masked_param_norms,
                                                                       batch_x, batch_y, step_scale_factor_w, model,
                                                                       loss_f, inner_product_w, norm_w,
                                                                       n_samples_weight_shift_direction,
                                                                       conditioning_w, confusion_w,
                                                                       diversity_w, coherence_w,
                                                                       timers, True, False, device, False)
                # batch-grad targeted
                w_conditioning[1], w_confusion[1], \
                    w_diversity[1], w_coherence[1] = shifted_w_metrics(individual_grads, individual_bias_grads,
                                                                       masked_grad_norms, masked_param_norms,
                                                                       batch_x, batch_y, step_scale_factor_w, model,
                                                                       loss_f, inner_product_w, norm_w,
                                                                       2,
                                                                       conditioning_w, confusion_w,
                                                                       diversity_w, coherence_w,
                                                                       timers, False, False, device, True)
                # un-targeted
                w_conditioning[2], w_confusion[2], \
                    w_diversity[2], w_coherence[2] = shifted_w_metrics(individual_grads, individual_bias_grads,
                                                                       masked_grad_norms, masked_param_norms,
                                                                       batch_x, batch_y, step_scale_factor_w, model,
                                                                       loss_f, inner_product_w, norm_w,
                                                                       n_samples_weight_shift_direction,
                                                                       conditioning_w, confusion_w,
                                                                       diversity_w, coherence_w,
                                                                       timers, False, False, device, False)

            else:
                w_conditioning, w_confusion, \
                    w_diversity, w_coherence = shifted_w_metrics(individual_grads, individual_bias_grads,
                                                                 masked_grad_norms, masked_param_norms,
                                                                 batch_x, batch_y, step_scale_factor_w, model,
                                                                 loss_f, inner_product_w, norm_w,
                                                                 n_samples_weight_shift_direction,
                                                                 conditioning_w, confusion_w, diversity_w, coherence_w,
                                                                 timers, targeted_sampling_w, True, device,
                                                                 batch_grad_targeted_w)
        timers.close_timers(['conditioning_w', 'confusion_w', 'diversity_w', 'coherence_w'])

    # calculate metrics that require multiple batches-------------------------------------------------------------------
    # these are by far the most expensive to calculate
    x_confusion, x_diversity = None, None
    if confusion_x_samples > 0 or diversity_x_samples > 0:
        timers.add_timers(['confusion_x', 'diversity_x'])
        x_confusion, x_diversity = multiple_batch_metrics(model, train_loader, loss_f,
                                                          inner_product_w, norm_w,
                                                          n_pairs_per_sample_confusion_x,
                                                          n_batches_per_sample_diversity_x,
                                                          n_samples_confusion=confusion_x_samples,
                                                          n_samples_diversity=diversity_x_samples,
                                                          timers=timers)
        timers.close_timers(['confusion_x', 'diversity_x'])
    else:
        model.clear_reserved_memory()

    ret_dict = {'conditioning_x': x_conditioning, 'input_grad_rank_x': x_grad_rank,
                'weight_grad_rank_x': x_rank_hw, 'coherence_x': x_coherence,
                'confusion_x': x_confusion, 'diversity_x': x_diversity,
                'conditioning_w': w_conditioning, 'confusion_w': w_confusion,
                'diversity_w': w_diversity, 'coherence_w': w_coherence}
    ret_dict = {k: v for k, v in ret_dict.items() if v is not None}

    times = timers.get_elapsed()
    to_return_times = {k: times[k] for k in ret_dict}

    return ret_dict, to_return_times


# ______________________________________________________________________________________________________________________
def shifted_x_metrics(individual_grads, individual_bias_grads, batch_x, batch_y,
                      shift_step_size: float, loss_f, norm_x, norm_w, model,
                      n_shift_directions: int = None, n_shifts_samples_average: int = None,
                      timers: TimerManager = None, targeted_sampling=True,
                      shift_convergence_test: bool = False,
                      half_interpolation: bool = False, full_interpolation: bool = False):
    """
    Calculate metrics that require shifting the input prior to forward call. These require that batch_x has a gradient
    stored.
    """
    b = batch_x.shape[0]
    model.zero_grad()

    if targeted_sampling and (not hasattr(batch_x, 'grad') or batch_x.grad is None):
        raise ValueError('Provided batch input does not have a stored gradient.')

    if n_shift_directions is None:
        n_shift_directions = b
    elif n_shift_directions > b:
        raise ValueError('n_shift_directions cannot be larger than the batch size.')

    if n_shifts_samples_average is None:
        n_shifts_samples_average = b
    elif n_shifts_samples_average > b:
        raise ValueError('n_shifts_samples_average cannot be larger than the batch size.')

    condition_x = dict()
    for mask in model.get_masks():
        if shift_convergence_test:
            condition_x[mask.name] = np.zeros((n_shift_directions, b))
        else:
            condition_x[mask.name] = np.zeros(b)

    for k in range(n_shift_directions):
        with TimerRunning(timers, ['conditioning_x'], None):
            # shifting in the direction that maximizes change in loss is ideal for avoiding degenerate directions
            # if the partials with respect to the individual inputs are not provided then we can attempt to avoid
            # degeneracy by shifting in the direction of other samples.
            if half_interpolation:
                raise ValueError('This sampling method is not to be used in final tests.')
                delta = batch_x[k-1] * np.random.choice([-1, 1])
            elif full_interpolation:
                raise ValueError('This sampling method is not to be used in final tests.')
                delta = (batch_x[k-1] - batch_x[k]) * np.random.choice([-1, 1])
            elif not targeted_sampling:
                raise ValueError('This sampling method is not to be used in final tests.')
                delta = empty_like(batch_x[k]).normal_(mean=batch_x[k].mean().item(), std=batch_x[k].std().item())
            else:
                delta = batch_x.grad[k] * np.random.choice([-1, 1])
            # individual_grads_k = {module_name: grads[k] for module_name, grads in individual_grads.items()}
            # individual_bias_grads_k = {module_name: grads[k] for module_name, grads in individual_bias_grads.items()}
            # normalize to shift_step_size
            delta = delta * shift_step_size / norm_x(delta)

            norm_x_ = norm_x(batch_x[k])
            norm_delta = norm_x(delta)

            shifted_pred, shifted_params = model.shifted_forward(batch_x, input_delta=delta)
            loss_train = loss_f(shifted_pred, batch_y)
            loss_train.backward()

            # this call should prevent the first mask called from having a way longer execution time than the others
            realize_grads(**individual_grads, opacus_samples=True)
            realize_grads(**individual_bias_grads, opacus_samples=True)

        for mask, TMU in model.paired_mask_and_memory_units():
            with TimerRunning(timers, ['conditioning_x'], mask.name):
                unshifted_masked_grads = mask.apply_flat_mask(weight=individual_grads, bias=individual_bias_grads,
                                                              individual_grads=True)
                shifted_masked_grads = mask(grad_instead=True, individual_grads=True)

                for j in range(n_shifts_samples_average):
                    if shift_convergence_test:
                        condition_x[mask.name][k, j] = (norm_w(shifted_masked_grads[j] - unshifted_masked_grads[j] *
                                                        norm_x_) / norm_w(unshifted_masked_grads[j]) * norm_delta)
                    else:
                        condition_x[mask.name][j] = max(condition_x[mask.name][j],
                                                        norm_w(shifted_masked_grads[j] - unshifted_masked_grads[j] *
                                                        norm_x_) / norm_w(unshifted_masked_grads[j]) * norm_delta)

        model.zero_grad()

    if not shift_convergence_test:
        for mask in model.get_masks():
            with TimerRunning(timers, ['conditioning_x'], mask.name):
                condition_x[mask.name] = np.mean(condition_x[mask.name])

    return condition_x


# ______________________________________________________________________________________________________________________
def shifted_w_metrics(individual_grads, individual_bias_grads,
                      masked_grad_norms, masked_param_norms,
                      batch_x, batch_y, step_scale_factor, model,
                      loss_f, inner_product_w, norm_w,
                      n_samples_omega: int = None,
                      conditioning_w=True, confusion_w=True, diversity_w=True, coherence_w=True,
                      timers: TimerManager = None, targeted_sampling=True,
                      max_min_shift_samples: bool = True, device='cuda:0', batch_grad_targeted_w: bool = False):
    """
    Calculate all gradient metrics that require a forward call with a shifted weight using individual_grads
    and individual_bias_grads as the sampled directions to shift over.
    """
    w_conditioning, w_confusion, w_diversity, w_coherence, w_coherence_numerator, w_coherence_denominator = \
        None, None, None, None, None, None
    w_diversity_sum, w_diversity_norm_sum = None, None
    model.zero_grad()

    if batch_grad_targeted_w and n_samples_omega > 2:
        raise ValueError('Only one omega sample can be generated using the batch gradient.')

    if n_samples_omega > len(batch_x):
        raise ValueError('n_samples_omega can not be greater than the batch size.')

    if conditioning_w or confusion_w or diversity_w or coherence_w:
        with TimerRunning(timers, ['conditioning_w', 'confusion_w', 'diversity_w', 'coherence_w'], None):
            # initialize metrics that require a projected forward call
            if conditioning_w:
                w_conditioning = dict()
            if confusion_w:
                w_confusion = dict()
            if diversity_w:
                w_diversity = dict()
                w_diversity_sum = dict()
                w_diversity_norm_sum = dict()
            if coherence_w:
                w_coherence = dict()
                w_coherence_numerator = dict()
                w_coherence_denominator = dict()

            # # initialize sums and calculate norms
            # masked_grad_norms = dict()
            # masked_param_norms = dict()

        if n_samples_omega is None:
            n_samples_omega = np.shape(batch_x)[0]

        for mask in model.get_masks():

            # initialize sums
            if conditioning_w:
                if max_min_shift_samples:
                    w_conditioning[mask.name] = 0
                else:
                    w_conditioning[mask.name] = np.zeros(n_samples_omega)
            if confusion_w:
                if max_min_shift_samples:
                    # this will always be less than 1 and there is a min, to setting this to two should make
                    # it obvious if there is a problem
                    w_confusion[mask.name] = 2
                else:
                    w_confusion[mask.name] = np.zeros(n_samples_omega)
            if coherence_w:
                w_coherence[mask.name] = 0
                w_coherence_numerator[mask.name] = 0
                w_coherence_denominator[mask.name] = np.float64(0)
            if diversity_w:
                w_diversity_norm_sum[mask.name] = 0
                with TimerRunning(timers, ['diversity_w'], mask.name):
                    w_diversity_sum[mask.name] = zeros(len(mask), device=device)

            # # calculate required norms
            # masked_grad_norms[mask.name] = norm(TMU[0])
            # masked_param_norms[mask.name] = norm(mask())

        # calculate metrics that require a projected forward call
        for i in range(n_samples_omega):
            with TimerRunning(timers, ['conditioning_w', 'confusion_w', 'diversity_w', 'coherence_w'], None):

                # calculate the shifted weight and bias deltas
                if batch_grad_targeted_w:
                    if n_samples_omega == 1:
                        random_shift_sign = np.random.choice([-1, 1])
                    elif i == 0:
                        random_shift_sign = 1
                    else:
                        random_shift_sign = -1
                    unmasked_weight_and_bias_grads = None
                    for mask, TMU in model.paired_mask_and_memory_units():
                        if TMU.dict_mode:
                            unmasked_weight_and_bias_grads = TMU.get_stored_dict()
                            break
                    if unmasked_weight_and_bias_grads is None:
                        raise ValueError('No unmasked gradient was stored, so batch_grad_targeted sampling ' +
                                         'can not be used.')
                    weight_delta = {module_name: random_shift_sign * grads
                                    for module_name, grads in unmasked_weight_and_bias_grads['weights'].items()}
                    bias_delta = {module_name: random_shift_sign * bias_grads
                                  for module_name, bias_grads in unmasked_weight_and_bias_grads['biases'].items()}
                elif targeted_sampling:
                    raise ValueError('This sampling method is not to be used in final tests.')
                    random_shift_sign = np.random.choice([-1, 1])
                    weight_delta = {module_name: random_shift_sign * grads[i]
                                    for module_name, grads in individual_grads.items()}
                    bias_delta = {module_name: random_shift_sign * bias_grads[i]
                                  for module_name, bias_grads in individual_bias_grads.items()}
                else:
                    raise ValueError('This sampling method is not to be used in final tests.')
                    weight_delta = {module_name: (empty_like(weight).normal_(mean=weight.mean().item(),
                                                                             std=weight.std().item()))
                                    for module_name, weight in model.weights.items()}
                    bias_delta = {module_name: empty_like(bias).normal_(mean=bias.mean().item(), std=bias.std().item())
                                  for module_name, bias in model.biases.items()}

                # normalize to step scale factor
                for module_name, value in weight_delta.items():
                    weight_delta[module_name] = value * step_scale_factor / norm_w(value)
                for module_name, value in bias_delta.items():
                    bias_delta[module_name] = value * step_scale_factor / norm_w(value)

                # estimate a gradient using the weight shifted slightly in the direction of the current gradient sample
                shifted_pred, shifted_params = model.shifted_forward(batch_x,
                                                                     weight_delta=weight_delta,
                                                                     tracking_on_shifted_weight=True,
                                                                     bias_delta=bias_delta,
                                                                     tracking_on_shifted_bias=True)
                loss_train = loss_f(shifted_pred, batch_y)
                loss_train.backward()

                # this call should prevent the first mask called from having a way longer execution time than the others
                realize_grads(**shifted_params['weight'])
                if shifted_params['bias'] is not None:
                    realize_grads(**shifted_params['bias'])

            # calculate sub-samples of metrics over w using the gradient using this shifted weight sample
            for mask, TMU in model.paired_mask_and_memory_units():
                with TimerRunning(timers, ['conditioning_w', 'confusion_w', 'diversity_w', 'coherence_w'], mask.name):
                    shift_magnitude = norm_w(mask.apply_flat_mask(weight=weight_delta, bias=bias_delta))

                    if shifted_params['bias'] is not None:
                        masked_shifted_grad = mask.apply_flat_mask(weight=shifted_params['weight'],
                                                                   bias=shifted_params['bias'],
                                                                   grad_instead=True)
                    else:
                        masked_shifted_grad = mask.apply_flat_mask(weight=shifted_params['weight'],
                                                                   grad_instead=True)

                    inner_shifted = inner_product_w(masked_shifted_grad, TMU[0])
                    norm_shifted = norm_w(masked_shifted_grad)

                # gradient conditioning with respect to W
                if conditioning_w:
                    with TimerRunning(timers, ['conditioning_w'], mask.name):
                        if max_min_shift_samples:
                            w_conditioning[mask.name] = max(w_conditioning[mask.name],
                                                            (norm_w(masked_shifted_grad - TMU[0])
                                                            * masked_param_norms[mask.name]) /
                                                            (shift_magnitude * masked_grad_norms[mask.name]))
                        else:
                            w_conditioning[mask.name][i] = ((norm_w(masked_shifted_grad - TMU[0])
                                                             * masked_param_norms[mask.name]) /
                                                            (shift_magnitude * masked_grad_norms[mask.name]))

                # gradient confusion
                if confusion_w:
                    with TimerRunning(timers, ['confusion_w'], mask.name):
                        if max_min_shift_samples:
                            w_confusion[mask.name] = min(w_confusion[mask.name],
                                                         inner_shifted /
                                                         (norm_shifted * masked_grad_norms[mask.name]))
                        else:
                            w_confusion[mask.name][i] = inner_shifted / (norm_shifted * masked_grad_norms[mask.name])

                # gradient diversity
                if diversity_w:
                    with TimerRunning(timers, ['diversity_w'], mask.name):
                        w_diversity_sum[mask.name] += masked_shifted_grad
                        w_diversity_norm_sum[mask.name] += (norm_shifted**2)

                # gradient coherence
                if coherence_w:
                    with TimerRunning(timers, ['coherence_w'], mask.name):
                        w_coherence_numerator[mask.name] += inner_shifted / n_samples_omega
                        w_coherence_denominator[mask.name] += (norm_shifted**2) / n_samples_omega
            
            # technically not needed here because all relevant parameters are being overwritten, but best to be safe
            model.zero_grad()

        # normalize where required
        if diversity_w:
            for mask in model.get_masks():
                with TimerRunning(timers, ['diversity_w'], mask.name):
                    w_diversity[mask.name] = norm_w(w_diversity_sum[mask.name])**2 \
                                             / w_diversity_norm_sum[mask.name]

        # if confusion_w:
        #     for mask in model.get_masks():
        #         with TimerRunning(timers, ['confusion_w'], mask.name):
        #             # remove this condition because it's always positive
        #             # w_confusion[mask.name] = - min(0, w_confusion[mask.name])
        #             w_confusion[mask.name] = - w_confusion[mask.name]

        if coherence_w:
            for mask in model.get_masks():
                with TimerRunning(timers, ['coherence_w'], mask.name):
                    w_coherence[mask.name] = w_coherence_numerator[mask.name] \
                                             / w_coherence_denominator[mask.name]

    return w_conditioning, w_confusion, w_diversity, w_coherence


# ______________________________________________________________________________________________________________________
def multiple_batch_metrics(model, data_loader, loss_f, inner_w, norm_w,
                           n_pairs_per_sample_confusion: int, n_batches_per_sample_diversity: int,
                           n_samples_confusion: int = 1, n_samples_diversity: int = 1, timers: TimerManager = None,
                           device='cuda:0') \
        -> (dict[np.ndarray], dict[np.ndarray], dict[str, float]):
    """
    This function modifies the network state by zeroing model gradients and clearing all memory vectors.
    """

    model.zero_grad()
    model.reset_memory_units(1)

    n_iters = max(2*n_pairs_per_sample_confusion*n_samples_confusion,
                  n_batches_per_sample_diversity*n_samples_diversity)

    g_confusion, g_diversity, g_diversity_norm_sum, g_diversity_grad_sum = None, None, None, None
    if n_samples_confusion > 0:
        g_confusion = dict()

        for mask in model.get_masks():
            with TimerRunning(timers, ['confusion_x'], mask.name):
                g_confusion[mask.name] = np.zeros(n_samples_confusion)

    if n_samples_diversity > 0:
        g_diversity = dict()
        g_diversity_norm_sum = dict()
        g_diversity_grad_sum = dict()

        for mask in model.get_masks():
            with TimerRunning(timers, ['diversity_x'], mask.name):
                g_diversity[mask.name] = np.zeros(n_samples_diversity)
                g_diversity_norm_sum[mask.name] = 0.0
                g_diversity_grad_sum[mask.name] = zeros(len(mask), device=device)

    iter_ = 0
    for x_train, y_train in data_loader:
        with TimerRunning(timers, ['confusion_x', 'diversity_x'], None):
            x_train, y_train = x_train.cuda(non_blocking=True), y_train.cuda(non_blocking=True)

            # calculate and store training loss
            y_train_hat = model(x_train)
            loss_train = loss_f(y_train_hat, y_train)

            # call .backward
            loss_train.backward()

            # this call should prevent the first mask called from having a way longer execution time than the others
            realize_grads(**model.weights)
            realize_grads(**model.biases)

        # GD
        if iter_ < n_batches_per_sample_diversity * n_samples_diversity:
            for mask in model.get_masks():
                with TimerRunning(timers, ['diversity_x'], mask.name):
                    grad_i = mask(grad_instead=True)

                    g_diversity_norm_sum[mask.name] += norm_w(grad_i)**2
                    g_diversity_grad_sum[mask.name] += grad_i

                    if (iter_ + 1) % n_batches_per_sample_diversity == 0:
                        g_diversity[mask.name][iter_ //
                                               n_batches_per_sample_diversity] = (g_diversity_norm_sum[mask.name] /
                                                                                  norm_w(
                                                                                    g_diversity_grad_sum[mask.name]
                                                                                  ) ** 2)
                        g_diversity_norm_sum[mask.name] = 0
                        g_diversity_grad_sum[mask.name] = np.zeros(len(mask))

        # GC
        if iter_ < 2 * n_pairs_per_sample_confusion * n_samples_confusion:
            if iter_ % 2 == 0:
                for mask in model.get_masks():
                    with TimerRunning(timers, ['confusion_x'], mask.name):
                        model.store_masked_grads([mask.name])
            else:
                for mask, TMU in model.paired_mask_and_memory_units():
                    with TimerRunning(timers, ['confusion_x'], mask.name):
                        grad_i = mask(grad_instead=True)
                        grad_j = TMU[0]
                        g_confusion[mask.name][iter_ // (2 * n_pairs_per_sample_confusion)] = \
                            min(g_confusion[mask.name][iter_ // (2 * n_pairs_per_sample_confusion)],
                                inner_w(grad_i, grad_j)/(norm_w(grad_i) * norm_w(grad_j)))

        model.zero_grad()
        iter_ += 1
        if iter_ >= n_iters:
            break

    model.clear_reserved_memory()

    if n_samples_confusion > 0:
        for mask_name in g_confusion.keys():
            with TimerRunning(timers, ['confusion_x'], mask_name):
                g_confusion[mask_name] = -np.minimum(g_confusion[mask_name], 0)

    return g_confusion, g_diversity


# ______________________________________________________________________________________________________________________
# def metrics_over_x_batch(model, loss_f, batch_x, batch_y,
#                          normalizing_constant: float = None, n_samples_normalizing_constant: int = 2) -> float:
#     """
#     This function calculates the relative effective rank using the gradient of the loss function
#     with respect to the input instead of the weights.
#
#     It modifies the network state by clearing all gradients
#     """
#     model.zero_grad()
#     batch_x.requires_grad = True
#
#     y_pred = model(batch_x)
#     loss = loss_f(y_pred, batch_y)
#
#     loss.backward()
#
#     # this shouldn't clear the gradient on batch_x because it isn't a network parameter
#     model.zero_grad()
#
#     return relative_effective_rank(batch_x.grad,
#                                    normalizing_constant=normalizing_constant,
#                                    n_samples=n_samples_normalizing_constant)


# ______________________________________________________________________________________________________________________
def coherence_over_x(masked_individual_grads, masked_grad, inner, induced_norm):
    numerator = 0
    denominator = 0
    b = masked_individual_grads.shape[0]
    for i in range(b):
        numerator += 1/b * inner(masked_grad, masked_individual_grads[i])
        denominator += 1/b * (induced_norm(masked_individual_grads[i])**2)

    return numerator / denominator


# ______________________________________________________________________________________________________________________
def relative_effective_rank(memory_matrix, normalizing_constant=None, n_samples=1, device='cuda:0'):
    """
    Calculate the relative effective rank of the input_ matrix as defined in the shattered gradient paper.
    Calculates and returns a normalizing constant if needed.
    """
    if device is not None:
        norm_2 = Itemizer(norm)
    else:
        norm_2 = norm
    effective_rank = (norm_2(memory_matrix, ord='fro') ** 2) / (norm_2(memory_matrix, ord=2) ** 2)

    if normalizing_constant is None:
        m, n = np.shape(memory_matrix)
        std_element_wise = std(memory_matrix)

        # calculate approximate normalizing constant
        normalizing_constant = 0
        for k in range(n_samples):
            # a = np.random.normal(scale=std_element_wise, size=(m, n))
            a = normal(0, float(std_element_wise), size=(m, n))
            if device is None:
                # normalizing_constant = np.trace(A.T@A)/(np.linalg.norm(A, 2)**2)
                normalizing_constant += (norm(a, ord='fro') ** 2) / ((norm(a, ord=2) ** 2) * n_samples)
            else:
                normalizing_constant += (norm(a, ord='fro').item() ** 2) / ((norm(a, ord=2).item() ** 2) * n_samples)

        return effective_rank / normalizing_constant, normalizing_constant

    else:
        return effective_rank / normalizing_constant
