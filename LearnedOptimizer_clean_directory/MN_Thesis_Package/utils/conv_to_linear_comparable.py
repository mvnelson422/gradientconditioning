import numpy as np
from scipy.optimize import minimize


def n_parameters_conv(hidden_channels, input_shape_batchless, size_output, filter_size=(3, 3),
                      bias=False, strides: list[int] = None, final_pooling_reduction_factor=1):
    """
    This function assumes there is never bias on the final linear layer.
    It also assumes that stride and padding are fixed throughout for no growth.
    """
    if strides is None:
        strides = [1]*len(hidden_channels)
    total_stride_reduction = 1

    bias_offset = 1 if bias else 0
    input_channels, input_h, input_v = input_shape_batchless

    n_channels = [input_channels] + hidden_channels
    n_params = list(np.zeros(len(n_channels), dtype=int))
    for k in range(len(n_params) - 1):
        n_params[k] = (filter_size[0] * filter_size[1] * n_channels[k] + bias_offset) * n_channels[k + 1] \
                      / total_stride_reduction
        if k > 0:
            total_stride_reduction *= strides[k - 1]
    n_params[-1] = (input_h * input_v * n_channels[-1]) // final_pooling_reduction_factor * size_output

    return n_params


def linear_hidden_layers_from_target_n_parameters(n_parameters_by_layer, input_size_flattened, output_size,
                                                  crelu_used: bool = False):
    total_params_target = np.sum(n_parameters_by_layer)
    n = len(n_parameters_by_layer) + 1

    def n_params_off(wl):
        if crelu_used:
            return np.abs(input_size_flattened * (wl//2) + wl**2/2 * (n-1) + output_size * wl - total_params_target)
        else:
            return np.abs(input_size_flattened * wl + wl ** 2 * (n - 1)
                          + output_size * wl - total_params_target)

    w_best = minimize(n_params_off, np.sqrt(total_params_target / n), bounds=[(0, np.inf)])

    upper = np.ceil(w_best.x)
    lower = np.floor(w_best.x)

    w = lower
    if n_params_off(upper) < n_params_off(lower):
        w = upper

    output_size_by_layer = (np.ones(n + 2) * w).astype(int).tolist()
    output_size_by_layer[0] = input_size_flattened
    output_size_by_layer[-1] = output_size

    # output_size_by_layer = np.zeros(len(n_parameters_by_layer) + 1).astype(int).tolist()

    # output_size_by_layer[0] = input_size_flattened
    # output_size_by_layer[1] = np.round(n_parameters_by_layer[0]/output_size_by_layer[0]).astype(int)
    # output_size_by_layer[-1] = output_size

    actual_n_parameters = list(np.zeros(len(output_size_by_layer), dtype=int))
    actual_n_parameters[0] = output_size_by_layer[0] * output_size_by_layer[1]

    for i in range(1, len(output_size_by_layer) - 1):
        actual_n_parameters[i] = output_size_by_layer[i] * output_size_by_layer[i + 1]
    actual_n_parameters[-1] = output_size_by_layer[-2] * output_size_by_layer[-1]

    if crelu_used:
        actual_n_parameters[:-1] = (np.array(actual_n_parameters[:-1])/2).astype(int).tolist()

    return output_size_by_layer, actual_n_parameters
