from __future__ import annotations
from time import perf_counter


# ______________________________________________________________________________________________________________________
class TimerError(Exception):
    """An exception used in the Timer class."""


# ______________________________________________________________________________________________________________________
class Timer:
    def __init__(self, name):
        self.name = name
        self.elapsed = 0.0
        self.last_start_time = None
        self.active_subtimer = None
        self.subtimers = dict()
        self.closed = False

    def start(self, time: float = None, subtimer: str = None):
        if self.closed:
            raise TimerError('This timer has been closed.')

        if self.last_start_time is not None:
            raise TimerError('Timer is already running.')

        if time is None:
            self.last_start_time = perf_counter()
        else:
            self.last_start_time = time

        self.active_subtimer = subtimer

    def stop(self, time: float = None):
        if self.closed:
            raise TimerError('This timer has been closed.')

        if self.last_start_time is None:
            raise TimerError("Timer isn't running")

        if time is None:
            elapsed = perf_counter() - self.last_start_time
        else:
            elapsed = time - self.last_start_time

        if self.active_subtimer:
            if self.active_subtimer not in self.subtimers:
                self.subtimers[self.active_subtimer] = 0
            self.subtimers[self.active_subtimer] += elapsed
        else:
            self.elapsed += elapsed

        self.last_start_time = None
        self.active_subtimer = None

    def close(self, time: float = None):
        if self.closed:
            raise TimerError('This timer was already closed.')

        if self.last_start_time:
            self.stop(time)

        self.closed = True


# ______________________________________________________________________________________________________________________
class TimerManager:
    def __init__(self):
        self.timers = dict()

    def add_timers(self, timer_names: list[str]):
        for timer_name in timer_names:
            if timer_name in self.timers.keys():
                raise TimerError(f'Timer {timer_name} was already created.')
            self.timers[timer_name] = Timer(timer_name)

    def close_timers(self, timer_names: list[str]):
        time = perf_counter()

        for timer_name in timer_names:
            self.timers[timer_name].close(time)

    def get_elapsed(self, timer_names: list[str] = None) -> dict:
        """
        Returns a dictionary with the elapsed time on the requested timers.
        If timer_names is None then returns all elapsed times.
        """
        times = dict()

        for timer_name, timer in self.timers.items():
            if timer_names is not None and timer_name not in timer_names:
                continue
            if timer.last_start_time:
                raise TimerError('One of the requested timers is still running.')
            if len(timer.subtimers.keys()) > 0:
                # print(timer_name, timer.elapsed, {subtimer_name: subtime
                #                                   for subtimer_name, subtime in timer.subtimers.items()})
                times[timer_name] = {sub_name: timer.elapsed + subtime
                                     for sub_name, subtime in timer.subtimers.items()}
            else:
                times[timer_name] = timer.elapsed

        return times

    def delete_timers(self, timer_names: list[str] = None):
        """Deletes specified or all timers if none."""

        for timer_name in timer_names:
            if self.timers[timer_name].last_start_time:
                raise TimerError('Stop timer before deleting.')
            del self.timers[timer_name]

    def get_timers(self, timer_names: list[str] = None):
        return [self.timers[timer_name] for timer_name in timer_names]


# ______________________________________________________________________________________________________________________
class TimerRunning:
    def __init__(self, timer_manager: TimerManager, timer_names: list[str], subtimer_name: str | type(None)):
        self.timers = timer_manager.get_timers(timer_names)
        self.subtimer_name = subtimer_name

    def __enter__(self):
        """
        On enter start the specified timers with the provided subtimer.
        """
        if self.timers is None:
            return self

        start_time = perf_counter()
        for timer in self.timers:
            timer.start(start_time, self.subtimer_name)

        return self

    def __exit__(self, *exc_info):
        """On exit stops the specified timers. That also means that all subtimers are set back to None."""
        if self.timers is None:
            return

        stop_time = perf_counter()
        for timer in self.timers:
            timer.stop(stop_time)
