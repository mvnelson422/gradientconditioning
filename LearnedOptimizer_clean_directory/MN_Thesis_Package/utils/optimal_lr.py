import numpy as np
from scipy.optimize import minimize
import torch
from warnings import warn

from tqdm import tqdm
from time import sleep


# ______________________________________________________________________________________________________________________
def lr_line_search(model, loss_f, x_train=None, y_train=None,
                   dataloader=None, n_trials=1,
                   max_lr=.1, tol=1e-9, method=None,
                   test_run=False, test_tol=1e-6, retval='median'):
    """ 
    Uses a scipy.optim method (not chosen yet) to find the optimal lr_ step. If test_run = True,
    tests every value in range and returns the tested learning rates and resulting loss values for plotting purposes.
    """

    with torch.no_grad():
        if n_trials > 1 or x_train is None or y_train is None:

            if dataloader is None:
                raise ValueError("dataset must be provided if x_train and y_train are not given")

            # temp_loader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, shuffle=True)
            n = 0
            optimal_lr = np.zeros(n_trials)

            for x, y in dataloader:
                x_train, y_train = x.cuda(non_blocking=True), y.cuda(non_blocking=True)

                def lr_optim_f(lr_):
                    lr_ = np.array(lr_).flatten()[0]
                    y_train_hat = model(x_train, project_lr=lr_)
                    return loss_f(y_train_hat, y_train).item()

                res = minimize(lr_optim_f, x0=np.array(tol), method=method, bounds=[(0, max_lr)], tol=tol)

                optimal_lr[n] = np.array(res.x).flatten()[0]
                n += 1

                if res.success is False:
                    warn(f'optimizer failed to converge: {res.message}')

                if n >= n_trials:
                    break
        else:
            def lr_optim_f(lr_):
                lr_ = np.array(lr_).flatten()[0]
                y_train_hat = model(x_train, project_lr=lr_)
                return loss_f(y_train_hat, y_train).item()

            res = minimize(lr_optim_f, x0=np.array(tol), method=method, bounds=[(0, max_lr)], tol=tol)
            optimal_lr = np.array(res.x).flatten()[0]

            if res.success is False:
                warn(f'optimizer failed to converge: {res.message}')

        if test_run and n_trials == 1:
            max_lr = min(max_lr, 2 * optimal_lr)
            n_iters = int(max_lr / test_tol)
            print(f'Calculating loss over space 0-{np.round(max_lr, 8)} to check result of optimization package.')
            sleep(.25)
            loop = tqdm(total=n_iters, position=0)

            lr_tests = np.linspace(0, max_lr, n_iters)
            losses = np.zeros(len(lr_tests))
            for i, lr in enumerate(lr_tests):
                losses[i] = lr_optim_f(lr)
                loop.update()
            loop.close()
            print(f'scipy.optimize.minimize was {100 * (1 - res.nit * tol / max_lr)} more efficient than a naive test.')
            return lr_tests, losses, optimal_lr

    if retval is None:
        return optimal_lr
    if retval == 'mean':
        return np.mean(optimal_lr)
    if retval == 'median':
        return np.median(optimal_lr)
    if retval == 'all_samples':
        return optimal_lr

# ___________________________________________________________________________________________________________________________________________
# def estimate_loss(model, train_loader, loss_f, project_lr, tolerable_loss_variance, min_batches, max_batches):
#     """
#     estimate the loss using train_loader
#     """
#     with torch.no_grad():
#         lossArray = []
#         for x, y in train_loader:
#             x = x.to('cuda')
#             y = y.to('cuda')

#             y_hat = model.forward(x, project_lr=project_lr)
#             lossArray += [loss_f(y_hat, y).item()]

#             if len(lossArray) >= max_batches:
#                 return np.mean(lossArray)
#             elif len(lossArray) >= min_batches and np.var(lossArray) < tolerable_loss_variance:
#                 return np.mean(lossArray)

# #___________________________________________________________________________________________________________________________________________
# def find_tolerable_loss(model, train_loader, loss_f, target_batches, n_tests):
#     target_var = 0
#     for i in range(n_tests):
#         losses = np.zeros(target_batches)
#         for j, (x, y) in enumerate(train_loader):
#             x = x.to('cuda')
#             y = y.to('cuda')

#             y_hat = model.forward(x)
#             losses[j] = loss_f(y_hat, y).item()

#             if j >= target_batches - 1:
#                 break
#         target_var += np.var(losses)/n_tests

#     return target_var


# ___________________________________________________________________________________________________________________________________________
# def find_optimal_lr_old(model, y_hat, test_loader, optimizer_class, loss_f, lr_bounds, n_points, findS=False):
#     """
#     Train the provided model several times for one step (using the same initialization repeatedly)
#     to identify the learning rate which results in the lowest loss.
#     """

#     def optimize_func(lr):
#         # reset the optimizer using a cloned model
#         temp_model = deepcopy(model).to('cuda')
#         optimizer = optimizer_class(temp_model.parameters(), lr=lr)

#         # recalculate loss using the same y_hat, calculate backwards pass, and step
#         # TODO: find a way to do this more efficiently (i.e. without recalculating backwards pass)
#         temp_model.zero_grad()
#         loss = loss_f(y_hat, y)
#         loss.backward()
#         optimizer.step()

#         # evaluate the loss using the whole testing set
#         with torch.no_grad():
#             lossArray = []
#             for x, y in test_loader:
#                 x = x.to('cuda')
#                 y = y.to('cuda')

#                 y_hat = temp_model(x)
#                 lossArray += [loss_f(y_hat, y).item()]

#         return np.mean(lossArray)

#     # test all learning rates in specified geomspace
#     test_points = np.geomspace(lr_bounds[0], lr_bounds[1], n_points)

#     # test each point in geomspace
#     optimal_lr = test_points[0]
#     low = optimize_func(optimal_lr)

#     for lr in test_points[1:]:
#         loss = optimize_func(lr)
#         if loss < low:
#             low = loss
#             optimal_lr = lr

#     return optimal_lr, low
