from __future__ import annotations

from matplotlib import pyplot as plt
from matplotlib.ticker import ScalarFormatter
from os.path import exists
from os import mkdir, remove, listdir
from torch import Tensor
import numpy as np
from torch import save, load
from torch.utils.data import DataLoader
from time import time
from re import split
import tikzplotlib
import traceback
from sys import exc_info
from tqdm import tqdm


from .main_training_loop import train


# ______________________________________________________________________________________________________________________
def format_exception(e):
    """Helper function to print stack trace nicely when crashes occur that don't kill the training scheduler"""
    exception_list = traceback.format_stack()
    exception_list = exception_list[:-2]
    exception_list.extend(traceback.format_tb(exc_info()[2]))
    exception_list.extend(traceback.format_exception_only(exc_info()[0], exc_info()[1]))

    exception_str = f"Exception: {type(e)}: {e}\nTraceback (most recent call last):\n"
    exception_str += "".join(exception_list)

    # Removing the last \n
    exception_str = exception_str[:-1]

    return exception_str


# ______________________________________________________________________________________________________________________
def numeric_order(text):
    """Default ordering argument for test execution. Sorts ascending based on the highest integer found in the name."""
    def digit_convert(txt):
        return int(txt) if txt.isdigit() else -1

    def remove_digit(txt):
        return '' if txt.isdigit() else txt
    order_int = max([digit_convert(c) for c in split(r'(\d+)', text)])
    order_str = ''.join([remove_digit(c) for c in split(r'(\d+)', text)])
    if order_int == -1:
        order_int = 99999999999999999999999
    return [order_int, order_str]


# ______________________________________________________________________________________________________________________
class TrainTestExecutionError(Exception):
    """Exception used exclusively by the classes in TrainTestScheduler.py."""


# ______________________________________________________________________________________________________________________
class TrainingScheduler(object):
    def __init__(self, root_folder='TrainingSchedulerFiles', priority_sorting_key=numeric_order):
        self.root = root_folder

        if not exists(self.root):
            print(f'Root folder not found. Creating at: {self.root}')
            mkdir(self.root)

        for template_type in ['test', 'dataset', 'model']:
            if not exists(f'{self.root}/{template_type}_templates'):
                print(f'{template_type.capitalize()} templates folder not found. ' +
                      f'Creating at {self.root}/{template_type}_templates')
                mkdir(f'{self.root}/{template_type}_templates')

        self.current_dataset_template = None
        self.train = None
        self.test = None
        self.train_loader = None
        self.test_loader = None
        self.priority_key = priority_sorting_key

    # __________________________________________________________________________________________________________________
    def create_model_template(self, template_name: str, model_constructor, num_classes: int, replace: bool = False,
                              **model_constructor_inputs):
        fileloc = f'{self.root}/model_templates/{template_name}.model_template'

        if exists(fileloc):
            if replace:
                print(f'Overwriting previous test settings for {template_name}.')
            else:
                print(f'Model settings have already been saved under the name {template_name}. ' +
                      'Set replace=True if you wish to update these.')

        save({'name': template_name, 'model_constructor': model_constructor, 'num_classes': num_classes,
              'model_constructor_inputs': model_constructor_inputs},
             fileloc)

    # __________________________________________________________________________________________________________________
    def create_test_template(self, template_name: str, mask_sizes: list[float],
                             train_batch_size: int, test_batch_size: int,
                             rer_constants_fileloc: str, n_samples_rer_constants: int,
                             sample_every_n_iterations: int, epochs_to_train: int,
                             lr: float, optimizer_constructor, loss_function,
                             lr_scheduler_constructor,
                             lr_scheduler_constructor_settings: dict,
                             replace: bool = False, **metric_settings):
        fileloc = f'{self.root}/test_templates/{template_name}.test_template'

        if exists(fileloc):
            if replace:
                print(f'Overwriting previous test settings for {template_name}.')
            else:
                print(f'Test settings have already been saved under the name {template_name}. ' +
                      'Set replace=True if you wish to update these.')

        save({'name': template_name, 'mask_sizes': mask_sizes,
              'train_batch_size': train_batch_size, 'test_batch_size': test_batch_size,
              'rer_constants_fileloc': rer_constants_fileloc, 'n_samples_rer_constants': n_samples_rer_constants,
              'sample_every_n_iterations': sample_every_n_iterations, 'epochs_to_train': epochs_to_train,
              'lr': lr, 'optimizer_constructor': optimizer_constructor,
              'loss_function': loss_function,
              'lr_scheduler_constructor': lr_scheduler_constructor,
              'lr_scheduler_constructor_settings': lr_scheduler_constructor_settings,
              'metric_settings': metric_settings},
             fileloc)

    # __________________________________________________________________________________________________________________
    def create_dataset_template(self, template_name: str, source_folder: str, transform, n_classes: int,
                                dataset_importer, replace: bool = False):
        """dataset_importer format expected like those from torchvision.datasets"""
        fileloc = f'{self.root}/dataset_templates/{template_name}.dataset_template'

        if exists(fileloc):
            if replace:
                print(f'Overwriting previous dataset settings for {template_name}.')
            else:
                print(f'Dataset settings have already been saved under the name {template_name}. ' +
                      'Set replace=True if you wish to update these.')

        if source_folder is None:
            source_folder = f'{self.root}/dataset_templates/{template_name}/data'
            print(f'No source folder for the dataset was provided. Defaulting to {source_folder}.')

        save({'name': template_name,
              'source_folder': source_folder,
              'transform': transform,
              'n_classes': n_classes,
              'dataset_importer': dataset_importer},
             fileloc)

    # __________________________________________________________________________________________________________________
    def schedule_test(self, test_name: str, n_tries: int, test_template_name: str,
                      dataset_template_names: list[str], model_template_names: list[str],
                      overwrite_previous: bool = False, clear_conflicting_results_if_scope_reduced: bool = False):
        """overwrite_previous = True is not required to extend the scope of a test with no other changes."""
        fileloc = f'{self.root}/test_schedule.training_scheduler'

        if exists(fileloc):
            scheduled_tests = load(fileloc)
        else:
            scheduled_tests = dict()

        if test_name in scheduled_tests.keys():
            difference_str = ''
            missing_datasets = []
            missing_models = []
            identical = True

            if n_tries != scheduled_tests[test_name]['n_tries']:
                identical = False
            if n_tries < scheduled_tests[test_name]['n_tries']:
                difference_str += f"n_tries was reduced from {scheduled_tests[test_name]['n_tries']} to {n_tries}.\n"

            if not test_template_name == scheduled_tests[test_name]['test_template_name']:
                identical = False
                difference_str += f'test_template_name does not match: {test_template_name} != ' + \
                                  scheduled_tests[test_name]['test_template_name'] + '\n'

            if not (set(scheduled_tests[test_name]['dataset_template_names'])
                    .issubset(set(dataset_template_names))):
                identical = False
                diff = set(scheduled_tests[test_name]['dataset_template_names']) - set(dataset_template_names)
                missing_datasets += list(diff)
                difference_str += f"datasets previously included are missing: {diff}\n"
            elif not set(dataset_template_names).issubset(set(scheduled_tests[test_name]['dataset_template_names'])):
                identical = False

            if not (set(scheduled_tests[test_name]['model_template_names'])
                    .issubset(set(model_template_names))):
                identical = False
                diff = set(scheduled_tests[test_name]['model_template_names']) - set(model_template_names)
                missing_models += list(diff)
                difference_str += f"models previously included are missing: {diff}\n"
            elif not set(model_template_names).issubset(set(scheduled_tests[test_name]['model_template_names'])):
                identical = False

            if identical:
                print('A prior test was found with the same settings, so no changes were made.')
            elif difference_str == '':
                print('A prior scheduled/completed test was found under the same name, but the provided parameters ' +
                      'encompass the scope of the prior test so it was updated.')

            elif not overwrite_previous:
                raise TrainTestExecutionError(f'A test with the name {test_name} has already been scheduled and it ' +
                                              f'includes elements that are not encompassed by the scope of the ' +
                                              f'test just provided: \n{difference_str}')

            else:
                directories_to_clear = []
                to_clear_if_empty = []
                for dataset_template_name in missing_datasets:
                    for model_template_name in missing_models:
                        storage_loc = f'{self.root}/results/{test_template_name}/{dataset_template_name}/' + \
                                      f'{model_template_name}/'
                        if exists(storage_loc):
                            directories_to_clear += [storage_loc]
                            to_clear_if_empty += [f'{self.root}/results/{test_template_name}/{dataset_template_name}/']
                to_clear_if_empty = list(set(to_clear_if_empty))

                if len(directories_to_clear) > 0:
                    if clear_conflicting_results_if_scope_reduced:
                        for directory in directories_to_clear:
                            remove(directory)
                        for directory in to_clear_if_empty:
                            if len(listdir(directory)) == 0:
                                remove(directory)
                        print(f'A previous test with the name {test_name} was found containing results not included ' +
                              'in the scope of the new test, so those results were cleared and the test was ' +
                              'rescheduled (excluding extra results caused by a reduction in n_tries). ' +
                              f'Reduced scope: \n{difference_str}')

                    elif len(difference_str) > 0:
                        raise TrainTestExecutionError(f'A test with the name {test_name} was executed previously ' +
                                                      'and returned valid results using templates not in the new, ' +
                                                      'scope, so this test cannot be rescheduled without setting ' +
                                                      'clear_conflicting_results_if_scope_reduced=True or ensuring ' +
                                                      'that the scope of the newly scheduled test encompasses that of' +
                                                      f'old one one. Differences:\n{difference_str}')

                else:
                    if len(missing_datasets) + len(missing_models) == 0:
                        print(f'A previous test with the name {test_name} was found, but it had not been run and was ' +
                              f'replaced. Scope reduced:\n{difference_str}')
                    else:
                        print(f'A previous test with the name {test_name} with some results was found, but all saved ' +
                              'results fall in the scope of the new test, so the test has been updated without ' +
                              f'issue. Scope reduced:\n{difference_str}')

        scheduled_tests[test_name] = {'n_tries': n_tries,
                                      'test_template_name': test_template_name,
                                      'dataset_template_names': dataset_template_names,
                                      'model_template_names': model_template_names}
        save(scheduled_tests, fileloc)

    def get_scheduled_tests(self):
        fileloc = f'{self.root}/test_schedule.training_scheduler'
        if not exists(fileloc):
            raise TrainTestExecutionError('No tests have been scheduled yet. Use schedule_test() to schedule one.')
        return load(fileloc)

    def scheduled_test_results(self, compact=False):
        scheduled_tests = self.get_scheduled_tests()

        for test_name, test in scheduled_tests.items():
            test_template, _, _ = self.load_templates(test_template_name=test['test_template_name'])
            for dataset_template_name in sorted(test['dataset_template_names'], key=self.priority_key):
                for model_template_name in sorted(test['model_template_names'], key=self.priority_key):
                    output_loc = f"{self.root}/results/" + \
                                 f"{test['test_template_name']}/{dataset_template_name}/{model_template_name}/"

                    complete, partial, incomplete, failed = 0, 0, 0, 0
                    error = None
                    run_time = 0
                    for k in range(test['n_tries']):
                        prior_stats_loc = f'test_{k+1}.stats'
                        if exists(output_loc + prior_stats_loc):
                            prior_run_stats = load(output_loc + prior_stats_loc)
                            if prior_run_stats['error'] is not None:
                                failed += 1
                                error = prior_run_stats['error']
                            elif prior_run_stats['epochs_completed'] >= test_template['epochs_to_train']:
                                complete += 1
                                run_time += prior_run_stats['run_time']
                            else:
                                partial += 1
                        else:
                            incomplete += 1

                    if complete >= test['n_tries']:
                        yield f'+ {test_name} for {model_template_name} on {dataset_template_name} completed in ' + \
                                f'{run_time/60:.1f} minutes'
                    elif complete + partial + failed == 0:
                        yield f'- {test_name} for {model_template_name} on {dataset_template_name} not yet started'
                    else:
                        if compact:
                            if failed > 0:
                                yield f'/ {test_name} for {model_template_name} on {dataset_template_name} failed'
                            elif partial > 0:
                                yield f'% {test_name} for {model_template_name} on {dataset_template_name} ' + \
                                      'partially complete'
                        else:
                            yield f"- {test_name} for {model_template_name} on {dataset_template_name}:" + \
                                  f"\n    failed: {failed}/{test['n_tries']} with {type(error)}: {error}" + \
                                  f"\n    complete: {complete}/{test['n_tries']}, " + \
                                  f"avg time: {(run_time/max(complete, 1))/60:.1f} minutes" + \
                                  f"\n    partially complete: {partial}/{test['n_tries']}" + \
                                  f"\n    incomplete: {incomplete}/{test['n_tries']}\n"

    # __________________________________________________________________________________________________________________
    def execute_tests(self, test_names: list[str], rerun: bool = False, retry: bool = False,
                      max_epochs_each: int = None, debug: bool = False, epoch_only_tqdm: bool = False):
        scheduled_tests = self.get_scheduled_tests()

        for test_name in test_names:
            if test_name not in scheduled_tests.keys():
                raise TrainTestExecutionError('The provided test was not found in scheduled tests.')

            test = scheduled_tests[test_name]
            n_tries = test['n_tries']
            test_template, _, _ = self.load_templates(test_template_name=test['test_template_name'])

            for dataset_template_name in sorted(test['dataset_template_names'], key=self.priority_key):
                _, dataset_template, _ = self.load_templates(dataset_template_name=dataset_template_name)
                for model_template_name in sorted(test['model_template_names'], key=self.priority_key):
                    _, _, model_template = self.load_templates(model_template_name=model_template_name)
                    output_loc = f'{self.root}/results/' + \
                                 f"{test_template['name']}/{dataset_template_name}/{model_template_name}/"

                    for k in range(1, n_tries+1):
                        previous_results = None
                        if exists(output_loc + f'test_{k}.stats'):
                            previous_results = load(output_loc + f'test_{k}.stats')

                        if not rerun and previous_results is not None and \
                                previous_results['epochs_completed'] >= test_template['epochs_to_train']:
                            yield f'Test {test_name} {k}/{n_tries} completed previously for ' + \
                                  f'{model_template_name} on {dataset_template_name}'

                        elif not retry and not rerun and previous_results is not None and \
                                previous_results['error'] is not None:
                            yield f'Test {test_name} {k}/{n_tries} failed previously for ' + \
                                  f'{model_template_name} on {dataset_template_name}. ' + \
                                  'Set retry=True to attempt failed tests again. See error stack: ' + \
                                  f"{previous_results['error']}"
                            break

                        else:
                            error = self.perform_test(test_template, dataset_template, model_template,
                                                      k, max_epochs_each=max_epochs_each, rerun=rerun, debug=debug,
                                                      epoch_only_tqdm=epoch_only_tqdm)
                            error_stack = None
                            if error is not None:
                                (error, error_stack) = error

                            if error is not None:
                                yield f'Test {test_name} {k}/{n_tries} failed to complete for ' + \
                                      f'{model_template_name} on {dataset_template_name} because of a ' + \
                                      f'{type(error)}:\n\n{error_stack}\n\n'

                                break
                            else:
                                yield f'Test {test_name} {k}/{n_tries} completed successfully for ' + \
                                      f'{model_template_name} on {dataset_template_name}'

    def perform_test(self, test_template: dict, dataset_template: dict, model_template: dict,
                     test_index: int, max_epochs_each: int = None, rerun: bool = False, debug: bool = False,
                     epoch_only_tqdm: bool = False):
        output_location = f"{self.root}/results/{test_template['name']}/" + \
                          f"{dataset_template['name']}/{model_template['name']}/"
        model_name = f'test_{test_index}.model'
        results_name = f'test_{test_index}.results'
        test_stats_name = f'test_{test_index}.stats'
        iters_previously_completed = 0
        epochs_previously_completed = 0

        if not exists(output_location):
            if not exists(f"{self.root}/results/{test_template['name']}/{dataset_template['name']}"):
                if not exists(f"{self.root}/results/{test_template['name']}/"):
                    if not exists(f"{self.root}/results/"):
                        mkdir(f"{self.root}/results/")
                    mkdir(f"{self.root}/results/{test_template['name']}/")
                mkdir(f"{self.root}/results/{test_template['name']}/{dataset_template['name']}")
            mkdir(output_location)

        # if rerun is set to True delete previous results immediately so no confusion occurs if the test run fails.
        if rerun:
            for filename in [model_name, results_name, test_stats_name]:
                if exists(output_location + filename):
                    remove(output_location + filename)
        else:
            if exists(output_location + test_stats_name):
                prior_stats = load(output_location + test_stats_name)
                iters_previously_completed = prior_stats['iterations_completed']
                epochs_previously_completed = prior_stats['epochs_completed']

        if epochs_previously_completed < test_template['epochs_to_train']:
            opacus_storage_required = (test_template['metric_settings']['conditioning_x']
                                       or ((test_template['metric_settings']['targeted_sampling_w']) and
                                           (test_template['metric_settings']['conditioning_w']
                                            or test_template['metric_settings']['confusion_w']
                                            or test_template['metric_settings']['diversity_w']
                                            or test_template['metric_settings']['coherence_w'])))
            opacus_required = (opacus_storage_required or test_template['metric_settings']['weight_grad_rank_x'] or
                               test_template['metric_settings']['coherence_x'])

            try:
                # create ModelHandler object to handle training and test execution for us
                model_handler = self._create_handler(dataset_template=dataset_template, test_template=test_template,
                                                     model_template=model_template,
                                                     iters_previously_completed=iters_previously_completed,
                                                     epochs_previously_completed=epochs_previously_completed,
                                                     output_location=output_location,
                                                     model_name=model_name, results_name=results_name,
                                                     opacus_required=opacus_required)
            except Exception as e:
                traceback_str = format_exception(e)
                save({'iterations_completed': 0,
                      'epochs_completed': 0,
                      'run_time': 0, 'error': e, 'stack': traceback_str},
                     output_location + test_stats_name)
                return e, traceback_str

            if max_epochs_each:
                last_epoch = min(test_template['epochs_to_train'] + 1,
                                 model_handler.epochs_completed + 1 + max_epochs_each)
            else:
                last_epoch = test_template['epochs_to_train'] + 1

            print(f"beginning {test_template['name']} test #{test_index} for {model_template['name']} on " +
                  f"{dataset_template['name']}. Epochs completed: {model_handler.epochs_completed}/" +
                  f"{test_template['epochs_to_train']}\n" +
                  f"    opacus required: {opacus_required}\n" +
                  f"    opacus storage required: {opacus_storage_required}\n")

            loop = tqdm(total=last_epoch-1, initial=model_handler.epochs_completed, disable=not epoch_only_tqdm)
            for epoch in range(model_handler.epochs_completed+1, last_epoch):
                t0 = time()
                try:
                    train(model_handler=model_handler, max_epochs=epoch,
                          train_loader=self.train_loader, test_loader=self.test_loader,
                          rer_constants_fileloc=test_template['rer_constants_fileloc'],
                          n_samples_normalizing_constant=test_template['n_samples_rer_constants'],
                          sample_every=test_template['sample_every_n_iterations'], debug=debug,
                          tqdm_disabled=epoch_only_tqdm)
                    t_total = time()-t0
                    loop.update()

                except Exception as e:
                    traceback_str = format_exception(e)
                    save({'iterations_completed': model_handler.iters_completed,
                          'epochs_completed': model_handler.epochs_completed,
                          'run_time': time()-t0, 'error': e, 'stack': traceback_str},
                         output_location + test_stats_name)
                    loop.close()
                    return e, traceback_str

                save({'iterations_completed': model_handler.iters_completed,
                     'epochs_completed': model_handler.epochs_completed, 'run_time': t_total,
                      'error': None, 'stack': None},
                     output_location + test_stats_name)
                save({'model': model_handler.model, 'optimizer': model_handler.optimizer,
                      'lr_scheduler': model_handler.lr_scheduler},
                     output_location + model_name)
                save(model_handler.get_results_dictionary(), output_location + results_name)
            loop.close()
        else:
            print(f"{test_template['name']} test #{test_index} for {model_template['name']} on " +
                  f"{dataset_template['name']} already completed.\n")

    # __________________________________________________________________________________________________________________
    def load_templates(self, test_template_name: str = None,
                       dataset_template_name: str = None,
                       model_template_name: str = None):
        test_template, dataset_template, model_template = None, None, None

        if dataset_template_name:
            if self.current_dataset_template and dataset_template_name == self.current_dataset_template['name']:
                dataset_template = self.current_dataset_template
            else:
                dataset_template_loc = f'{self.root}/dataset_templates/{dataset_template_name}.dataset_template'
                if dataset_template_name:
                    if exists(dataset_template_loc):
                        dataset_template = load(dataset_template_loc)
                    else:
                        raise TrainTestExecutionError(f'Provided dataset template {dataset_template_name} ' +
                                                      'was not found.')

        if test_template_name:
            test_template_loc = f'{self.root}/test_templates/{test_template_name}.test_template'
            if exists(test_template_loc):
                test_template = load(test_template_loc)
            else:
                raise TrainTestExecutionError(f'Provided test template {test_template_name} was not found.')

        if model_template_name:
            model_template_loc = f'{self.root}/model_templates/{model_template_name}.model_template'
            if exists(model_template_loc):
                model_template = load(model_template_loc)
            else:
                raise TrainTestExecutionError(f'Provided model template {model_template_name} was not found.')

        return test_template, dataset_template, model_template

    # __________________________________________________________________________________________________________________
    @staticmethod
    def _initialize_model(model_template: dict, input_sample: Tensor):
        """Initialize a model according to the provided model_template and data sample."""
        return model_template['model_constructor'](x_typical=input_sample, num_classes=model_template['num_classes'],
                                                   **model_template['model_constructor_inputs'])

    def _update_data(self, dataset_template: dict, train_batch_size: int, test_batch_size: int):
        """Update the current state of self to make sure dataloaders are correct with given templates."""
        if self.current_dataset_template is None or \
                not dataset_template['name'] == self.current_dataset_template['name']:
            self.current_dataset_template = dataset_template
            self.train_loader = None
            self.train = None
            self.test_loader = None
            self.test = None

        if self.train is None:
            self.train = dataset_template['dataset_importer'](dataset_template['source_folder'], download=True,
                                                              transform=dataset_template['transform'], train=True)
        if self.test is None:
            self.test = dataset_template['dataset_importer'](dataset_template['source_folder'], download=True,
                                                             transform=dataset_template['transform'], train=False)

        if self.train_loader is None or (self.train_loader.dataset != self.train
                                         or self.train_loader.batch_size != train_batch_size):
            self.train_loader = DataLoader(self.train, batch_size=train_batch_size, shuffle=True)

        if self.test_loader is None or (self.test_loader.dataset != self.train
                                        or self.test_loader.batch_size != train_batch_size):
            self.test_loader = DataLoader(self.test, batch_size=test_batch_size, shuffle=True)

    def _create_handler(self, dataset_template: dict, test_template: dict, model_template: dict,
                        iters_previously_completed: int, epochs_previously_completed: int,
                        output_location: str, model_name: str, results_name: str,
                        opacus_required: bool):

        # make sure that the current data loaders are compatible with provided templates
        self._update_data(dataset_template, test_template['train_batch_size'], test_template['test_batch_size'])

        # if model was saved previously then load it
        if exists(output_location + model_name) and exists(output_location + results_name):
            # an error will be raised here if one file exists but the other doesn't
            model_dict = load(output_location + model_name)
            model = model_dict['model']
            optimizer = model_dict['optimizer']
            lr_scheduler = model_dict['lr_scheduler']
            if model.opacus_wrapped:
                # model.opacus_wrapped['opacus'].to_standard_module()
                model.opacus_wrapped['opacus'].remove_hooks()
                model.opacus_wrapped['opacus']._clean_up_attributes()  # noqa
                model.enable_opacus()
            model = model.cuda()

            results = load(output_location + results_name)
            prior_metric_results = results['metrics']
            prior_timing_results = results['timing']

        # otherwise initialize a model and prepare it for training
        else:
            data_sample = None
            for x_train, y_train in self.train_loader:
                data_sample = x_train.cuda(non_blocking=True)
                break

            model = self._initialize_model(model_template, data_sample)
            del data_sample
            model.cuda()

            # enable opacus if it is required for the provided test template
            if opacus_required:
                model.enable_opacus()

            optimizer = test_template['optimizer_constructor'](model.parameters(), lr=test_template['lr'])
            lr_scheduler = (test_template['lr_scheduler_constructor']
                            (optimizer, **test_template['lr_scheduler_constructor_settings']))

            # initialize masks as specified by the provided test template
            sizes_count = dict()
            for mask_size in test_template['mask_sizes']:
                if mask_size not in sizes_count.keys():
                    sizes_count[mask_size] = 0
                else:
                    sizes_count[mask_size] += 1

                model.initialize_mask(mask_size, f'{int(100*mask_size)}%_{sizes_count[mask_size]}')

            prior_metric_results, prior_timing_results = None, None

        # construct and return a model handler
        model_handler = ModelHandler(model=model, loss_function=test_template['loss_function'],
                                     optimizer=optimizer,
                                     lr_scheduler=lr_scheduler,
                                     iters_previously_completed=iters_previously_completed,
                                     epochs_previously_completed=epochs_previously_completed,
                                     num_classes=dataset_template['n_classes'],
                                     prior_metric_results=prior_metric_results,
                                     prior_timing_results=prior_timing_results)

        model_handler.set_metric_settings(**test_template['metric_settings'])

        return model_handler


# ______________________________________________________________________________________________________________________
class ModelHandler(object):
    @classmethod
    def load(cls, fileloc: str):
        var_dict = load(fileloc)
        updated_handler = ModelHandler(var_dict['model'],
                                       var_dict['loss_function'],
                                       var_dict['optimizer'],
                                       var_dict['lr_scheduler'],
                                       var_dict['num_classes'])
        for attr in ['iters_completed', 'epochs_completed', 'metric_settings', 'metrics', 'timing']:
            updated_handler.__setattr__(attr, var_dict[attr])
        return updated_handler

    def __init__(self, model, loss_function, optimizer, lr_scheduler, num_classes,
                 iters_previously_completed: int = 0, epochs_previously_completed: int = 0,
                 prior_metric_results: dict = None, prior_timing_results: dict = None):
        self.model = model
        self.loss_function = loss_function
        self.optimizer = optimizer
        self.lr_scheduler = lr_scheduler
        self.iters_completed = iters_previously_completed
        self.epochs_completed = epochs_previously_completed
        self.num_classes = num_classes

        # metric sampling settings
        self.metric_settings = None
        self.metrics = prior_metric_results
        self.timing = prior_timing_results

    def save(self, fileloc: str):
        var_dict = vars(self)
        save(var_dict, fileloc)

    def set_metric_settings(self, **kwargs):
        self.metric_settings = kwargs

    def get_metric_settings(self):
        return self.metric_settings

    def report_metrics(self, metric_dict: dict, timing_dict: dict, training_loss: float,
                       testing_loss: float, test_accuracy: float, lr: float):  # , per_class_test_accuracy=None):
        if self.metrics is None:
            self.metrics = dict()
            self.timing = dict()

            self.metrics['iter'] = []
            self.metrics['epoch'] = []
            self.metrics['training_loss'] = []
            self.metrics['testing_loss'] = []
            self.metrics['test_accuracy'] = []
            self.metrics['lr'] = []
            # self.metrics['per_class_test_accuracy'] = []

            for key in metric_dict:
                if type(metric_dict[key]) is dict:
                    self.metrics[key] = dict()
                    self.timing[key] = dict()
                    for key_2 in metric_dict[key]:
                        self.metrics[key][key_2] = []
                        self.timing[key][key_2] = []
                else:
                    self.metrics[key] = []
                    self.timing[key] = []

        self.metrics['iter'] += [self.iters_completed]
        self.metrics['epoch'] += [self.epochs_completed]
        self.metrics['training_loss'] += [training_loss]
        self.metrics['testing_loss'] += [testing_loss]
        self.metrics['test_accuracy'] += [test_accuracy]
        self.metrics['lr'] += [lr]
        # self.metrics['per_class_test_accuracy'] += [per_class_test_accuracy]

        for key in metric_dict:
            if type(metric_dict[key]) is dict:
                for key_2 in metric_dict[key]:
                    self.metrics[key][key_2] += [metric_dict[key][key_2]]
                    self.timing[key][key_2] += [timing_dict[key][key_2]]
            else:
                self.metrics[key] += [metric_dict[key]]
                self.timing[key] += [timing_dict[key]]

    def increment_epoch(self):
        self.epochs_completed += 1

    def increment_iteration(self):
        self.iters_completed += 1

    def get_results_dictionary(self):
        out = dict()
        out['metrics'] = self.metrics
        out['timing'] = self.timing
        return out

    def plot_metrics(self, len_train_loader: int = None, fig_size: tuple[int] = (12, 6),
                     log_plots: tuple[str] = ('conditioning_x', 'conditioning_w'),
                     save_figs: bool = False, filename_postfix: str = ''):

        # correct for not stepping each time metrics were calculated and then convert to epochs
        x = (np.array(self.metrics['iter']) - np.arange(len(self.metrics['iter'])))
        x_label = 'iterations'
        if len_train_loader is not None:
            x = x / len_train_loader
            x_label = 'epochs'

        for k1, d2 in self.metrics.items():
            if k1 in ['iter', 'epoch', 'training_loss', 'testing_loss']:
                continue

            fig, ax = plt.subplots(2, 1)
            fig.set_figwidth(fig_size[0])
            fig.set_figheight(fig_size[1])

            ax[0].plot(x, self.metrics['training_loss'], label='training')
            ax[0].plot(x, self.metrics['testing_loss'], label='testing')
            ax[0].set_ylabel('loss')
            ax[0].legend()

            if type(d2) is dict:
                for mask_name, values in reversed(d2.items()):
                    to_plot = values
                    if len(to_plot) > len(self.metrics['iter']):
                        to_plot = to_plot

                    if k1 in log_plots:
                        ax[1].semilogy(x, to_plot, label=mask_name.replace("%", r"\%"))
                    else:
                        ax[1].plot(x, to_plot, label=mask_name.replace("%", r"\%"))

                plt.legend()
            else:
                to_plot = d2
                if len(to_plot) > len(self.metrics['iter']):
                    to_plot = to_plot

                if k1 in log_plots:
                    ax[1].semilogy(x, to_plot)
                else:
                    ax[1].plot(x, to_plot)

            ax[1].set_title(k1)
            ax[1].set_xlabel(x_label)
            ax[1].legend()
            plt.tight_layout()

            if save_figs:
                tikzplotlib.save(f'plots/{k1}{filename_postfix}.tex',
                                 axis_width="6.5in", axis_height="1.5in", textsize=8.0)
            else:
                plt.show()

    def plot_mask_accuracy_comparisons(self, fig_size: tuple = (8, 3),
                                       save_figs: bool = False, filename_postfix: str = ''):
        # names and keys of each metric
        metrics = {'conditioning_x': 'Gradient Conditioning in X',
                   'weight_grad_rank_x': 'Relative Effective Rank Weight Grads',
                   'coherence_x': 'Gradient Coherence',
                   'confusion_x': 'Gradient Confusion',
                   'diversity_x': 'Gradient Diversity',
                   'conditioning_w': 'Gradient Conditioning in W',
                   'confusion_w': 'W-Adapted Gradient Confusion',
                   'diversity_w': 'W-Adapted Gradient Diversity',
                   'coherence_w': 'W-Adapted Gradient Coherence'}

        for metric_key, metric_name in metrics.items():
            masks_dict = dict()
            for mask in self.model.get_masks():
                if mask.percent_included not in masks_dict:
                    masks_dict[mask.percent_included] = []
                masks_dict[mask.percent_included] += [mask.name]

            fig, ax = plt.subplots(1, 2)
            fig.set_figwidth(fig_size[0])
            fig.set_figheight(fig_size[1])

            if 1.0 not in masks_dict.keys():
                raise ValueError('No full mask was included to compare against.')
            full_mask_name = masks_dict[1.0][0]

            # compute the median absolute error and timing for the flattened results vectors
            aggregated_masks_dict = dict()
            for mask_size, mask_names in masks_dict.items():
                aggregated_masks_dict[mask_size] = dict()
                abs_differences_samples = []
                samples_for_std = []
                time_samples = []

                for mask_name in mask_names:
                    if mask_size == 1.0:
                        abs_differences_samples += [0]
                    else:
                        abs_differences_samples += list(np.abs(np.array(self.metrics[metric_key][mask_name])
                                                               .flatten() -
                                                               np.array(self.metrics[metric_key][full_mask_name])
                                                               .flatten()))
                        samples_for_std += [np.array(self.metrics[metric_key][mask_name]).flatten()]
                    time_samples += list(self.timing[metric_key][mask_name])
                aggregated_masks_dict[mask_size]['median_difference'] = np.median(abs_differences_samples)
                if mask_size == 1.0:
                    aggregated_masks_dict[mask_size]['std'] = 1
                else:
                    aggregated_masks_dict[mask_size]['std'] = np.std(samples_for_std)
                aggregated_masks_dict[mask_size]['mean_time'] = np.mean(time_samples)

            # group results together for timing
            abs_median_differences = []
            stds = []
            mean_timing = []
            mask_sizes = []
            for mask_size in aggregated_masks_dict.keys():
                abs_median_differences += [aggregated_masks_dict[mask_size]['median_difference']]
                stds += [aggregated_masks_dict[mask_size]['std']]
                mean_timing += [aggregated_masks_dict[mask_size]['mean_time']]
                mask_sizes += [mask_size]

            order = np.argsort(mask_sizes)[::-1]
            abs_median_differences = np.array(abs_median_differences)[order]
            stds = np.array(stds)[order]
            mean_timing = np.array(mean_timing)[order]
            mask_sizes = np.array(mask_sizes)[order]

            # plot
            ax[0].semilogx(mask_sizes*100, 100*abs_median_differences/stds)
            ax[1].semilogx(mask_sizes*100, mean_timing)
            # ax[0][1].loglog(mask_sizes*100, 100*abs_median_differences/stds)
            # ax[1][1].loglog(mask_sizes*100, mean_timing)

            if not save_figs:
                ax[0].set_title(f'{metric_name} error relative to {mask_sizes[order[0]]*100}% mask value')
            else:
                ax[0].set_title('Median Absolute Error')
            ax[0].set_xlabel('mask size (%)')
            ax[0].set_ylabel(f'% of std')
            ax[0].xaxis.set_major_formatter(ScalarFormatter())

            # for axj in [0, 1]:
            if not save_figs:
                ax[1].set_title(f'{metric_name} compute time vs mask size')
            else:
                ax[1].set_title('Compute Time')
            ax[1].set_xlabel('mask size (%)')
            ax[1].yaxis.tick_right()
            ax[1].yaxis.set_label_position('right')
            ax[1].set_ylabel('seconds')
            ax[1].xaxis.set_major_formatter(ScalarFormatter())

            # for k in [0, 1]:
            #     for j in [0, 1]:
            #         ax[k][j].legend(bbox_to_anchor=(1, 1), loc='upper left')

            plt.tight_layout()

            if save_figs:
                tikzplotlib.save(f'plots/mask-{metric_name}{filename_postfix}.tex',
                                 axis_width="3.25in", axis_height="1.4in", textsize=8.0)
            else:
                plt.show()


def plot_metrics_from_multiple(metric_dicts: list[dict], model_names: list[str], len_train_loader: int = None,
                               fig_size: tuple[int] = (12, 6),
                               log_plots: tuple[str] = ('conditioning_x', 'conditioning_w'),
                               save_figs: bool = False, filename_postfix: str = ''):

    for k1, d2 in metric_dicts[0].items():
        if k1 in ['iter', 'epoch', 'training_loss', 'testing_loss']:
            continue

        for i, metrics in enumerate(metric_dicts):
            # correct for not stepping each time metrics were calculated and then convert to epochs
            x = (np.array(metrics['iter']) - np.arange(len(metrics['iter'])))
            x_label = 'iterations'
            if len_train_loader is not None:
                x = x / len_train_loader
                x_label = 'epochs'

            fig, ax = plt.subplots(2, 1)
            fig.set_figwidth(fig_size[0])
            fig.set_figheight(fig_size[1])

            ax[0].plot(x, metrics['training_loss'], label=f'training {model_names[i]}')
            ax[0].plot(x, metrics['testing_loss'], label=f'testing {model_names[i]}')

            if type(d2) is dict:
                for mask_name, values in reversed(d2.items()):
                    to_plot = values
                    if len(to_plot) > len(metrics['iter']):
                        to_plot = to_plot

                    if k1 in log_plots:
                        ax[1].semilogy(x, to_plot, label=f'{model_names[i]} - ' + mask_name.replace('%', r'\%')
                                                         + ' mask')
                    else:
                        ax[1].plot(x, to_plot, label=f'{model_names[i]} - ' + mask_name.replace('%', r'\%') + ' mask')
            else:
                to_plot = d2
                if len(to_plot) > len(metrics['iter']):
                    to_plot = to_plot

                if k1 in log_plots:
                    ax[1].semilogy(x, to_plot, label=f'{model_names[i]}')
                else:
                    ax[1].plot(x, to_plot, label=f'{model_names[i]}')

            ax[1].set_title(k1)
            ax[1].xlabel(x_label)

            ax[0].set_ylabel('loss')
            ax[0].legend()
            ax[1].legend()

            if save_figs:
                tikzplotlib.save(f'plots/{k1}{filename_postfix}.tex',
                                 axis_width="6.5in", axis_height="1.5in", textsize=8.0)
            else:
                plt.show()
