import torch
from tqdm import tqdm
from numpy import mean

# from .optimal_lr import lr_line_search
from .gradient_metrics import all_grad_metrics


# ___________________________________________________________________________________________________________________________________________
def train(model_handler, max_epochs: int, train_loader, test_loader,
          rer_constants_fileloc: str = 'rer_constants.dt',
          n_samples_normalizing_constant: int = 5, sample_every: int = 50, debug: bool = False,
          tqdm_disabled: bool = False):
    """
    Using a ModelHandler train up to max_epochs while calculating the gradient metrics as specified by the ModelHandler.
    """
    if sample_every > len(train_loader) and model_handler.lr_scheduler is not None:
        raise ValueError('At least one sample must be drawn per epoch in order to use the lr_scheduler. ')

    rer_constants = torch.load(rer_constants_fileloc)
    rer_constants_updated = False

    while model_handler.epochs_completed < max_epochs:
        loop = tqdm(total=len(train_loader), disable=tqdm_disabled)
        test_losses_this_epoch = []
        for x_train, y_train in train_loader:
            x_train, y_train = x_train.cuda(), y_train.cuda()

            if sample_every is not None and model_handler.iters_completed % sample_every == 0:
                x_train.requires_grad = True
                if model_handler.model.opacus_wrapped:
                    model_handler.model.enable_hooks()

                y_hat = model_handler.model(x_train)
                loss = model_handler.loss_function(y_hat, y_train)
                loss.backward()

                # hessian_comp = hessian(model, loss_f, data=(x_train, y_train), cuda=True)
                # top_eigenvalues, top_eigenvector = hessian_comp.eigenvalues()
                # print("The top Hessian eigenvalue of this model is %.4f"%top_eigenvalues[-1])

                metrics_i, timing_i = all_grad_metrics(model_handler.model, model_handler.loss_function, x_train,
                                                       y_train, train_loader, inner_product_w=torch.inner,
                                                       norm_x=torch.linalg.vector_norm, norm_w=torch.linalg.vector_norm,
                                                       rank_normalizing_constants=rer_constants,
                                                       n_samples_norm_constant=n_samples_normalizing_constant,
                                                       debug=debug, **model_handler.get_metric_settings())

                if not rer_constants_updated:
                    torch.save(rer_constants, rer_constants_fileloc)
                    rer_constants_updated = True

                with torch.no_grad():
                    # get x and y test and transfer them to gpu
                    x_test, y_test = next(iter(test_loader))
                    x_test, y_test = x_test.cuda(), y_test.cuda()  # non_blocking tiny speed improvement
                    y_test_hat = model_handler.model(x_test)

                    # calculate and store testing losses and accuracy
                    test_loss = model_handler.loss_function(y_test_hat, y_test).item()
                    class_probabilities = torch.exp(y_test_hat)
                    chosen_classes = torch.multinomial(class_probabilities, 1).squeeze(1)
                    test_accuracy = torch.mean((chosen_classes == y_test).float()).item()

                    test_losses_this_epoch += [test_loss]

                    # per-class accuracies
                    # acct = np.zeros(model_handler.num_classes)
                    # for j in range(model_handler.num_classes):
                    #     mask = (y_test == j)
                    #     acct[j] = torch.mean((chosen_classes[mask] == y_test[mask]).cpu().float())

                # just in case
                model_handler.optimizer.zero_grad()

                lr = model_handler.optimizer.param_groups[0]['lr']
                model_handler.report_metrics(metrics_i, timing_i, loss.item(), test_loss, test_accuracy, lr)  # , acct)

            else:
                y_hat = model_handler.model(x_train)
                loss = model_handler.loss_function(y_hat, y_train)
                loss.backward()
                model_handler.optimizer.step()

                # opacus hooks don't disable properly for some reason. This ensures the next batch of opacus individual
                # grads that are intentionally calculated are calculated correctly, and it also prevents memory errors
                # from repeated storage of individual grads.
                if model_handler.model.opacus_wrapped:
                    model_handler.model.zero_grad()
            loop.update()
            loop.set_description(f'epoch: {model_handler.epochs_completed+1}/{max_epochs}, loss = {loss:.3f}')
            model_handler.increment_iteration()

        loop.close()
        if model_handler.lr_scheduler:
            model_handler.lr_scheduler.step(mean(test_losses_this_epoch))
        model_handler.increment_epoch()

    return model_handler

