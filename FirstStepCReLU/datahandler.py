import numpy as np
from sklearn.model_selection import train_test_split
from torch.utils.data import Dataset, DataLoader

#___________________________________________________________________________________________________________________________________________
class XYDataset(Dataset):
    def __init__(self, X, Y):
        self.len = len(Y)
        self.X = X.astype(np.float32)
        self.Y = Y.astype(np.float32)
        
    def __len__(self):
        return self.len
    
    def __getitem__(self, i):
        return self.X[i], self.Y[i]
    
#___________________________________________________________________________________________________________________________________________
def constructLoader(ds_name, m=100, test_size=None, random_state=None,
                    train_batch_size=32, test_batch_size=32):
    trainLoader, testLoader = None, None
    
    if ds_name == 'cos':
        m = 100
        X = np.linspace(0,2*np.pi,endpoint=False,num=m)
        Y = np.cos(X)[:,None]
        X -= np.mean(X)
        X /- np.std(X)
        X = np.stack((X,np.ones(m)),axis=1) # adds an unneeded bias

    elif ds_name == 'check':
        ds_name = 'check'
        s = int(np.ceil(np.sqrt(m)))
        m = s**2
        x = np.linspace(-2+2/(s),2-2/(s),num=s)
        X = np.reshape(np.stack(np.meshgrid(x,x),axis=2),(m,2))
        Y = (.5+.5*np.prod(np.sign((X-1)/2-np.floor(X/2)),axis=1))[:,None]
        X -= np.mean(X,axis=0)
        X /= np.std(X,axis=0)
        X = np.concatenate([X,np.ones(m)[:,None]], axis=1) # adds an unneccesary bias
    
    else:
        raise(InputError("Please input_ a supported data type"))

    if test_size:
        X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=test_size, random_state=random_state)
        trainData = XYDataset(X_train, Y_train)
        testData = XYDataset(X_test, Y_test)
        
        trainLoader = DataLoader(
                dataset=trainData,
                batch_size=train_batch_size,
                shuffle=True,
                pin_memory=True)
        testLoader = DataLoader(
                dataset=testData,
                batch_size=test_batch_size,
                shuffle=True,
                pin_memory=True)

    else:
        trainData = XYDataset(X, Y)
        trainLoader = DataLoader(
                dataset=trainData,
                batch_size=train_batch_size,
                shuffle=True,
                pin_memory=True)

    return trainLoader, testLoader