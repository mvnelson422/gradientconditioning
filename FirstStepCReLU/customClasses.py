import torch
from torch import nn

#___________________________________________________________________________________________________________________________________________
class CReLU(nn.Module):
    def __init__(self):
        super(CReLU, self).__init__()
        self.relu = nn.ReLU()

    def forward(self, input, dim=1):
        return torch.cat([self.relu(input), self.relu(-input)], dim=dim)

#___________________________________________________________________________________________________________________________________________
class LinearHettingerInit(nn.Module):
    def __init__(self, in_features, out_features, withCReLU=False, bias=True):
        """
        There was some carelessness in treating the bias in Chris's paper. So the initialization scheme may need
        to be tweaked slightly. Specifically, the standard deviation used might need to be adjusted.
        """
        if withCReLU:
            in_features = in_features//2

        super(LinearHettingerInit, self).__init__()
        self.__dict__.update(locals())

        P = torch.randn(out_features, in_features)/(in_features*out_features)**(0.25)
        b = torch.randn(out_features)/(in_features*out_features)**(0.25)

        if withCReLU:
            #temp = nn.Linear(in_features, out_features)
            #P = temp.weight.data
            N = P

            self.W = nn.Linear(2*in_features, out_features, bias=bias)
            self.W.weight.data = torch.cat([P, -N], dim=1)

        else:
            self.W = nn.Linear(in_features, out_features, bias=bias)
            #self.W.weight.data = P
            
        #if bias:
        #    self.W.bias.data = b
    
    def forward(self, input):
        return self.W(input)
    
#___________________________________________________________________________________________________________________________________________
class LinearNet(nn.Module):
    """
    Primarily designed with one-dimensional data in mind. 
    Could easily be adapted for two-dimensional 
    input_ by adding an AdaptiveAvgPool at the end.
    Obviously not optimal architecture.
    """
    def __init__(self, inputDimension, numClasses, depth, channels, layerClass=LinearHettingerInit, activationClass=CReLU, outf=None):
        super(LinearNet, self).__init__()
        self.inDims = inputDimension
        self.numClasses = numClasses
        self.depth = depth
        self.width = channels

        layers = [layerClass(inputDimension, channels)]

        channelsin = channels
        if activationClass is CReLU:
            channelsin = 2*channels

        if layerClass is LinearHettingerInit:
            withCReLU = (activationClass is CReLU)
            for i in range(depth - 1):
                layers += [activationClass(), layerClass(channelsin, channels, withCReLU=withCReLU)]
            layers += [activationClass(), layerClass(channelsin, numClasses, withCReLU=withCReLU)]

        else:
            for i in range(depth - 1):
                layers += [activationClass(), layerClass(channelsin, channels)]
            layers += [activationClass(), layerClass(channelsin, numClasses)]


        self.layers = nn.ModuleList(layers)
        self.outf = outf

    def forward(self, x, bool_return_S=False):
        S = 0
        if bool_return_S:
            n = len(self.layers)
            ds = torch.zeros(n+1)
            ds[0] = x.shape[1]

            for i, layer in enumerate(self.layers):
                x = layer(x)
                ds[i+1] = x.shape[1]

            S = np.sum([np.sqrt(ds[i-1]*ds[i]) for i in range(1, n+1)])*np.prod([1+2/ds[k] for k in range(1, n)])

        else:
            for layer in self.layers:
                x = layer(x)

        if self.outf is not None:
            x = self.outf(x)

        if bool_return_S:
            return x, S
        else:
            return x
        
#_______________________________________________________________________________________________________________________________________
class ResBlock(nn.Module):
    """
    A simple ResNet block with optional batch normalization and 2d convolutions. 
    """
    def __init__(self, in_planes, planes, stride=1, batchNorm=True, dropout_prob=.5, initialization=None):
        """"""
        super(ResBlock, self).__init__()
        
        if batchNorm:
            self.main_path =  nn.Sequential(
                                nn.BatchNorm2d(in_planes),
                                nn.Conv2d(in_planes, planes, kernel_size=3, padding=1, stride=stride),
                                nn.BatchNorm2d(planes),
                                nn.ReLU(),
                                nn.Conv2d(planes, planes, kernel_size=3, padding=1, stride=1))
        else:
            self.main_path =  nn.Sequential(
                                  nn.Conv2d(in_planes, planes, kernel_size=3, padding=1, stride=stride),
                                  nn.ReLU(),
                                  nn.Conv2d(planes, planes, kernel_size=3, padding=1, stride=1))
        
        # if dropout_prob is not None, add a dropout layer at the end of the main path
        if dropout_prob:
            self.main_path = nn.Sequential(self.main_path, nn.Dropout(p=dropout_prob))
            
        self.shortcut = nn.Sequential()
        if stride != 1 or in_planes != planes:
            if batchNorm:
                self.shortcut = nn.Sequential(
                        nn.BatchNorm2d(in_planes),
                        nn.Conv2d(in_planes, planes, kernel_size=1, stride=stride),
                        )
            else:
                self.shortcut = nn.Conv2d(in_planes, planes, kernel_size=1, stride=stride)
        
        self.finalReLU = nn.ReLU()

    def forward(self, x):
        out = self.main_path(x)
        out += self.shortcut(x)
        return self.finalReLU(out)

#_______________________________________________________________________________________________________________________________________
class ConvBlock(nn.Module):
    """
    A simple Convolutional block with optional batch normalization and 2d convolutions. 
    """
    def __init__(self, in_planes, planes, stride=1, batchNorm=True, dropout_prob=.5, initialization=None):
        """"""
        super(ConvBlock, self).__init__()
        
        if batchNorm:
            self.main_path =  nn.Sequential(
                                nn.BatchNorm2d(in_planes),
                                nn.Conv2d(in_planes, planes, kernel_size=3, padding=1, stride=stride),
                                nn.BatchNorm2d(planes),
                                nn.ReLU(),
                                nn.Conv2d(planes, planes, kernel_size=3, padding=1, stride=1))
        else:
            self.main_path =  nn.Sequential(
                                  nn.Conv2d(in_planes, planes, kernel_size=3, padding=1, stride=stride),
                                  nn.ReLU(),
                                  nn.Conv2d(planes, planes, kernel_size=3, padding=1, stride=1))
        
        # if dropout_prob is not None, add a dropout layer at the end of the main path
        if dropout_prob:
            self.main_path = nn.Sequential(self.main_path, nn.Dropout(p=dropout_prob))
            
        self.finalReLU = nn.ReLU()

    def forward(self, x):
        out = self.main_path(x)
        return self.finalReLU(out)

#_______________________________________________________________________________________________________________________________________ 
class BlockNet(nn.Module):
#     def __init__(self, in_layers, hidden_layer_specifications, num_classes,
#                  objective_func=None,
#                  dropout_prob=.5,
#                  batch_norm=True, initialization=None,
#                  block_type=ResBlock):
    def __init__(self, in_layers, num_classes, 
                     num_blocks, channels, 
                     block_type=ConvBlock, activationClass=nn.ReLU, 
                     objective_func=None, dropout_prob=0, batch_norm=False, initialization=None):
        """
        input_:
            hidden_layer_specifications: a vector of integers specifying the layer for each resnet block.
                When a stride other than 1 is desired, that entry should be a tuple where the second input_
                is the desired stride.
        """
        super(BlockNet, self).__init__()
        self.hidden_layer_specifications = [channels]*num_blocks
        self.num_classes = num_classes

        if activationClass is not nn.ReLU:
            print('Only nn.ReLU is compatible with this ResNet class. Ingoring specification.')
            
        
        # initial layer
        current_planes = self.hidden_layer_specifications[0]
        stride = 1
        # if type(current_planes) != int:
        #     current_planes, stride = current_planes
        
        self.inp = nn.Sequential(nn.Conv2d(in_layers, current_planes, kernel_size=3, padding=1, stride=stride))

        
        # the bulk of the network is composed of these blocks
        blocks = []
        for num_planes in self.hidden_layer_specifications[1:]:
            stride = 1
            #if type(num_planes) != int:
            #    num_planes, stride = num_planes
                
            blocks += [block_type(current_planes, num_planes, stride=stride, 
                 batchNorm=batch_norm, dropout_prob=dropout_prob, initialization=initialization).cuda()]
            current_planes = num_planes

        self.blocks = nn.ModuleList(blocks)
            
        # finally a linear layer and activation function
        self.avgpool = nn.AdaptiveAvgPool2d(1)
        self.linear = nn.Linear(current_planes, num_classes)
        
        if objective_func is not None:
            self.objective_func = objective_func
        else:
            self.objective_func = nn.Sequential()

    def forward(self, x):
        # initial reshaping convolutional layer
        out = self.inp(x)
        # print(out.shape)
        
        # blocks
        for block in self.blocks:
            out = block(out)
            # print(out.shape)

        # linear and softmax
        out = self.avgpool(out)
        out = out.view(out.size(0), -1)
        out = self.linear(out).squeeze()
        # print(out.shape)
        
        return self.objective_func(out)
