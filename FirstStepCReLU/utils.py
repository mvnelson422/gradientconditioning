import torch
from torch import nn
import numpy as np
from copy import deepcopy
from matplotlib import pyplot as plt

#___________________________________________________________________________________________________________________________________________
def find_optimal_lr(model, trainloader, testloader, optimizer_class, lossf, lr_bounds, npoints, findS=False):
    """
    Train the provided model several times for one step (using the same initialization repeatedly)
    to identify the learning rate which results in the lowest loss.
    """

    def optimize_func(lr):
        # reset the optimizer using a cloned model
        temp_model = deepcopy(model).to('cuda')
        optimizer = optimizer_class(temp_model.parameters(), lr=lr)

        # train for one step
        # best to use a large-ish batch size to reduce randomness here
        temp_model.zero_grad()
        for x, y in trainloader:
            x = x.to('cuda')
            y = y.to('cuda')
            
            y_hat = temp_model(x)
            
            loss = lossf(y_hat, y)
            loss.backward()
            optimizer.step()
            break

        # evaluate the loss using the whole testing set
        with torch.no_grad():
            lossArray = []
            for x, y in testloader:
                x = x.to('cuda')
                y = y.to('cuda')

                y_hat = temp_model(x)
                lossArray += [lossf(y_hat, y).item()]

        return np.mean(lossArray)
    
    # test all learning ratese in specified geomspace
    testpoints = np.geomspace(lr_bounds[0], lr_bounds[1], npoints)

    # test each point in geomspace
    optimal_lr = testpoints[0]
    low = optimize_func(optimal_lr)

    for lr in testpoints[1:]:
        loss = optimize_func(lr)
        if loss < low:
            low = loss
            optimal_lr = lr 

    if findS:
        Sn = 0
        with torch.no_grad():
            for x, y in trainloader:
                model = model.to('cuda')
                _, Sn = model(x.to('cuda'), True)
                break
        return optimal_lr, low, Sn
    else:
        return optimal_lr, low

#___________________________________________________________________________________________________________________________________________
def test_configurations(networkClass, layerClass, activationFClass, widths, depths, trainloader, testloader, trials_per_architecture,
                        optimizer_class, lossf, outDim, npoints=120, lr_bounds=(1e-12, .1), outf=None, verbose=True, findS=False):
    x, y = next(iter(testloader))
    inDim = x.size()[1]

    optimal_lrs = np.zeros((len(widths), len(depths)))
    variances = np.zeros((len(widths), len(depths)))
    mean_initial_losses = np.zeros((len(widths), len(depths)))
    mean_final_losses = np.zeros((len(widths), len(depths)))

    if findS:
        scaling_factors = np.ones_like(optimal_lrs)
    trainable_params = np.zeros_like(optimal_lrs)

    for j, depth in enumerate(depths):
        for i, width in enumerate(widths):
            lrs_per_trial = np.zeros(trials_per_architecture)
            initial_losses = np.zeros(trials_per_architecture)
            final_losses = np.zeros(trials_per_architecture)

            for k in range(trials_per_architecture):
                model = networkClass(inDim, outDim, depth, width, layerClass, activationFClass, outf).to('cuda')

                # find initial loss using the whole testing set
                with torch.no_grad():
                    lossArray = []
                    for x, y in testloader:
                        x = x.to('cuda')
                        y = y.to('cuda')

                        y_hat = model(x)
                        lossArray += [lossf(y_hat, y).item()]

                initial_losses[k] = np.mean(lossArray)

                # calculate optimal learning rate and lowest loss
                optimal_lr = 0.0

                if k == 0 and findS:
                    optimal_lr, lowest_loss, Sn  = find_optimal_lr(model, trainloader, testloader, optimizer_class, lossf, lr_bounds, npoints, findS=True)
                    scaling_factors[i, j] = Sn
                else:
                    optimal_lr, lowest_loss = find_optimal_lr(model, trainloader, testloader, optimizer_class, lossf, lr_bounds, npoints, findS=False)

                lrs_per_trial[k] = optimal_lr
                final_losses[k] = lowest_loss 

            optimal_lrs[i, j] = np.mean(lrs_per_trial)
            variances[i, j] = np.var(lrs_per_trial)
            mean_initial_losses[i, j] = np.mean(initial_losses)
            mean_final_losses[i, j] = np.mean(final_losses)
            trainable_params[i, j] = sum(p.numel() for p in model.parameters() if p.requires_grad)

            if verbose and findS:
                print(f'width: {width}, depth: {depth}, trainable parameters: {trainable_params[i, j]}, optimal lr: {optimal_lrs[i, j]}',
                      f'S: {scaling_factors[i, j]}, lr*S: {optimal_lrs[i, j]*(scaling_factors[i, j])}')
            elif verbose:
                print(f'width: {width}, depth: {depth}, trainable parameters: {trainable_params[i, j]}, optimal lr: {optimal_lrs[i, j]}',
                      f'variance: {variances[i, j]}')

            if i >= 2 and optimal_lrs[i, j]/optimal_lrs[i-1, j] > 10:
                break
    
    if findS:
        return optimal_lrs, variances, mean_initial_losses, mean_final_losses, trainable_params, scaling_factors
    else:
        return optimal_lrs, variances, mean_initial_losses, mean_final_losses, trainable_params
    
    
#___________________________________________________________________________________________________________________________________________
def test_plot_and_save(networkClass, layerClass, activationFClass, filename, root,
                       lossf, outf, optimizer_class,
                       trainloader, testloader,
                       minwidth, maxwidth, nwidths, depths, outDim,
                       trials_per_architecture=10, lr_npoints=280, lr_bounds=(.01, 1e-12), findS=False,
                       plot=True, plot_errors=True, figsize = (10, 6), colors=['b', 'g', 'r', 'c', 'm', 'y', 'k'],
                       verbose=True, repeat_if_file_exists=False, plotvswidth=False,
                       max_lr_to_plot=.01, ymin=None, ymax=None):

    try:
        var_dict = torch.load(root + filename)
        if repeat_if_file_exists:
            print('Previous run found. Re-executing tests and overwriting file.')
        else:
            print('Previous run found. Outputting previous results.')
    except:
        var_dict = None

    widths = np.logspace(np.log10(minwidth), np.log10(maxwidth), nwidths, dtype=np.int64)

    # if file doesn't exist or we want to replace it, run test_configurations()
    if var_dict is None or repeat_if_file_exists:
        var_dict = dict()
        if findS:
            
            optimal_lrs, variances, mean_initial_losses, mean_final_losses, trainable_params, scaling_factors == test_configurations(networkClass, 
                                                                                            layerClass, activationFClass, 
                                                                                            widths, depths, trainloader, testloader, 
                                                                                            trials_per_architecture, optimizer_class, lossf,
                                                                                            outDim,
                                                                                            npoints=lr_npoints, lr_bounds=lr_bounds,
                                                                                            outf=outf,
                                                                                            verbose=verbose,
                                                                                            findS=True)
            var_dict['scaling_factors'] = scaling_factors
        else:
            optimal_lrs, variances, mean_initial_losses, mean_final_losses, trainable_params = test_configurations(networkClass, 
                           layerClass, activationFClass, widths, depths, trainloader, testloader, trials_per_architecture,
                            optimizer_class, lossf, outDim, npoints=lr_npoints, lr_bounds=lr_bounds, outf=outf, 
                                                                verbose=verbose, findS=False)

        var_dict['optimal_lrs'] = optimal_lrs
        var_dict['variances'] = variances
        var_dict['mean_initial_losses'] = mean_initial_losses
        var_dict['mean_final_losses'] = mean_final_losses
        var_dict['trainable_params'] = trainable_params
        torch.save(var_dict, root+filename)

    # plot
    if plot:
        optimal_lrs = var_dict['optimal_lrs']
        variances = var_dict['variances']
        mean_initial_losses = var_dict['mean_initial_losses']
        mean_final_losses = var_dict['mean_final_losses']
        trainable_params = var_dict['trainable_params']
        
        toplot = ['optimal lr', 'losses']
        if findS: 
            toplot += ['Sn*lr']
            scaling_factors = var_dict['scaling_factors']

        for xvar in toplot:

            if xvar == 'optimal lr':
                # plot optimal learning rate versus total number of parameters
                mask = optimal_lrs < max_lr_to_plot
                if ymax is None:
                    ymax = np.max(optimal_lrs[mask])
                if ymin is None:
                    ymin = np.min(optimal_lrs[mask])
                xmax = np.max(trainable_params[mask])


            plt.figure(figsize=figsize)

            for i, depth in enumerate(depths):
                if plotvswidth:
                    x = widths
                else:
                    x = trainable_params[:, i]
                if xvar == 'optimal lr':
                    y = optimal_lrs[:, i]
                    err = np.sqrt(variances[:, i])
                elif xvar == 'losses':
                    y = mean_initial_losses[:, i]
                elif xvar == 'Sn*lr':
                    y = optimal_lrs[:, i]*scaling_factors[:, i]

                if xvar == 'Sn*lr':
                    plt.semilogx(x, y, 'o', color=colors[i], label=f'depth: {depth}')
                else:
                    plt.loglog(x, y, 'o', color=colors[i], label=f'depth: {depth}')
                
                if xvar == 'losses':
                    plt.loglog(x, mean_final_losses[:, i], 'o', color=colors[i], alpha=.3, label=f'depth: {depth} after one step')
                elif xvar == 'optimal lr' and plot_errors:
                    # upper and lower error bounds
                    err = np.sqrt(variances[:, i])
                    plt.loglog(x, y - err, 'o', color=colors[i], alpha=.5)
                    plt.loglog(x, y + err, 'o', color=colors[i], alpha=.5)

            titlexstr = ''
            if plotvswidth:
                plt.xlabel('hidden layer width')
            else:
                plt.xlabel('# of trainable parameters')
            if xvar == 'optimal lr':
                plt.ylabel('optimal learning rate')
                titlexstr = 'optimal learning rates'
            elif xvar == 'losses': 
                plt.ylabel('architecture loss')
                titlexstr = 'initial architecture loss'
            elif xvar == 'Sn*lr':
                plt.ylabel('lr*Sn')
                titlexstr = 'Scaling Factor * optimal learning rate'

            plt.title(titlexstr + ' by network size')
            plt.legend(bbox_to_anchor=(1.2, 1))
            if xvar == 'optimal lr':
                plt.ylim(0, ymax)
                plt.xlim(0, xmax * 1.1)
            plt.show()

    return var_dict